/* @flow */
import union from 'lodash/union';
import isPlainObject from 'lodash/isPlainObject';

export const COLLECTION_SET = 'COLLECTION_SET';
export const COLLECTION_UNSET = 'COLLECTION_UNSET';
export const COLLECTION_TOGGLE = 'COLLECTION_TOGGLE';

function parseAction(action) {
    const data = !Array.isArray(action.data) ? [action.data] : action.data;
    return {
        key: action.key,
        values: data
            .filter(entity => isPlainObject(entity))
            .map(entity => entity.id),
    };
}

export function set(state, action) {
    const { key, values } = parseAction(action);
    return {
        ...state,
        [key]: union(state[key] || [], values),
    };
}

export function unset(state, action) {
    const { key, values } = parseAction(action);
    return {
        ...state,
        [key]: (state[key] || []).filter(val => values.indexOf(val) === -1),
    };
}

export function toggle(state, action) {
    const { key, values } = parseAction(action);
    let collection = state[key] || [];
    values.forEach(val => {
        if (collection.indexOf(val) === -1) {
            collection.push(val);
        } else {
            collection = collection.filter(id => id !== val);
        }
    });

    return {
        ...state,
        [key]: collection,
    };
}

export default function collections(state = {}, action) {
    switch (action.type) {
        case COLLECTION_SET: {
            return set(state, action);
        }
        case COLLECTION_UNSET: {
            return unset(state, action);
        }
        case COLLECTION_TOGGLE: {
            return toggle(state, action);
        }
        default:
            return state;
    }
}
