import { COMPOSE_POST } from '../constants';

const initialState = {
    compose: null,
};

export default function post(state = initialState, action) {
    switch (action.type) {
        case COMPOSE_POST:
            return {
                ...state,
                compose: { ...state.compose, ...action.data },
            };
        default:
            return state;
    }
}
