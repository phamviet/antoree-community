import { COMPOSE_COMMENT } from '../constants';

const initialState = {
    compose: null,
};

export default function comment(state = initialState, action) {
    switch (action.type) {
        case COMPOSE_COMMENT:
            return {
                ...state,
                compose: { ...state.compose, ...action.data },
            };
        default:
            return state;
    }
}
