import { SET_RUNTIME_VARIABLE, UNSET_RUNTIME_VARIABLE } from '../constants';

export default function runtime(state = {}, action) {
    switch (action.type) {
        case SET_RUNTIME_VARIABLE:
            return {
                ...state,
                [action.payload.name]: action.payload.value,
            };
        case UNSET_RUNTIME_VARIABLE: {
            const { [action.payload.name]: deletedKey, ...newState } = state;
            return newState;
        }

        default:
            return state;
    }
}
