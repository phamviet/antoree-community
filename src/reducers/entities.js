import zipObject from 'lodash/zipObject';
import omit from 'lodash/omit';
import isPlainObject from 'lodash/isPlainObject';

export const ENTITY_STORE = 'ENTITY_STORE';
export const ENTITY_REMOVE = 'ENTITY_REMOVE';

export default function entities(state = {}, action) {
    switch (action.type) {
        case ENTITY_STORE: {
            let data = !Array.isArray(action.data)
                ? [action.data]
                : action.data;
            data = data.filter(entity => isPlainObject(entity));
            const merged = data.map(entity => ({
                ...(state[entity.id] ? state[entity.id] : {}),
                ...entity,
            }));
            return {
                ...state,
                ...zipObject(data.map(entity => entity.id), merged),
            };
        }
        case ENTITY_REMOVE: {
            const data = !Array.isArray(action.data)
                ? [action.data]
                : action.data;
            return omit(state, data.map(entity => entity.id));
        }
        default:
            return state;
    }
}
