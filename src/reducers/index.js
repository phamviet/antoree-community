import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import { reducer as formReducer } from 'redux-form';
import { reducer as toastrReducer } from 'react-redux-toastr';
import runtime from './runtime';
import entities from './entities';
import collections from './collections';
import user from './user';
import post from './post';
import comment from './comment';
import intl from './intl';

export default function createRootReducer({ apolloClient }) {
    return combineReducers({
        runtime,
        entities,
        collections,
        user,
        post,
        comment,
        intl,
        apollo: apolloClient.reducer(),
        loadingBar: loadingBarReducer,
        form: formReducer,
        toastr: toastrReducer,
    });
}
