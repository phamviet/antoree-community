export const USER_UPDATE = 'USER_UPDATE';
export default function user(state = null, action) {
    switch (action.type) {
        case USER_UPDATE:
            return {
                ...state,
                ...action.data,
            };
        default:
            return state;
    }
}
