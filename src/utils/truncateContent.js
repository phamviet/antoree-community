const slice = (string, maxLength, suffix) => {
    let content = string;
    if (content.length > maxLength) {
        content = content.substring(0, maxLength + 1);
        content = content.substring(
            0,
            Math.min(content.length, content.lastIndexOf(' ')),
        );
        content += suffix;
    }
    return content;
};

export default slice;
