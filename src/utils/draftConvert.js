/* eslint-disable react/jsx-no-comment-textnodes,jsx-a11y/iframe-has-title */
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import draftToHtml from 'draftjs-to-html';
import { convertFromHTML, ContentState, EditorState } from 'draft-js';
import Img from 'components/Img';
import { DETAIL } from '../shared/constants/imageSizes';

const alignmentStyles = {
    left: { float: 'left' },
    right: { float: 'right' },
    center: {
        marginLeft: 'auto',
        marginRight: 'auto',
        display: 'block',
    },
};

export const fromHTML = html => {
    if (html) {
        const blocksFromHTML = convertFromHTML(html);
        if (blocksFromHTML) {
            const contentState = ContentState.createFromBlockArray(
                blocksFromHTML.contentBlocks,
                blocksFromHTML.entityMap,
            );

            return EditorState.createWithContent(contentState);
        }
    }

    return EditorState.createEmpty();
};

export const toHTML = raw =>
    // eslint-disable-next-line no-unused-vars
    draftToHtml(raw, {}, false, (entity, text) => {
        const data = {
            src: '',
            alignment: 'left',
            ...(entity.data || {}),
        };

        if (entity.type === 'image') {
            return ReactDOMServer.renderToStaticMarkup(
                <Img
                    path={data.src}
                    style={alignmentStyles[data.alignment]}
                    {...DETAIL}
                />,
            );
        }

        if (entity.type === 'embed') {
            return ReactDOMServer.renderToStaticMarkup(
                <div className="embed-responsive embed-responsive-16by9">
                    <iframe
                        src={data.src}
                        allowFullscreen
                        className="embed-responsive-item"
                    />
                </div>,
            );
        }

        return '';
    });

export default { toHTML, fromHTML };
