import generateUrl from './generateUrl';

export default id => generateUrl(window.App.IMAGE_DOMAIN, id);
