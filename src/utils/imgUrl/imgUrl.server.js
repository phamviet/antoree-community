import generateUrl from './generateUrl';

export default id => generateUrl(process.env.IMAGE_DOMAIN, id);
