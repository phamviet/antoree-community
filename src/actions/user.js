/* eslint-disable import/prefer-default-export */
import { toastr } from 'react-redux-toastr';
import ReactGA from 'react-ga';
import toggleUserSubscription from 'shared/mutations/toggleUserSubscription.graphql';
import { ENTITY_STORE } from '../reducers/entities';

export function toggleSubscription(id) {
    return (dispatch, getState, { client }) => {
        const { user } = getState();
        if (!user) {
            ReactGA.event({
                category: 'User',
                action: 'LoginRequired',
                label: 'UserSubscription',
            });
            toastr.confirm('Please login!', { disableCancel: true });
            return false;
        }

        return client
            .mutate({
                mutation: toggleUserSubscription,
                variables: { id },
            })
            .then(({ data }) => {
                dispatch({
                    type: ENTITY_STORE,
                    data: data.user,
                });

                ReactGA.event({
                    category: 'User',
                    action: data.user.isSubscribed ? 'FOLLOW' : 'UNFOLLOW',
                    label: 'UserSubscription',
                    value: data.user.isSubscribed ? 1 : 0,
                });
            })
            .catch(console.error);
    };
}
