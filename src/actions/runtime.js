/* eslint-disable import/prefer-default-export */

import {
    SET_RUNTIME_VARIABLE,
    UNSET_RUNTIME_VARIABLE,
    NAVBAR_ACTION,
} from '../constants';

export function setRuntimeVariable({ name, value }) {
    return {
        type: SET_RUNTIME_VARIABLE,
        payload: {
            name,
            value,
        },
    };
}

export function unsetRuntimeVariable({ name }) {
    return {
        type: UNSET_RUNTIME_VARIABLE,
        payload: {
            name,
        },
    };
}

export function resetNavbarActions() {
    return unsetRuntimeVariable({ name: NAVBAR_ACTION });
}
