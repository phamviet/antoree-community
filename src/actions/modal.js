/* eslint-disable import/prefer-default-export */

import { SET_RUNTIME_VARIABLE, MODAL_KEY } from '../constants';

export function openModal(component) {
    return (dispatch, getState) => {
        const { runtime } = getState();
        const modals = runtime[MODAL_KEY] || [];
        let newVal;
        if (modals.some(modal => modal.component.key === component.key)) {
            newVal = modals.map(modal => {
                if (modal.component.key === component.key) {
                    return {
                        ...modal,
                        component,
                        isOpen: true,
                    };
                }

                return modal;
            });
        } else {
            newVal = [...modals, { component, isOpen: true }];
        }

        dispatch({
            type: SET_RUNTIME_VARIABLE,
            payload: {
                name: MODAL_KEY,
                value: newVal,
            },
        });
    };
}

export function closeModal(key) {
    return (dispatch, getState) => {
        const { runtime } = getState();
        const modals = runtime[MODAL_KEY] || [];
        if (modals.some(({ component }) => component.key === key)) {
            dispatch({
                type: SET_RUNTIME_VARIABLE,
                payload: {
                    name: MODAL_KEY,
                    value: modals.map(({ component }) => ({
                        component,
                        isOpen: component.key !== key,
                    })),
                },
            });
        }
    };
}
