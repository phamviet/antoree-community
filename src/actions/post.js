/* eslint-disable import/prefer-default-export */
/* eslint-disable no-nested-ternary */
import Promise from 'bluebird';
import { toastr } from 'react-redux-toastr';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import ReactGA from 'react-ga';
import togglePostReaction from 'shared/mutations/togglePostReaction.graphql';
import { ENTITY_STORE } from '../reducers/entities';
import { COLLECTION_TOGGLE } from '../reducers/collections';
import { COMPOSE_POST } from '../constants';

export function fetchPost(id) {
    // eslint-disable-next-line no-unused-vars
    return async (dispatch, getState, { client }) => {
        dispatch(showLoading());

        // todo fetch remote post with apolloclient
        await Promise.delay(1000);

        dispatch({
            type: COMPOSE_POST,
            data: { id, title: 'untitled', html: 'awesome' },
        });

        dispatch(hideLoading());
    };
}

export function toggleReaction(id, type) {
    return (dispatch, getState, { client }) => {
        const { user } = getState();
        if (!user) {
            ReactGA.event({
                category: 'User',
                action: 'LoginRequired',
                label: 'ArticleReaction',
            });
            toastr.confirm('Please login!', { disableCancel: true });
            return false;
        }

        return client
            .mutate({
                mutation: togglePostReaction,
                variables: { id, type },
            })
            .then(({ data }) => {
                dispatch({
                    type: ENTITY_STORE,
                    data: data.post,
                });

                if (type === 'BOOKMARK') {
                    dispatch({
                        type: COLLECTION_TOGGLE,
                        key: `bookmarks-${user.id}`,
                        data: data.post,
                    });
                }
                ReactGA.event({
                    category: 'Article',
                    action: type,
                    label: 'ArticleReaction',
                    value:
                        type === 'BOOKMARK'
                            ? data.post.isBookmarked ? 1 : 0
                            : data.post.isLiked ? 1 : 0,
                });

                return data.post;
            })
            .catch(console.error);
    };
}

export function toggleLike(id) {
    return toggleReaction(id, 'LIKE');
}

export function toggleBookmark(id) {
    return toggleReaction(id, 'BOOKMARK');
}
