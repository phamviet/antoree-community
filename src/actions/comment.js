import { showLoading, hideLoading } from 'react-redux-loading-bar';
import fetchCommentsQuery from 'shared/queries/fetchCommentsQuery.graphql';

export default function fetchComments(postId) {
    return async (dispatch, getState, { client }) => {
        dispatch(showLoading());

        const res = await client.query({
            query: fetchCommentsQuery,
            variables: { postId },
        });

        // eslint-disable-next-line no-unused-vars
        const { comments } = res.data;

        dispatch(hideLoading());
    };
}
