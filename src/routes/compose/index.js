import React from 'react';
import Layout from 'components/Layout';
import middleware, {
    ensureAuthenticated,
    ensureModerator,
} from '../middlewares';
import Compose from './Compose';
import postQuery from './postQuery.graphql';

const title = 'New post';

async function action({ client }, { postId }) {
    let post;

    if (postId) {
        try {
            const res = await client.query({
                query: postQuery,
                variables: { id: postId },
            });
            post = res.data.post;
        } catch (error) {
            console.error(error);
        }
    }

    return {
        chunks: ['compose'],
        title,
        component: (
            <Layout headerProps={{ fixed: 'top' }} fullWidth>
                <Compose post={post} title={title} />
            </Layout>
        ),
    };
}

export default middleware(ensureAuthenticated, ensureModerator)(action);
