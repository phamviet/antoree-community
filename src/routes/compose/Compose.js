/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { reset, change, SubmissionError } from 'redux-form';
import { filter } from 'graphql-anywhere';
import { toastr } from 'react-redux-toastr';
import {
    PostForm,
    FORM_NAME,
    PostFormComponent,
    PostMutable,
    savePostMutate,
} from 'components/Post';
import { convertFromRaw, EditorState } from 'draft-js';
import FormAction from 'components/Post/FormAction';
import { fetchPost } from 'actions/post';
import { STATUS_DRAFT } from 'shared/constants/post';
import { setRuntimeVariable } from 'actions/runtime';
import { createFetchUploader } from 'createFetch';
import topicSuggestionQuery from 'components/Picker/topicSuggestion.graphql';
import { NAVBAR_ACTION } from '../../constants';
import s from './Compose.css';

class Compose extends React.Component {
    static propTypes = {
        post: PostFormComponent.propTypes.initialValues,
        setRuntimeVariable: PropTypes.func.isRequired,
        changeFieldValue: PropTypes.func.isRequired,
    };

    static defaultProps = {
        post: undefined,
    };

    static contextTypes = {
        client: PropTypes.object.isRequired,
        fetch: PropTypes.func.isRequired,
    };

    // Events lifecycle
    componentDidMount() {
        // will be unset on new route transition
        this.props.setRuntimeVariable({
            name: NAVBAR_ACTION,
            value: <FormAction post={this.props.post} />,
        });
    }

    // Handlers
    handleSubmit = async input => {
        const { client } = this.context;
        try {
            // parse description
            const raw = JSON.parse(input.raw);
            const editorState = EditorState.createWithContent(
                convertFromRaw(raw),
            );
            const contentState = editorState.getCurrentContent();
            const firstBlock = contentState.getFirstBlock();
            // eslint-disable-next-line no-param-reassign
            input.description = firstBlock.getText();

            const { data } = await client.mutate({
                mutation: savePostMutate,
                variables: { input: filter(PostMutable, input) },
            });

            this.props.changeFieldValue(FORM_NAME, 'id', data.post.id, true);
            toastr.success('Saved!');
        } catch (error) {
            console.error(error);
            throw new SubmissionError(error);
        }
    };

    handleOnRequestSuggestions = async (name, selected) => {
        const { client } = this.context;
        const notIn = (selected || [])
            .filter(topic => topic.id)
            .map(topic => topic.id);

        try {
            const res = await client.query({
                query: topicSuggestionQuery,
                variables: { name, notIn },
            });
            return res.data.topics || [];
        } catch (error) {
            console.error(error);
        }

        return [];
    };

    handleUploadCallback = async ({ file }, done) => {
        const upload = createFetchUploader(fetch);
        upload(file, { attachedTo: 'post' })
            .then(f => done(null, f))
            .catch(done);
    };

    render() {
        const initialValues = { status: STATUS_DRAFT, ...this.props.post };
        return (
            <div className={[s.composer].join(' ')}>
                <PostForm
                    initialValues={initialValues}
                    onSubmit={this.handleSubmit}
                    handleUploadCallback={this.handleUploadCallback}
                    onRequestSuggestions={this.handleOnRequestSuggestions}
                />
            </div>
        );
    }
}

export default compose(
    connect(null, {
        fetchPost,
        setRuntimeVariable,
        resetForm: reset,
        changeFieldValue: change,
    }),
    withStyles(s),
)(Compose);
