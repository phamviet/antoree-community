import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Container } from 'reactstrap';
import CardList from 'components/PostList/CardList';
import LoadMore from 'components/LoadMore';
import { CardComponent } from 'components/Post/Card';
import s from './Topic.css';

const POST_PER_PAGE = 8;

class Topic extends React.Component {
    static propTypes = {
        topic: PropTypes.shape({
            id: PropTypes.string.isRequired,
            displayName: PropTypes.string.isRequired,
        }).isRequired,
        posts: PropTypes.arrayOf(PropTypes.shape(CardComponent.propTypes))
            .isRequired,
    };

    static contextTypes = {
        queries: PropTypes.object.isRequired,
    };

    state = {
        loadMore: this.props.posts.length >= POST_PER_PAGE,
        loading: false,
    };

    handleLoadMore = async () => {
        const { queries } = this.context;
        const { topic, posts } = this.props;
        const lastPost = posts[posts.length - 1];

        this.setState({ loading: true });
        try {
            const newPosts = await queries.fetchPosts({
                variables: {
                    topicIds: [topic.id],
                    limit: POST_PER_PAGE,
                    publishedBefore: lastPost.publishedAt,
                },
            });
            queries.collectionSet(topic.id, newPosts);
            if (newPosts.length < POST_PER_PAGE) {
                this.setState({ loadMore: false });
            }
        } catch (err) {
            console.error(err);
        }
        this.setState({ loading: false });
    };

    render() {
        const { topic, posts } = this.props;
        return (
            <div className="mb-5">
                <Container>
                    <CardList left posts={posts} title={topic.displayName} />
                    <LoadMore
                        onClick={this.handleLoadMore}
                        visible={posts.length && this.state.loadMore}
                        disabled={this.state.loading}
                        color="primary"
                        outline
                    />
                </Container>
            </div>
        );
    }
}

export default compose(
    connect(({ entities, collections }, { topic }) => ({
        posts: (collections[topic.id] || []).map(id => entities[id]),
    })),
    withStyles(s),
)(Topic);
