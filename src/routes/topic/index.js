import React from 'react';
import Layout from 'components/Layout';
import HeaderMenu from 'components/Navigation/HeaderMenu';
import Topic from './Topic';
import topicQueries from './topicQueries.graphql';

async function action(
    { client, queries: { collectionSet, fetchMenuItems } },
    { slug },
) {
    let topic;
    try {
        const { data } = await client.query({
            query: topicQueries,
            variables: { slug },
        });

        topic = data.topic;
        if (topic) {
            collectionSet(topic.id, data.posts);
        }
    } catch (error) {
        console.error(error);
    }

    if (!topic) {
        return {
            redirect: '/404',
        };
    }

    const menuItems = await fetchMenuItems();
    return {
        chunks: ['topic'],
        title: topic.displayName,
        component: (
            <Layout fullWidth>
                <HeaderMenu
                    isActive={item => (item ? item.slug === slug : false)}
                    items={menuItems}
                />
                <Topic topic={topic} />
            </Layout>
        ),
    };
}

export default action;
