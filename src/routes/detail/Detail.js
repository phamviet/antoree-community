import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Promise from 'bluebird';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import decorateComponentWithProps from 'decorate-component-with-props';
import { setRuntimeVariable } from 'actions/runtime';
import { reset, SubmissionError } from 'redux-form';
import { compose } from 'redux';
import { connect } from 'react-redux';
import omit from 'lodash/omit';
import { Container, Row, Col, Button, NavItem } from 'reactstrap';
import { injectIntl, FormattedMessage } from 'react-intl';
import messages from 'components/messages';
import { saveCommentMutate } from 'components/Comment';
import Author, { FollowButton, StudyRequestButton } from 'components/Author';
import Link from 'components/Link';
import { openModal } from 'actions/modal';
import { LearningRequest } from 'components/Modals';
import PostDetail, { Tags, Reaction } from 'components/PostDetail';
import Card, { CardComponent } from 'components/Post/Card';
import CardList from 'components/PostList/CardList';
import CommentList from 'components/Comment/CommentList';
import CommentBox from 'components/Comment/CommentBox';
import { BannerDetail } from 'components/Banners';
import { NAVBAR_ACTION } from '../../constants';
import s from './Detail.css';

const AntoreeEnglish = <strong>Antoree English</strong>;
const AntoreeInternational = <strong>Antoree International Pte.Ltd</strong>;

class Detail extends Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        post: PropTypes.shape({
            displayName: PropTypes.string.isRequired,
            html: PropTypes.string,
        }).isRequired,
        relatedPosts: PropTypes.arrayOf(
            PropTypes.shape(CardComponent.propTypes),
        ),
        user: PropTypes.shape({
            displayName: PropTypes.string.isRequired,
        }),
        resetForm: PropTypes.func.isRequired,
        setRuntimeVariable: PropTypes.func.isRequired,
        loadPostRest: PropTypes.func.isRequired,
        loadRelatedPosts: PropTypes.func.isRequired,
        // eslint-disable-next-line react/forbid-prop-types
        intl: PropTypes.object.isRequired,
    };

    static defaultProps = {
        user: null,
        relatedPosts: [],
    };

    static contextTypes = {
        client: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired,
    };

    // Events lifecycle
    componentDidMount() {
        // will be unset on new route transition
        const { user, post } = this.props;
        if (user && (user.id === post.author.id || user.isEditor)) {
            this.props.setRuntimeVariable({
                name: NAVBAR_ACTION,
                value: (
                    <NavItem className="mr-2 hidden-xs-down">
                        <Button
                            tag={Link}
                            to={`/compose/${post.id}`}
                            role="button"
                        >
                            Edit
                        </Button>
                    </NavItem>
                ),
            });
        }

        this.didMount(this.props.id);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.id !== this.props.id) {
            this.didMount(nextProps.id);
        }
    }

    didMount(id) {
        // lazy load post rest on client
        Promise.all([
            this.props.loadPostRest(id),
            this.props.loadRelatedPosts(id),
        ]);
    }

    // Handlers
    handleSubmitComment = async input => {
        const { client } = this.context;
        try {
            await client.mutate({
                mutation: saveCommentMutate,
                variables: { input },
            });
            this.props.resetForm();
        } catch (error) {
            console.error(error);
            throw new SubmissionError(error);
        }
    };

    renderRelatedPostsCard = post =>
        <Card vertical={false} {...omit(post, ['description'])} />;

    render() {
        const { post, user, relatedPosts = [], intl } = this.props;
        const { tags = [] } = post;
        const { author } = post;
        const DecoratedLearningRequest = decorateComponentWithProps(
            LearningRequest,
            {
                initialValues: {
                    postId: post.id,
                },
                kid: post.isKid,
            },
        );
        DecoratedLearningRequest.key = LearningRequest.key;

        return (
            <div>
                <div className={`container ${s.postContainer}`}>
                    <div className="pt-3 pt-sm-4">
                        <PostDetail className="mb-3 mb-md-5" {...post}>
                            <hr />
                            <p>
                                <FormattedMessage
                                    {...messages.antoreeSignature}
                                    values={{
                                        antoreeEnglish: AntoreeEnglish,
                                        antoreeInternational: AntoreeInternational,
                                    }}
                                />
                            </p>
                        </PostDetail>
                    </div>
                </div>
                <div className="container">
                    <BannerDetail
                        onAction={() =>
                            // eslint-disable-next-line react/prop-types
                            this.props.openModal(DecoratedLearningRequest)}
                        kid={post.isKid}
                        className="mb-3 mb-md-5"
                    />
                </div>
                <div className={`container ${s.postContainer}`}>
                    {!!tags.length &&
                        <Tags className="mb-3 mb-md-5" tags={post.tags} />}
                    <Reaction {...post} />
                    <hr />
                    <Row className="justify-content-md-center my-4 my-sm-5">
                        <Col>
                            <Author
                                {...author}
                                size={60}
                                subtitle={author.nationality}
                            />
                        </Col>
                        <Col md={4} xs={12} className="mt-sm-0 mt-3">
                            <FollowButton
                                isSubscribed={author.isSubscribed}
                                id={author.id}
                            />
                            <StudyRequestButton
                                {...author}
                                block
                                color="primary"
                            />
                        </Col>
                    </Row>
                </div>
                <div className={s.relatedContainer}>
                    {!!relatedPosts.length &&
                        <Container>
                            <CardList
                                posts={relatedPosts}
                                render={this.renderRelatedPostsCard}
                                cols={3}
                                title={intl.formatMessage(
                                    messages.relatedPosts,
                                )}
                            />
                        </Container>}
                    {false &&
                        <div className={s.postContainer}>
                            <div className="my-3">
                                <CommentBox
                                    initialValues={{
                                        authorId: post.author.id,
                                        postId: post.id,
                                    }}
                                    user={user}
                                    onSubmit={this.handleSubmitComment}
                                />
                            </div>
                            <CommentList comments={post.comments} />
                        </div>}
                </div>
            </div>
        );
    }
}

export default compose(
    connect(
        ({ user, entities }, { id }) => ({
            user,
            post: entities[id],
            relatedPosts: (entities[id].relatedPostIds || [])
                .map(idd => entities[idd]),
        }),
        {
            setRuntimeVariable,
            openModal,
            resetForm: reset,
        },
    ),
    withStyles(s),
    injectIntl,
)(Detail);
