import React from 'react';
import HeaderMenu from 'components/Navigation/HeaderMenu';
import Layout from 'components/Layout';
import { ENTITY_STORE } from 'reducers/entities';
import detailQueries from './detailQueries.graphql';
import postRestQuery from './postRestQuery.graphql';
import relatedPostsQuery from './relatedPostsQuery.graphql';
import Detail from './Detail';

async function action(
    { store, client, queries: { fetchMenuItems } },
    { slug },
) {
    let post = {};
    // lookup from client store
    const { entities = {} } = store.getState();
    const postIds = Object.keys(entities).filter(
        id => entities[id].slug === slug,
    );

    if (postIds.length) {
        post = entities[postIds[0]];
    } else {
        // fallback to server
        try {
            const res = await client.query({
                query: detailQueries,
                variables: { slug },
            });
            post = res.data.post;
            if (post) {
                store.dispatch({
                    type: ENTITY_STORE,
                    data: post,
                });
            }
        } catch (error) {
            // bugsnap
            throw error;
        }
    }

    if (!post) {
        return {
            redirect: '/404',
        };
    }

    const loadPostRest = async id => {
        try {
            const { data } = await client.query({
                query: postRestQuery,
                variables: { id },
            });
            store.dispatch({
                type: ENTITY_STORE,
                data: data.post,
            });
        } catch (error) {
            // bugsnap
            throw error;
        }
    };

    const loadRelatedPosts = async id => {
        try {
            const { data } = await client.query({
                query: relatedPostsQuery,
                variables: { id },
            });
            // relatedPosts
            const { relatedPosts = [] } = data.post;
            const relatedPostIds = relatedPosts.map(p => p.id);

            store.dispatch({
                type: ENTITY_STORE,
                data: relatedPosts,
            });
            store.dispatch({
                type: ENTITY_STORE,
                data: { id, relatedPostIds },
            });
        } catch (error) {
            // bugsnap
            throw error;
        }
    };

    const menuItems = await fetchMenuItems();

    return {
        chunks: ['detail'],
        title: post.displayName,
        description: post.description,
        component: (
            <Layout fullWidth>
                <HeaderMenu items={menuItems} />
                <Detail
                    loadPostRest={loadPostRest}
                    loadRelatedPosts={loadRelatedPosts}
                    id={post.id}
                />
            </Layout>
        ),
        meta: {
            'og:type': 'article',
            'og:locale': 'vi-VN',
            'og:image': post.photo ? post.photo.ogImage : null,
            'og:image:width': post.photo ? 600 : null,
            'og:image:height': post.photo ? 315 : null,
        },
    };
}

export default action;
