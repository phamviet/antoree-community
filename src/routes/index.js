/* eslint-disable prettier/prettier */
/* eslint-disable global-require */
import objectAssign from 'object-assign';

// The top-level (parent) route
const routes = {
    path: '/',

    // Keep in mind, routes are evaluated in order
    children: [
        {
            path: '/',
            load: () => import(/* webpackChunkName: 'home' */ './home'),
        },
        {
            path: '/manage',
            children: [
                {
                    path: '/',
                    load: () => import(/* webpackChunkName: 'admin-dashboard' */ './admin/dashboard'),
                },
                {
                    path: '/posts',
                    load: () => import(/* webpackChunkName: 'admin-post' */ './admin/post'),
                },
                {
                    path: '/topics',
                    load: () => import(/* webpackChunkName: 'admin-topic' */ './admin/topic'),
                },
                {
                    path: '/users',
                    load: () => import(/* webpackChunkName: 'admin-topic' */ './admin/user'),
                },
            ],
            load: () => import(/* webpackChunkName: 'admin' */ './admin'),
        },
        {
            path: '/compose/:postId?',
            name: 'compose',
            load: () => import(/* webpackChunkName: 'compose' */ './compose'),
        },
        {
            path: '/profile/:userId',
            name: 'profile',
            load: () => import(/* webpackChunkName: 'profile' */ './profile'),
        },
        {
            path: '/login',
            load: () => import(/* webpackChunkName: 'login' */ './login'),
        },
        {
            path: '/topic/:slug',
            load: () => import(/* webpackChunkName: 'topic' */ './topic'),
        },
        {
            path: '/article/:slug',
            load: () => import(/* webpackChunkName: 'detail' */ './detail'),
        },
        // Wildcard routes, e.g. { path: '*', ... } (must go last)
        {
            path: '*',
            load: () => import(/* webpackChunkName: 'not-found' */ './not-found'),
        },
    ],

    async action({ next }) {
        // Execute each child route until one of them return the result
        const route = await next();

        // Provide default values for title, description etc.
        route.title = route.title || '';
        route.description = route.description || '';
        route.meta = objectAssign(
            {
                'og:type': 'website',
                'og:image': 'https://static.community.antoree.com/images/600-default.png',
                'og:image:width': 600,
                'og:image:height': 315,
            },
            route.meta || {},
        );

        return route;
    },
};

// The error page is available by permanent url for development mode
if (__DEV__) {
    routes.children.unshift({
        path: '/error',
        action: require('./error').default,
    });

    // routes.children.unshift({
    //     path: '/docs',
    //     load: () => import(/* webpackChunkName: 'docs' */ './docs'),
    //     children: [
    //         {
    //             path: '/editor',
    //             load: () => import(/* webpackChunkName: 'docs-editor' */ './docs/editor'),
    //         }
    //     ],
    // });
}

export default routes;
