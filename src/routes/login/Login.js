/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { injectIntl, defineMessages, FormattedMessage } from 'react-intl';
import { Alert, Row, FormGroup, Col, Input, Button } from 'reactstrap';
import FaFacebook from 'react-icons/lib/fa/facebook';
import Link from 'components/Link';

import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Login.css';

const messages = defineMessages({
    title: {
        id: 'login.title',
        description: 'Login title',
        defaultMessage: 'Sign in to continue',
    },
    register: {
        id: 'login.register',
        defaultMessage: 'Register',
    },
    signin: {
        id: 'login.signin',
        defaultMessage: 'Sign In',
    },
    orContinueWith: {
        id: 'login.orContinueWith',
        defaultMessage: 'Or continue with',
    },
    usernameLabel: {
        id: 'login.usernameLabel',
        defaultMessage: 'Username or email address',
    },
    password: {
        id: 'login.password',
        defaultMessage: 'Password',
    },
    doNotHaveAccount: {
        id: 'login.doNotHaveAccount',
        defaultMessage: "Don't have an account?",
    },
});

class Login extends React.Component {
    static propTypes = {
        runtimeMessages: PropTypes.shape({
            error: PropTypes.arrayOf(PropTypes.string),
        }),
        // eslint-disable-next-line react/forbid-prop-types
        intl: PropTypes.object,
    };

    static defaultProps = {
        runtimeMessages: {},
        intl: {},
    };

    render() {
        const { runtimeMessages, intl } = this.props;

        return (
            <div
                className={classNames(
                    'd-flex',
                    'justify-content-md-center',
                    s.signInContent,
                )}
            >
                <Link
                    role="button"
                    aria-label="Close"
                    className={classNames('close', s.close)}
                    to="/"
                >
                    <span>&times;</span>
                </Link>
                <div className="col col-md-auto px-0">
                    <h5 className={s.head}>
                        <FormattedMessage {...messages.title} />
                    </h5>
                    {runtimeMessages &&
                        (runtimeMessages.error || []).map(errorMessage =>
                            <Alert
                                className="text-center"
                                key={errorMessage}
                                color="danger"
                            >
                                {errorMessage}
                            </Alert>,
                        )}
                    <Button
                        tag="a"
                        href="/login/facebook/"
                        block
                        size="lg"
                        className={classNames(
                            'mb-3',
                            s.socialButton,
                            s.facebook,
                        )}
                    >
                        <span className={s.facebookIcon}>
                            <FaFacebook />{' '}
                        </span>
                        Sign in with Facebook
                    </Button>
                    <Row>
                        <Col xs={12} md={6}>
                            <Button
                                tag="a"
                                size="lg"
                                href="/login/google"
                                className={classNames(
                                    'mb-3 mb-sm-0',
                                    s.socialButton,
                                    s.google,
                                )}
                                block
                                outline
                            >
                                Sign In with Google{' '}
                            </Button>
                        </Col>
                        <Col xs={12} md={6}>
                            <Button
                                tag="a"
                                href="/login/linkedin"
                                size="lg"
                                outline
                                block
                                className={classNames(
                                    s.socialButton,
                                    s.linkedin,
                                )}
                            >
                                Sign In with Linkedin{' '}
                            </Button>
                        </Col>
                    </Row>
                    <p className={s.registerEmail}>
                        <span>
                            <FormattedMessage {...messages.orContinueWith} />
                        </span>
                    </p>
                    <form action="/login" method="post">
                        <FormGroup>
                            <label htmlFor="usernameOrEmail">
                                <FormattedMessage
                                    {...messages.usernameLabel}
                                />:
                            </label>
                            <Input
                                className="form-control"
                                id="usernameOrEmail"
                                size="lg"
                                type="text"
                                name="username"
                                autoFocus // eslint-disable-line jsx-a11y/no-autofocus
                            />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="password">
                                <FormattedMessage {...messages.password} />:
                            </label>
                            <Input
                                className="form-control"
                                id="password"
                                size="lg"
                                type="password"
                                name="password"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Button
                                block
                                size="lg"
                                color="primary"
                                type="submit"
                                role="button"
                            >
                                <FormattedMessage {...messages.signin} />
                            </Button>
                        </FormGroup>
                    </form>
                    <h4 className={s.footerLink}>
                        <FormattedMessage {...messages.doNotHaveAccount} />{' '}
                        <a
                            href="https://antoree.com/vi/tai-khoan/dang-ky"
                            target="_blank"
                            title={intl.formatMessage(messages.register)}
                        >
                            <FormattedMessage {...messages.register} />
                        </a>
                    </h4>
                </div>
            </div>
        );
    }
}

export default compose(
    connect(({ runtime }) => ({
        runtimeMessages: runtime.messages,
    })),
    withStyles(s),
    injectIntl,
)(Login);
