import React from 'react';
import { defineMessages } from 'react-intl';
import Layout from 'components/Layout';
import Login from './Login';

const messages = defineMessages({
    title: {
        id: 'loginPage.title',
        description: 'Sign in page title',
        defaultMessage: 'Sign In',
    },
});

function action({ intl }) {
    const title = intl.formatMessage(messages.title);
    return {
        chunks: ['login'],
        title,
        component: (
            <Layout blank>
                <Login />
            </Layout>
        ),
    };
}

export default action;
