import React from 'react';
import DocPage from './DocPage';

async function action({ next }) {
    const route = await next();

    return {
        title: 'Docs',
        component: (
            <DocPage>
                {route ? route.component : null}
            </DocPage>
        ),
    };
}

export default action;
