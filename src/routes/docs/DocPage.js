import React from 'react';
import PropTypes from 'prop-types';

class DocPage extends React.Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
    };

    render() {
        if (__DEV__) {
            return this.props.children;
        }

        return (
            <div>
                <h1>Error</h1>
                <p>Sorry, a critical error occurred on this page.</p>
            </div>
        );
    }
}

export default DocPage;
