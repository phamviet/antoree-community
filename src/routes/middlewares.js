/* eslint-disable no-plusplus */
import { EDITOR, ADMINISTRATOR } from 'shared/constants/roles';

const ensureAuthenticated = async ({ store }) => {
    const { user } = store.getState();
    if (!user) {
        return {
            redirect: '/login',
        };
    }

    return null;
};

const ensureRole = role => async ({ store }) => {
    const { user } = store.getState();
    const userRole = parseInt(user.role, 10);
    const typedRole = parseInt(role, 10);

    // eslint-disable-next-line no-bitwise
    if ((typedRole & userRole) !== typedRole) {
        return {
            redirect: '/',
        };
    }

    return null;
};

const ensureModerator = ensureRole(EDITOR);
const ensureAdministrator = ensureRole(ADMINISTRATOR);

export {
    ensureAuthenticated,
    ensureRole,
    ensureModerator,
    ensureAdministrator,
};

export default (...middlewares) => action => async (...arg) => {
    let testRoute;

    for (let i = 0; i < middlewares.length; i++) {
        // eslint-disable-next-line no-await-in-loop
        testRoute = await middlewares[i].call(null, ...arg);
        if (testRoute) {
            break;
        }
    }

    if (testRoute) {
        return testRoute;
    }

    const route = await action(...arg);

    return route;
};
