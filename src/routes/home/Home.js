import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import omit from 'lodash/omit';
import { injectIntl, defineMessages } from 'react-intl';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Card, { CardComponent } from 'components/Post/Card';
import CardList from 'components/PostList/CardList';
import HomeSlider from 'components/Slider/HomeSlider';
import { HOME_FEATURED_TOPICS } from 'shared/constants/topic';
import { HOME_LATEST_POSTS, HOME_MASTHEAD_POSTS } from 'shared/constants/post';
import s from './Home.css';

const messages = defineMessages({
    topStories: {
        id: 'home.topStories',
        defaultMessage: "Today's top stories",
    },
});

class Home extends React.Component {
    static propTypes = {
        sliderPosts: PropTypes.arrayOf(
            PropTypes.shape({
                ...CardComponent.propTypes,
            }),
        ),
        topStories: PropTypes.arrayOf(
            PropTypes.shape({
                ...CardComponent.propTypes,
            }),
        ),
        featuredTopics: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string.isRequired,
                displayName: PropTypes.string.isRequired,
            }),
        ),
        onDidMount: PropTypes.func.isRequired,
    };

    static defaultProps = {
        sliderPosts: [],
        topStories: [],
        featuredTopics: [],
    };

    static contextTypes = {
        store: PropTypes.object.isRequired,
    };

    // Events lifecycle
    componentDidMount() {
        this.props.onDidMount();
    }

    getPostCollection = topic => {
        const { entities, collections } = this.context.store.getState();
        return (collections[`home-${topic.id}`] || []).map(id => entities[id]);
    };

    renderFeaturedTopicsCard = post =>
        <Card vertical={false} {...omit(post, ['description'])} />;

    render() {
        // eslint-disable-next-line react/prop-types
        const { intl, sliderPosts, topStories, featuredTopics } = this.props;
        return (
            <div className="mb-5">
                <div className="pt-0 pt-md-3">
                    <HomeSlider posts={sliderPosts} />
                </div>
                <Container className="py-3">
                    <CardList
                        left
                        posts={topStories}
                        title={intl.formatMessage(messages.topStories)}
                    />
                    {featuredTopics.map(topic => {
                        const posts = this.getPostCollection(topic);
                        return posts.length
                            ? <CardList
                                  key={topic.id}
                                  moreLink={topic.href}
                                  posts={this.getPostCollection(topic)}
                                  title={topic.displayName}
                                  cols={3}
                                  render={this.renderFeaturedTopicsCard}
                              />
                            : null;
                    })}
                </Container>
            </div>
        );
    }
}

export default compose(
    connect(({ entities, collections }) => ({
        sliderPosts: (collections[HOME_MASTHEAD_POSTS] || [])
            .map(id => entities[id]),
        topStories: (collections[HOME_LATEST_POSTS] || [])
            .map(id => entities[id]),
        featuredTopics: (collections[HOME_FEATURED_TOPICS] || [])
            .map(id => entities[id]),
    })),
    withStyles(s),
    injectIntl,
)(Home);
