import Promise from 'bluebird';
import React from 'react';
import Layout from 'components/Layout';
import { ENTITY_STORE } from 'reducers/entities';
import { COLLECTION_SET } from 'reducers/collections';
import { HOME_FEATURED_TOPICS } from 'shared/constants/topic';
import { HOME_LATEST_POSTS, HOME_MASTHEAD_POSTS } from 'shared/constants/post';
import HeaderMenu from 'components/Navigation/HeaderMenu';
import Home from './Home';
import homeQueries from './homeQueries.graphql';
import featuredTopicsQuery from './featuredTopicsQuery.graphql';
import slidesQuery from './slidesQuery.graphql';

const HOME_LOADED_IDS = 'HOME_LOADED_IDS';

/**
 * Notice: There was a reason causing queries running sequentially instead of parallel
 *
 * @param client
 * @param store
 * @param intl
 * @return {Promise.<{chunks: [string], title: string, component: XML}>}
 */
async function action({ client, store }) {
    let menuItems = [];
    try {
        const res = await client.query({
            query: slidesQuery,
        });

        const posts = res.data.viewer.search.edges.map(({ node }) => node);
        const restRes = await client.query({
            query: homeQueries,
            variables: {
                notIds: posts.map(p => p.id),
            },
        });

        const { topStories, headerMenu } = restRes.data;
        menuItems = headerMenu.entities;

        // redux way
        store.dispatch({
            type: ENTITY_STORE,
            data: topStories.concat(posts),
        });

        store.dispatch({
            type: COLLECTION_SET,
            key: HOME_MASTHEAD_POSTS,
            data: posts,
        });
        store.dispatch({
            type: COLLECTION_SET,
            key: HOME_LATEST_POSTS,
            data: topStories,
        });
        store.dispatch({
            type: COLLECTION_SET,
            key: HOME_LOADED_IDS,
            data: topStories.concat(posts),
        });
    } catch (error) {
        console.error(error);
        throw new Error('Failed to load home data.');
    }

    const loadFeaturedTopicsPosts = async () => {
        const { collections } = store.getState();
        const notIds = collections[HOME_LOADED_IDS] || [];
        try {
            const { data } = await client.query({
                query: featuredTopicsQuery,
                variables: { notIds },
            });
            const { featuredTopics } = data;

            store.dispatch({
                type: ENTITY_STORE,
                data: featuredTopics.topics.map(({ posts, ...topic }) => topic),
            });

            featuredTopics.topics.forEach(topic => {
                store.dispatch({
                    type: ENTITY_STORE,
                    data: topic.posts.edges.map(({ node }) => node),
                });
                store.dispatch({
                    type: COLLECTION_SET,
                    key: `home-${topic.id}`,
                    data: topic.posts.edges.map(({ node }) => node),
                });
            });
            store.dispatch({
                type: COLLECTION_SET,
                key: HOME_FEATURED_TOPICS,
                data: featuredTopics.topics,
            });
        } catch (error) {
            // bugsnap
            throw error;
        }
    };

    const didMount = () => Promise.all([loadFeaturedTopicsPosts()]);

    return {
        chunks: ['home'],
        title: null,
        description:
            'Cộng đồng Antoree - Tiếng anh trực tuyến 1 kèm 1, kết nối người học và người dạy tiếng anh trên toàn thế giới',
        component: (
            <Layout fullWidth>
                <HeaderMenu isActive={item => !item} items={menuItems} />
                <Home onDidMount={didMount} />
            </Layout>
        ),
    };
}

export default action;
