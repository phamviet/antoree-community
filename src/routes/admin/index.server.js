import React from 'react';
import middleware, {
    ensureAuthenticated,
    ensureModerator,
} from '../middlewares';

async function action() {
    return {
        component: (
            <div>
                <div />
            </div>
        ),
    };
}

export default middleware(ensureAuthenticated, ensureModerator)(action);
