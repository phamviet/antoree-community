/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import { graphql, createFragmentContainer } from 'react-relay';
import {
    Row,
    Col,
    Media,
    Nav,
    NavLink,
    NavDropdown,
    DropdownItem,
    DropdownToggle,
    DropdownMenu,
} from 'reactstrap';
import MdKeyboardControlIcon from 'react-icons/lib/md/keyboard-control';

function getDropdownActionItems({ user, onToggleRole }) {
    const navs = [];
    const { isEditor, isAdministrator } = user;

    navs.push(
        <NavLink onClick={() => onToggleRole(user, 'EDITOR')} href="#">
            {isEditor ? 'Remove' : 'Set'} Editor
        </NavLink>,
    );

    navs.push(
        <NavLink onClick={() => onToggleRole(user, 'ADMINISTRATOR')} href="#">
            {isAdministrator ? 'Remove' : 'Set'} Administrator
        </NavLink>,
    );

    return navs;
}

class UserComponent extends React.Component {
    state = {
        dropdownOpen: false,
    };

    toggleDropdownMenu = () => {
        this.setState(state => ({ dropdownOpen: !state.dropdownOpen }));
    };

    render() {
        const { data, ...props } = this.props;
        const { displayName, email, href, photo } = data;
        const dropdownActionItems = getDropdownActionItems({
            user: data,
            ...props,
        });

        return (
            <div>
                <Row>
                    <Col xs="7">
                        <Media className="align-items-center">
                            <Media left target="_blank" href={href}>
                                {photo &&
                                    <Media
                                        object
                                        src={photo.src}
                                        width={64}
                                        alt={displayName}
                                    />}
                            </Media>
                            <Media className="px-2" body>
                                <Media heading tag="p">
                                    {displayName || email} <br />
                                    {displayName !== email &&
                                        <small className="text-muted">
                                            {email}
                                        </small>}
                                </Media>
                            </Media>
                        </Media>
                    </Col>
                    <Col xs="5">
                        <Nav className="justify-content-end">
                            <NavDropdown
                                isOpen={this.state.dropdownOpen}
                                toggle={this.toggleDropdownMenu}
                            >
                                <DropdownToggle nav>
                                    <MdKeyboardControlIcon />
                                </DropdownToggle>
                                <DropdownMenu>
                                    {dropdownActionItems.map((action, i) =>
                                        <DropdownItem key={i}>
                                            {action}
                                        </DropdownItem>,
                                    )}
                                </DropdownMenu>
                            </NavDropdown>
                        </Nav>
                    </Col>
                </Row>
                <hr />
            </div>
        );
    }
}

UserComponent.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.string.isRequired,
        href: PropTypes.string.isRequired,
        displayName: PropTypes.string,
        email: PropTypes.string,
        isEditor: PropTypes.bool.isRequired,
        isAdministrator: PropTypes.bool.isRequired,
    }).isRequired,
    onToggleRole: PropTypes.func.isRequired,
};

export default createFragmentContainer(
    UserComponent,
    graphql`
        fragment User on User {
            id
            displayName
            email
            href
            isEditor
            isAdministrator
            photo {
                src: url(w: 64, h: 64)
            }
        }
    `,
);
