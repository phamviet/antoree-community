import { graphql } from 'react-relay';

export default {
    toggleUserRole: graphql`
        mutation mutation_toggleUserRoleMutation(
            $id: ID!
            $role: UserRoleEnum!
        ) {
            toggleUserRole(id: $id, role: $role) {
                id
                isEditor
                isAdministrator
            }
        }
    `,
};
