/* eslint-disable react/prop-types,react/sort-comp */
/* @flow */

import React from 'react';
import PropTypes from 'prop-types';
import { graphql, createPaginationContainer } from 'react-relay';
import { FormGroup } from 'reactstrap';
import LoadMore from 'components/LoadMore';
import SearchInput from 'components/SearchInput';
import SelectButton from 'components/SelectButton';
import roles, { ADMINISTRATOR, EDITOR } from 'shared/constants/roles';
import User from './User';
import mutation from './mutation';

export const initialQuery = graphql`
    query UserListQuery(
        $query: String
        $role: UserRoleEnum
        $count: Int
        $cursor: String
    ) {
        viewer {
            ...UserList_viewer
        }
    }
`;

export const ITEM_PER_PAGE = 10;
const filters = [
    {
        value: 'ALL',
        label: 'ALL',
    },
    {
        value: EDITOR,
        label: 'EDITOR',
    },
    {
        value: ADMINISTRATOR,
        label: 'ADMINISTRATOR',
    },
];

class UserList extends React.Component {
    static propTypes = {
        commitMutation: PropTypes.func.isRequired,
    };

    state = {
        loading: this.props.relay.isLoading(),
        role: 'ALL',
    };

    handleRoleFilterChange = role =>
        this.setState({ role, loading: true }, () =>
            this.refetch({ role: roles[role] }),
        );

    handleSearchChange = query =>
        this.setState({ loading: true }, () => this.refetch({ query }));

    handleToggleRole = ({ id }, role) =>
        this.props.commitMutation({
            mutation: mutation.toggleUserRole,
            variables: { id, role },
        });

    canLoadMore() {
        return this.props.relay.hasMore() && !this.state.loading;
    }

    refetch(variables = {}) {
        this.props.relay.refetchConnection(
            null,
            () => this.setState({ loading: false }),
            { ...variables, count: ITEM_PER_PAGE },
        );
    }

    loadMore() {
        if (!this.canLoadMore()) {
            return;
        }

        this.props.relay.loadMore(ITEM_PER_PAGE, () =>
            this.setState({ loading: false }),
        );
    }

    render() {
        const { users = {} } = this.props.viewer;
        return (
            <div>
                <FormGroup className="text-center">
                    <SelectButton
                        options={filters}
                        isActive={role => this.state.role === role}
                        onChange={role => this.handleRoleFilterChange(role)}
                    />
                </FormGroup>
                <FormGroup>
                    <SearchInput
                        placeholder="Search by name or email"
                        size="lg"
                        onChange={query => this.handleSearchChange(query)}
                    />
                </FormGroup>
                <small className="text-slate">
                    Total: {users.totalCount}
                </small>
                <hr />
                {users.edges.map(
                    ({ node }) =>
                        node &&
                        <User
                            key={node.id}
                            onToggleRole={this.handleToggleRole}
                            data={node}
                        />,
                )}
                <LoadMore
                    visible={this.canLoadMore()}
                    onClick={() => this.loadMore()}
                />
            </div>
        );
    }
}

export default createPaginationContainer(
    UserList,
    {
        viewer: graphql`
            fragment UserList_viewer on Viewer {
                users(
                    query: $query
                    role: $role
                    first: $count
                    after: $cursor
                ) @connection(key: "UserList_users") {
                    totalCount
                    edges {
                        node {
                            id
                            ...User
                        }
                    }
                }
            }
        `,
    },
    {
        direction: 'forward',
        getConnectionFromProps(props) {
            return props.viewer && props.viewer.users;
        },
        getFragmentVariables(prevVars, totalCount) {
            return {
                ...prevVars,
                count: totalCount,
            };
        },
        getVariables(props, { count, cursor }, fragmentVariables) {
            return {
                count,
                cursor,
                role: fragmentVariables.role,
                query: fragmentVariables.query,
            };
        },
        query: initialQuery,
    },
);
