/* eslint-disable no-unused-vars,react/prop-types */
import React from 'react';
import { QueryRenderer, commitMutation } from 'react-relay';
import UserList, { initialQuery, ITEM_PER_PAGE } from './UserList';
import middleware, { ensureAdministrator } from '../../middlewares';

const title = 'User Management';

function action({ client }) {
    return {
        chunks: ['admin-user'],
        title,
        component: (
            <QueryRenderer
                environment={client.relay}
                query={initialQuery}
                variables={{ count: ITEM_PER_PAGE }}
                render={({ error, props }) => {
                    if (props) {
                        return (
                            <UserList
                                commitMutation={config =>
                                    commitMutation(client.relay, config)}
                                viewer={props.viewer}
                            />
                        );
                    }
                    return <div>Loading</div>;
                }}
            />
        ),
    };
}

export default middleware(ensureAdministrator)(action);
