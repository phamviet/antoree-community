import React from 'react';
import { Container } from 'reactstrap';
import Layout from 'components/Layout';
import middleware, {
    ensureAuthenticated,
    ensureModerator,
} from '../middlewares';

async function action({ userContext, next }) {
    const { component, ...route } = await next();

    const headerProps = {
        ...userContext,
    };

    const Component = () =>
        <Layout headerProps={headerProps} fullWidth>
            <Container className="my-3">
                {component}
            </Container>
        </Layout>;

    return {
        ...route,
        component: <Component />,
    };
}

export default middleware(ensureAuthenticated, ensureModerator)(action);
