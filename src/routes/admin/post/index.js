/* eslint-disable no-unused-vars,react/prop-types */
import React from 'react';
import { QueryRenderer, commitMutation } from 'react-relay';
import Dashboard, {
    initialQuery,
    initialStatus,
    ITEM_PER_PAGE,
} from './Dashboard';

const title = 'Post Management';

function action({ client }) {
    return {
        chunks: ['admin-post'],
        title,
        component: (
            <QueryRenderer
                environment={client.relay}
                query={initialQuery}
                variables={{ statuses: [initialStatus], count: ITEM_PER_PAGE }}
                render={({ error, props }) => {
                    if (props) {
                        return (
                            <Dashboard
                                commitMutation={config =>
                                    commitMutation(client.relay, config)}
                                viewer={props.viewer}
                            />
                        );
                    }
                    return <div>Loading</div>;
                }}
            />
        ),
    };
}

export default action;
