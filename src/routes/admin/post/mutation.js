import { graphql } from 'react-relay';

export default {
    changePostStatus: graphql`
        mutation mutation_changePostStatusMutation(
            $id: ID!
            $status: PostStatusEnum!
        ) {
            changePostStatus(id: $id, status: $status) {
                id
                status
                __typename
            }
        }
    `,
    togglePostFlag: graphql`
        mutation mutation_togglePostFlagMutation(
            $id: ID!
            $flag: PostFlagEnum!
        ) {
            togglePostFlag(id: $id, flag: $flag) {
                id
                isFeatured
            }
        }
    `,
    deletePost: graphql`
        mutation mutation_deletePostMutation($id: ID!) {
            deletePost(id: $id) {
                id
            }
        }
    `,
};
