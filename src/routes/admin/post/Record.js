/* eslint-disable react/prop-types */
/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import { graphql, createFragmentContainer } from 'react-relay';
import { FormattedRelative } from 'react-intl';
import {
    Row,
    Col,
    Media,
    Nav,
    NavItem,
    NavLink,
    NavDropdown,
    DropdownItem,
    DropdownToggle,
    DropdownMenu,
} from 'reactstrap';
import ExternalLinkIcon from 'react-icons/lib/fa/external-link';
import EditIcon from 'react-icons/lib/fa/edit';
import MdKeyboardControlIcon from 'react-icons/lib/md/keyboard-control';
import TrashIcon from 'react-icons/lib/fa/trash-o';
import Link from 'components/Link';
import { PUBLISHED, PENDING } from 'shared/constants';

function getActionItems({ post, onChangeStatus, onDelete }) {
    const navs = [];
    const { id, status } = post;

    navs.push(
        <NavLink
            onClick={() =>
                onChangeStatus({
                    id,
                    status: status === PUBLISHED ? PENDING : PUBLISHED,
                })}
            href="#"
        >
            {status === PUBLISHED ? 'Hide' : 'Approve'}
        </NavLink>,
    );

    navs.push(
        <NavLink title="Edit" tag={Link} to={`/compose/${id}`}>
            <EditIcon />
        </NavLink>,
    );

    if (status === PENDING) {
        navs.push(
            <NavLink onClick={() => onDelete(post)} href="#">
                <TrashIcon />
            </NavLink>,
        );
    }

    return navs;
}

function getDropdownActionItems({ post, onToggleFeatured }) {
    const navs = [];
    const { status, isFeatured } = post;

    if (status === PUBLISHED) {
        navs.push(
            <NavLink onClick={() => onToggleFeatured(post)} href="#">
                {isFeatured ? 'Unset' : 'Set'} Featured
            </NavLink>,
        );
    }

    return navs;
}

class RecordComponent extends React.Component {
    state = {
        dropdownOpen: false,
    };

    toggleDropdownMenu = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen,
        });
    };

    render() {
        const { data, ...props } = this.props;
        const { displayName, href, photo } = data;
        const actionItems = getActionItems({
            post: data,
            ...props,
        });

        const dropdownActionItems = getDropdownActionItems({
            post: data,
            ...props,
        });

        return (
            <div>
                <Row>
                    <Col xs="7">
                        <Media className="align-items-center">
                            <Media left target="_blank" href={href}>
                                {photo &&
                                    <Media
                                        object
                                        src={photo.src}
                                        alt={displayName}
                                    />}
                            </Media>
                            <Media className="px-2" body>
                                <Media heading tag="p">
                                    {displayName}{' '}
                                    <a target="_blank" href={href}>
                                        <ExternalLinkIcon />
                                    </a>
                                </Media>
                                <small>
                                    {
                                        <FormattedRelative
                                            value={
                                                data.publishedAt ||
                                                data.createdAt
                                            }
                                        />
                                    }
                                </small>
                            </Media>
                        </Media>
                    </Col>
                    <Col xs="5">
                        <Nav className="justify-content-end">
                            {dropdownActionItems.length > 0 &&
                                <NavDropdown
                                    isOpen={this.state.dropdownOpen}
                                    toggle={this.toggleDropdownMenu}
                                >
                                    <DropdownToggle nav>
                                        <MdKeyboardControlIcon />
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        {dropdownActionItems.map((action, i) =>
                                            <DropdownItem key={i}>
                                                {action}
                                            </DropdownItem>,
                                        )}
                                    </DropdownMenu>
                                </NavDropdown>}
                            {actionItems.map((action, i) =>
                                <NavItem key={i}>
                                    {action}
                                </NavItem>,
                            )}
                        </Nav>
                    </Col>
                </Row>
                <hr />
            </div>
        );
    }
}

RecordComponent.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.string.isRequired,
        displayName: PropTypes.string.isRequired,
        href: PropTypes.string.isRequired,
        status: PropTypes.string.isRequired,
    }).isRequired,
    onChangeStatus: PropTypes.func.isRequired,
    onToggleFeatured: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
};

export default createFragmentContainer(
    RecordComponent,
    graphql`
        fragment Record on Post {
            id
            displayName
            href
            status
            createdAt
            publishedAt
            isFeatured
            photo {
                src: url(w: 64, h: 64)
            }
        }
    `,
);
