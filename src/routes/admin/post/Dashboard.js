/* eslint-disable react/prop-types,react/sort-comp,no-underscore-dangle */
/* @flow */

import React from 'react';
import PropTypes from 'prop-types';
import { graphql, createPaginationContainer } from 'react-relay';
import { FormGroup } from 'reactstrap';
import { toastr } from 'react-redux-toastr';
import LoadMore from 'components/LoadMore';
import SearchInput from 'components/SearchInput';
import SelectButton from 'components/SelectButton';
import { PENDING, PUBLISHED } from 'shared/constants';
import { flags, FLAG_FEATURED } from 'shared/constants/post';
import Record from './Record';
import mutation from './mutation';

export const initialQuery = graphql`
    query DashboardQuery(
        $query: String
        $flag: PostFlagEnum
        $statuses: [PostStatusEnum]
        $count: Int
        $cursor: String
    ) {
        viewer {
            ...Dashboard_viewer
        }
    }
`;

export const initialStatus = PUBLISHED;
export const ITEM_PER_PAGE = 10;

const nodeDeleteConfig = {
    type: 'NODE_DELETE',
    deletedIDFieldName: 'id',
};

const filters = [
    {
        value: PUBLISHED,
        label: 'PUBLISHED',
    },
    {
        value: flags[FLAG_FEATURED],
        label: 'FEATURED',
    },
    {
        value: PENDING,
        label: 'PENDING',
    },
];

class Dashboard extends React.Component {
    static propTypes = {
        commitMutation: PropTypes.func.isRequired,
    };

    state = {
        loading: this.props.relay.isLoading(),
        status: initialStatus,
    };

    handleChangeStatus = variables => {
        this.props.commitMutation({
            mutation: mutation.changePostStatus,
            variables,
            configs: [nodeDeleteConfig],
        });
    };

    handleToggleFeatured = ({ id }) => {
        this.props.commitMutation({
            mutation: mutation.togglePostFlag,
            variables: { id, flag: flags[FLAG_FEATURED] },
            configs:
                this.state.status === flags[FLAG_FEATURED]
                    ? [nodeDeleteConfig]
                    : [],
            onCompleted: () => {
                toastr.success('Success!');
            },
        });
    };

    handleDelete = async ({ id }) => {
        const toastrConfirmOptions = {
            onOk: async () => {
                try {
                    await this.props.commitMutation({
                        mutation: mutation.deletePost,
                        variables: { id },
                        configs: [nodeDeleteConfig],
                    });
                } catch (error) {
                    console.error(error);
                }
            },
        };

        toastr.confirm(
            'Are you sure you want to delete this article?',
            toastrConfirmOptions,
        );
    };

    render() {
        const { posts = {} } = this.props.viewer;
        return (
            <div>
                <FormGroup className="text-center">
                    <SelectButton
                        options={filters}
                        isActive={status => this.state.status === status}
                        onChange={status =>
                            this._handleStatusFilterChange(status)}
                    />
                </FormGroup>
                <FormGroup>
                    <SearchInput
                        placeholder="Search"
                        size="lg"
                        onChange={query => this._handleQueryFilterChange(query)}
                    />
                </FormGroup>
                <small className="text-slate">
                    Total: {posts.totalCount}
                </small>
                <hr />
                {posts.edges.map(
                    ({ node }) =>
                        node &&
                        <Record
                            key={node.id}
                            data={node}
                            onChangeStatus={this.handleChangeStatus}
                            onToggleFeatured={this.handleToggleFeatured}
                            onDelete={this.handleDelete}
                        />,
                )}
                <LoadMore
                    visible={this.canLoadMore()}
                    onClick={() => this._loadMore()}
                />
            </div>
        );
    }

    canLoadMore() {
        return this.props.relay.hasMore() && !this.state.loading;
    }

    _handleStatusFilterChange(status) {
        this.setState({ status, loading: true }, () =>
            this._reFetch(
                status === flags[FLAG_FEATURED]
                    ? { flag: status, statuses: null }
                    : { statuses: [status], flag: null },
            ),
        );
    }

    _handleQueryFilterChange(query) {
        this.setState({ loading: true }, () => this._reFetch({ query }));
    }

    _reFetch(variables) {
        this.props.relay.refetchConnection(
            null,
            () => this.setState({ loading: false }),
            { ...variables, count: ITEM_PER_PAGE },
        );
    }

    _loadMore() {
        if (!this.canLoadMore()) {
            return;
        }

        this.props.relay.loadMore(ITEM_PER_PAGE, () =>
            this.setState({ loading: false }),
        );
    }
}

export default createPaginationContainer(
    Dashboard,
    {
        viewer: graphql`
            fragment Dashboard_viewer on Viewer {
                posts: search(
                    query: $query
                    flag: $flag
                    statuses: $statuses
                    first: $count
                    after: $cursor
                ) @connection(key: "PostContainer_posts") {
                    totalCount
                    edges {
                        node {
                            id
                            ...Record
                        }
                    }
                }
            }
        `,
    },
    {
        direction: 'forward',
        getConnectionFromProps(props) {
            return props.viewer && props.viewer.posts;
        },
        getFragmentVariables(prevVars, totalCount) {
            return {
                ...prevVars,
                count: totalCount,
            };
        },
        getVariables(props, { count, cursor }, fragmentVariables) {
            return {
                count,
                cursor,
                // in most cases, for variables other than connection filters like
                // `first`, `after`, etc. you may want to use the previous values.
                statuses: fragmentVariables.statuses,
                flag: fragmentVariables.flag,
                query: fragmentVariables.query,
            };
        },
        query: initialQuery,
    },
);
