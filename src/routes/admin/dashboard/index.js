const title = 'Dashboard';

function action() {
    return {
        chunks: ['admin-dashboard'],
        title,
        component: null,
    };
}

export default action;
