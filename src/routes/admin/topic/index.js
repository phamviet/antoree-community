import React from 'react';
import ManageTopic from './ManageTopic';

const title = 'Topic Management';

function action() {
    return {
        chunks: ['admin-topic'],
        title,
        component: <ManageTopic />,
    };
}

export default action;
