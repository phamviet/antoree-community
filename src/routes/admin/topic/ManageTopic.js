/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ReactList from 'react-list';
import {
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
    Col,
} from 'reactstrap';
import AddIcon from 'react-icons/lib/md/add';
import RemoveIcon from 'react-icons/lib/md/remove';
import { HOME_TOPICS_MENU, HOME_FEATURED_TOPICS } from 'shared/constants/topic';
import topicsQuery from './topicsQuery.graphql';
import collectionEntitiesQuery from '../../../shared/queries/collectionEntitiesQuery.graphql';
import toggleCollection from './toggleCollection.graphql';

const ALL = 'ALL';

const tabs = [
    {
        name: ALL,
        label: 'All Topics',
    },
    {
        name: HOME_TOPICS_MENU,
        label: 'Home Menu',
    },
    {
        name: HOME_FEATURED_TOPICS,
        label: 'Home Featured Topics',
    },
];

class ManageTopic extends React.Component {
    static contextTypes = {
        client: PropTypes.object.isRequired,
    };

    state = {
        activeTab: ALL,
        initialized: {
            [ALL]: true,
            [HOME_TOPICS_MENU]: false,
            [HOME_FEATURED_TOPICS]: false,
        },
        collections: {
            [HOME_TOPICS_MENU]: [],
            [HOME_FEATURED_TOPICS]: [],
        },
        offset: 0,
        topics: [],
        collectionsTopics: {
            [HOME_TOPICS_MENU]: {},
            [HOME_FEATURED_TOPICS]: {},
        },
    };

    componentDidMount() {
        this.fetchTopics();
        this.fetchCollectionTopics(HOME_TOPICS_MENU);
        this.fetchCollectionTopics(HOME_FEATURED_TOPICS);
    }

    getCollectionTopics = name =>
        this.state.collections[name].map(
            id => this.state.collectionsTopics[name][id],
        );

    fetchCollectionTopics = async name => {
        try {
            const resp = await this.context.client.query({
                query: collectionEntitiesQuery,
                variables: { name },
            });

            const { entities } = resp.data.collection;
            const topics = {};
            const topicIds = [];
            (entities || []).forEach(topic => {
                topics[topic.id] = topic;
                topicIds.push(topic.id);
            });

            this.setState({
                collectionsTopics: {
                    ...this.state.collectionsTopics,
                    [name]: {
                        ...this.state.collectionsTopics[name],
                        ...topics,
                    },
                },
                collections: {
                    ...this.state.collections,
                    [name]: [...this.state.collections[name], ...topicIds],
                },
            });
        } catch (error) {
            console.error(error);
        }
    };

    fetchTopics = async (name = '') => {
        try {
            const resp = await this.context.client.query({
                query: topicsQuery,
                variables: { name },
            });

            if (name) {
                this.setState({ topics: resp.data.topics });
            } else {
                this.setState({
                    topics: this.state.topics.concat(resp.data.topics),
                });
            }
        } catch (error) {
            console.error(error);
        }
    };

    updateTopic = topic =>
        this.setState({
            topics: this.state.topics.map(
                p => (p.id === topic.id ? { ...p, ...topic } : p),
            ),
        });

    toggle = tab => {
        const initialized = this.state.initialized[tab];
        if (!initialized) {
            this.setState({
                initialized: { ...this.state.initialized, [tab]: true },
            });

            if (tab !== ALL) {
                // this.fetchCollectionTopics(tab);
            }
        }

        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab,
            });
        }
    };

    handleToggleCollection = async (topic, name) => {
        try {
            await this.context.client.mutate({
                mutation: toggleCollection,
                variables: {
                    entityId: topic.id,
                    entityType: 'TOPIC',
                    name,
                },
            });
            const { collections, collectionsTopics } = this.state;
            if (collections[name].indexOf(topic.id) > -1) {
                this.setState({
                    collections: {
                        ...collections,
                        [name]: collections[name].filter(id => id !== topic.id),
                    },
                });
            } else {
                this.setState({
                    collections: {
                        ...collections,
                        [name]: [...collections[name], topic.id],
                    },
                    collectionsTopics: {
                        ...collectionsTopics,
                        [name]: {
                            ...collectionsTopics[name],
                            [topic.id]: topic,
                        },
                    },
                });
            }
        } catch (error) {
            console.error(error);
            // throw new Error('Failed to change topic status.');
        }
    };

    // eslint-disable-next-line no-unused-vars
    handleDelete = topic => {};

    renderTopic = (index, key, tab) => {
        const { collections, collectionsTopics } = this.state;
        const topic =
            tab === ALL
                ? this.state.topics[index]
                : collectionsTopics[tab][collections[tab][index]];

        return (
            <div key={key} className="py-2">
                <Row>
                    <Col xs="8">
                        {topic.displayName}
                    </Col>
                    <Col xs="4">
                        {tab === ALL
                            ? <Nav className="justify-content-end">
                                  <NavItem>
                                      <NavLink
                                          onClick={() =>
                                              this.handleToggleCollection(
                                                  topic,
                                                  HOME_TOPICS_MENU,
                                              )}
                                          href="#"
                                      >
                                          {collections[
                                              HOME_TOPICS_MENU
                                          ].indexOf(topic.id) === -1
                                              ? <AddIcon />
                                              : <RemoveIcon />}{' '}
                                      </NavLink>
                                  </NavItem>
                                  <NavItem>
                                      <NavLink
                                          onClick={() =>
                                              this.handleToggleCollection(
                                                  topic,
                                                  HOME_FEATURED_TOPICS,
                                              )}
                                          href="#"
                                      >
                                          {collections[
                                              HOME_FEATURED_TOPICS
                                          ].indexOf(topic.id) === -1
                                              ? <AddIcon />
                                              : <RemoveIcon />}{' '}
                                      </NavLink>
                                  </NavItem>
                              </Nav>
                            : <Nav className="justify-content-end">
                                  <NavItem>
                                      <NavLink
                                          onClick={() =>
                                              this.handleToggleCollection(
                                                  topic,
                                                  tab,
                                              )}
                                          href="#"
                                      >
                                          <RemoveIcon />
                                      </NavLink>
                                  </NavItem>
                              </Nav>}
                    </Col>
                </Row>
                <hr />
            </div>
        );
    };

    render() {
        return (
            <div>
                <Nav tabs>
                    {tabs.map(({ name, label }) =>
                        <NavItem key={name}>
                            <NavLink
                                className={classnames({
                                    active: this.state.activeTab === name,
                                })}
                                role="button"
                                onClick={() => {
                                    this.toggle(name);
                                }}
                            >
                                {label}
                            </NavLink>
                        </NavItem>,
                    )}
                </Nav>
                <TabContent className="py-3" activeTab={this.state.activeTab}>
                    {tabs.map(({ name }) =>
                        <TabPane tabId={name} key={name}>
                            <ReactList
                                status={name}
                                itemRenderer={(...args) =>
                                    this.renderTopic(...args, name)}
                                length={
                                    name === ALL
                                        ? this.state.topics.length
                                        : this.state.collections[name].length
                                }
                            />
                        </TabPane>,
                    )}
                </TabContent>
            </div>
        );
    }
}

export default ManageTopic;
