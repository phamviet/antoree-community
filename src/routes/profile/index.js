import React from 'react';
import { STATUS_PUBLISHED } from 'shared/constants/post';
import Layout from 'components/Layout';
import Profile from './Profile';
import profileQuery from './profile.graphql';
import timelineQuery from './timelineQuery.graphql';
import bookmarksQuery from './bookmarksQuery.graphql';

async function action(
    { client, store, queries: { collectionSet } },
    { userId },
) {
    let profile;
    try {
        const { data } = await client.query({
            query: profileQuery,
            variables: { id: userId },
        });
        profile = data.profile;
    } catch (error) {
        throw error;
    }

    if (!profile) {
        return {
            redirect: '/404',
        };
    }

    const handleLoadTab = async tab => {
        if (tab === 'timeline') {
            const statuses = [STATUS_PUBLISHED];
            const state = store.getState();
            if (state.user && state.user.id === profile.id) {
                // statuses.push(STATUS_PENDING);
            }

            try {
                const { data } = await client.query({
                    query: timelineQuery,
                    variables: { id: profile.id, statuses },
                });
                collectionSet(`timeline-${userId}`, data.posts);
            } catch (error) {
                throw error;
            }
        } else if (tab === 'bookmarks') {
            try {
                const { data } = await client.query({
                    query: bookmarksQuery,
                    fetchPolicy: 'network-only',
                });
                collectionSet(`bookmarks-${userId}`, data.viewer.bookmarks);
            } catch (error) {
                throw error;
            }
        }
    };

    return {
        chunks: ['profile'],
        title: profile.displayName,
        component: (
            <Layout fullWidth>
                <Profile profile={profile} onLoadTab={handleLoadTab} />
            </Layout>
        ),
    };
}

export default action;
