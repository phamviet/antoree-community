import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { compose } from 'redux';
import { connect } from 'react-redux';
import {
    TabContent,
    Container,
    TabPane,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import ProfileHeader from 'components/Profile/ProfileHeader';
import { AvatarComponent } from 'components/Avatar';

import { Timeline, Bookmarks } from 'components/Profile';
import s from './Profile.css';

const defaultTabs = [
    {
        name: 'timeline',
        label: 'Timeline',
        render: props => <Timeline {...props} />,
    },
];

// eslint-disable-next-line no-unused-vars
const teacherTabs = [
    {
        name: 'teaching',
        label: 'Teaching Profile',
    },
    {
        name: 'review',
        label: 'Reviews',
    },
];

const authenticatedTabs = [
    {
        name: 'bookmarks',
        label: 'Bookmarks',
        render: props => <Bookmarks {...props} />,
    },
];

class Profile extends React.Component {
    static propTypes = {
        profile: PropTypes.shape({
            id: PropTypes.string.isRequired,
            displayName: PropTypes.string.isRequired,
            ...AvatarComponent.propTypes,
        }).isRequired,
        // eslint-disable-next-line react/no-unused-prop-types
        user: PropTypes.shape({
            id: PropTypes.string.isRequired,
        }),
        onLoadTab: PropTypes.func.isRequired,
    };

    static defaultProps = {
        timelinePosts: [],
        user: null,
    };

    state = {
        isMyProfile: false,
        activeTab: undefined,
        tabs: defaultTabs,
        initialized: {},
    };

    componentDidMount() {
        this.refreshState(this.props);
        this.toggle('timeline');
    }

    componentWillReceiveProps(props) {
        this.refreshState(props);
    }

    refreshState(props) {
        const { user, profile } = props;
        const isMyProfile = user && user.id === profile.id;
        let tabs = defaultTabs;
        if (isMyProfile) {
            tabs = tabs.concat(authenticatedTabs);
        }

        this.setState({
            isMyProfile,
            tabs,
        });
    }

    toggle = tab => {
        if (this.state.activeTab !== tab) {
            const { initialized } = this.state;
            if (!initialized[tab]) {
                this.props.onLoadTab(tab);
            }

            this.setState({
                activeTab: tab,
                initialized: { ...initialized, [tab]: true },
            });
        }
    };

    render() {
        const { profile } = this.props;
        const { tabs } = this.state;
        return (
            <div className="mt-4">
                <div className={s.header}>
                    <Container>
                        <ProfileHeader {...profile} />
                        <Nav
                            tabs
                            className={classNames(
                                s.tabs,
                                'justify-content-center',
                            )}
                        >
                            {tabs.map(({ name, label }) =>
                                <NavItem key={name}>
                                    <NavLink
                                        role="button"
                                        className={classNames({
                                            active:
                                                this.state.activeTab === name,
                                        })}
                                        onClick={() => {
                                            this.toggle(name);
                                        }}
                                    >
                                        {label}
                                    </NavLink>
                                </NavItem>,
                            )}
                        </Nav>
                    </Container>
                </div>

                <div className={s.content}>
                    <Container>
                        <div className="d-flex justify-content-center">
                            <TabContent activeTab={this.state.activeTab}>
                                {tabs.map(({ name, render }) =>
                                    <TabPane tabId={name} key={name}>
                                        {render(this.props)}
                                    </TabPane>,
                                )}
                            </TabContent>
                        </div>
                    </Container>
                </div>
            </div>
        );
    }
}

export default compose(
    connect(({ user }) => ({
        user,
    })),
    withStyles(s),
)(Profile);
