/* eslint-disable no-console */
import Sequelize from 'sequelize';
import noop from 'lodash/noop';
import config from '../config';

const sequelize = new Sequelize(config.databaseUrl, {
    define: {
        freezeTableName: true,
    },
    logging: process.env.NODE_ENV !== 'production' ? console.log : noop,
});

export default sequelize;
