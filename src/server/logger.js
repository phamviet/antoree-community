const winston = require('winston');
const slackWinston = require('slack-winston').Slack;

if (process.env.NODE_ENV === 'production') {
    winston.add(slackWinston, {
        webhook_url:
            'https://hooks.slack.com/services/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',
        channel: 'community',
        username: 'Antoree Live',
        icon_emoji: ':white_check_mark:',
        level: 'info',
    });
}

export default winston;
