// For query builder only
import knex from 'knex';
import config from '../config';

export default knex({
    client: 'mysql',
    connection: config.databaseUrl,
    debug: __DEV__,
});
