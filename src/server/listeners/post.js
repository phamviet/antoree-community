import events from '../events';
import logger from '../logger';

export default () => {
    events.on('post:published', post =>
        logger.info('New post published', { displayName: post.displayName }),
    );
};
