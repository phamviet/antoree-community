/**
 * Antoree Community
 *
 * Copyright © 2017-present Antoree
 */

import path from 'path';
import Promise from 'bluebird';
import express from 'express';
import moment from 'moment-timezone';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import requestLanguage from 'express-request-language';
import helmet from 'helmet';
import responseTime from 'response-time';
import { IntlProvider } from 'react-intl';
import AuthenticationError from 'passport/lib/errors/authenticationerror';
import { AuthorizationError } from 'passport-oauth2';
import httpsRedirect from 'express-https-redirect';
import session from 'express-session';
import flash from 'express-flash';
import compress from 'compression';
import slashes from 'connect-slashes';
import SequelizeSession from 'connect-session-sequelize';
import nodeFetch from 'node-fetch';
import React from 'react';
import ReactDOM from 'react-dom/server';
import omit from 'lodash/omit';
import { getDataFromTree } from 'react-apollo';
import PrettyError from 'pretty-error';
import createApolloClient from '../core/createApolloClient';
import { db } from './models';
import listeners from './listeners';
import App from '../components/App';
import Html from '../components/Html';
import { ErrorPageWithoutStyle } from '../routes/error/ErrorPage';
import errorPageStyle from '../routes/error/ErrorPage.css';
import createFetch from '../createFetch';
import router from '../router';
import passport from './passport';
import upload, { uploadFile, serveUploadedFile } from './upload';
import bugsnag from './utils/bugsnag';
import utils from './utils';
import assets from './assets.json'; // eslint-disable-line import/no-unresolved
import configureStore from '../store/configureStore';
import { setRuntimeVariable } from '../actions/runtime';
import { setLocale } from '../actions/intl';
import createQuery from '../shared/queries';
import config from '../config';
import sitemap from './sitemap';
import auth from './auth';
import graphql, { graphqlOptions } from './graphql';

//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

/**
 * force UTC
 *   - you can require moment or moment-timezone, both is configured to UTC
 *   - you are allowed to use new Date() to instantiate datetime values for models, because they are transformed into UTC in the model layer
 *   - be careful when not working with models, every value from the native JS Date is local TZ
 *   - be careful when you work with date operations, therefor always wrap a date into moment
 */
moment.tz.setDefault('UTC');

const app = express();
const production = app.get('env') === 'production';
const SequelizeStore = SequelizeSession(session.Store);

app.enable('trust proxy');
app.disable('etag');

//
// Register Node.js middleware
// -----------------------------------------------------------------------------

if (production) {
    app.use(httpsRedirect());
}

app.use(
    helmet({
        hsts: {
            includeSubDomains: true,
            preload: true,
            setIf: req =>
                req.secure || req.headers['x-forwarded-proto'] === 'https',
        },
        contentSecurityPolicy: {
            directives: {
                defaultSrc: ['* data: blob:'],
                scriptSrc: [
                    "'self'",
                    '*.community.antoree.com',
                    '*.google-analytics.com',
                    '*.facebook.com',
                    '*.facebook.net',
                    'cdn.jsdelivr.net', // GraphiQL
                    'd2wy8f7a9ursnm.cloudfront.net', // Bugsnag
                    "'unsafe-eval'",
                    "'unsafe-inline'",
                ],
                styleSrc: [
                    "'self'",
                    '*.community.antoree.com',
                    'fonts.googleapis.com',
                    'diegoddox.github.io', // todo
                    'cdn.jsdelivr.net', // GraphiQL
                    "'unsafe-inline'",
                ],
            },
        },
    }),
);

app.use(responseTime());
app.use(compress());
app.use(
    express.static(path.resolve(__dirname, 'public'), {
        maxAge: '7 days',
        etag: false,
    }),
);

// Public app, mount it ASAP
sitemap(app);
app.use(cookieParser());
app.use(
    requestLanguage({
        languages: config.locales,
        queryName: 'lang',
        cookie: {
            name: 'lang',
            options: {
                path: '/',
                maxAge: utils.ONE_YEAR_MS * 10,
            },
            url: '/lang/{language}',
        },
    }),
);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

if (production) {
    app.use(bugsnag.requestHandler);
}

// ### Caching todo
// app.use(cacheControl('public'));

//
// Authentication
// -----------------------------------------------------------------------------
app.use(
    session({
        secret: config.secretKey,
        store: new SequelizeStore({
            db,
        }),
        name: 'sid',
        resave: false,
        saveUninitialized: true,
        expiration: utils.ONE_WEEK_MS,
        proxy: true, // if you do SSL outside of node.
        httpOnly: true,
        secure: production,
    }),
);

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// start routes
app.use(
    slashes(true, {
        headers: { 'Cache-Control': `public, max-age=${utils.ONE_YEAR_S}` },
    }),
);

// Authentication
auth(app);

//
// Register API middleware
// -----------------------------------------------------------------------------
app.post('/api/file', upload.single('file'), uploadFile());
app.get('/file/:id', serveUploadedFile());

// graphql server
graphql(app);

const initUser = req => {
    const user = req.user;

    if (!user) {
        return undefined;
    }

    return {
        ...user.dataValues,
        photo: { ...user.photo.dataValues },
        isEditor: user.isEditor(),
        isAdministrator: user.isAdministrator(),
    };
};

// filter out heavy data load
const getClientState = state => omit(state, ['entities', 'collections']);
//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
app.get('*', async (req, res, next) => {
    try {
        const css = new Set();

        const apolloClient = createApolloClient(graphqlOptions(req));

        // Universal HTTP client
        const fetch = createFetch(nodeFetch, {
            baseUrl: config.api.serverUrl,
            cookie: req.headers.cookie,
        });

        const serverMessages = req.flash();
        const initialState = {
            user: initUser(req),
            runtime: { messages: serverMessages },
        };

        const store = configureStore(initialState, {
            cookie: req.headers.cookie,
            apolloClient,
            fetch,
            // I should not use `history` on server.. but how I do redirection? follow universal-router
            history: null,
        });

        store.dispatch(
            setRuntimeVariable({
                name: 'startTime',
                value: Date.now(),
            }),
        );

        store.dispatch(
            setRuntimeVariable({
                name: 'availableLocales',
                value: config.locales,
            }),
        );

        const locale = req.language;
        const intl = await store.dispatch(
            setLocale({
                locale,
            }),
        );

        // Global (context) variables that can be easily accessed from any React component
        // https://facebook.github.io/react/docs/context.html
        const context = {
            // Enables critical path CSS rendering
            // https://github.com/kriasoft/isomorphic-style-loader
            insertCss: (...styles) => {
                // eslint-disable-next-line no-underscore-dangle
                styles.forEach(style => css.add(style._getCss()));
            },
            // Universal HTTP client
            fetch,
            // Apollo Client for use with react-apollo
            client: apolloClient,
            store,
            queries: createQuery({ store, client: apolloClient }),
            storeSubscription: null,
            intl,
        };

        const route = await router.resolve({
            ...context,
            path: req.path,
            query: req.query,
            locale,
        });

        if (route.redirect) {
            res.redirect(route.status || 302, route.redirect);
            return;
        }

        const data = { ...route };
        const rootComponent = (
            <App context={context} store={store}>
                {route.component}
            </App>
        );
        await getDataFromTree(rootComponent);
        // this is here because of Apollo redux APOLLO_QUERY_STOP action
        await Promise.delay(0);

        data.children = await ReactDOM.renderToString(rootComponent);
        data.styles = [{ id: 'css', cssText: [...css].join('') }];
        data.scripts = [assets.vendor.js];
        if (route.chunks) {
            data.scripts.push(...route.chunks.map(chunk => assets[chunk].js));
        }
        data.scripts.push(assets.client.js);

        // Furthermore invoked actions will be ignored, client will not receive them!
        if (__DEV__) {
            // eslint-disable-next-line no-console
            console.log('Serializing store...');
        }

        data.app = {
            apiUrl: config.api.clientUrl,
            state: getClientState(context.store.getState()),
            IMAGE_DOMAIN: process.env.IMAGE_DOMAIN,
            lang: locale,
            analytics: config.analytics,
        };
        data.user = req.user;

        const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);
        res.status(route.status || 200);
        res.send(`<!doctype html>${html}`);
    } catch (err) {
        next(err);
    }
});

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

// bugsnag
if (process.env.NODE_ENV === 'production') {
    app.use(bugsnag.errorHandler);
}

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    const locale = req.language;
    // catch passport error
    if (
        err instanceof AuthenticationError ||
        err instanceof AuthorizationError
    ) {
        req.logout();
        // todo req.flash is undefined => Get it back!
        // eslint-disable-next-line no-unused-expressions
        req.flash && req.flash('error', err.message);
        res.redirect('/login');
        return;
    }

    console.error(pe.render(err));
    const html = ReactDOM.renderToStaticMarkup(
        <Html
            title="Internal Server Error"
            description={err.message}
            styles={[{ id: 'css', cssText: errorPageStyle._getCss() }]} // eslint-disable-line no-underscore-dangle
            app={{ lang: locale }}
            user={req.user}
        >
            {ReactDOM.renderToString(
                <IntlProvider locale={locale}>
                    <ErrorPageWithoutStyle error={err} />
                </IntlProvider>,
            )}
        </Html>,
    );
    res.status(err.status || 500);
    res.send(`<!doctype html>${html}`);
});

// Event listener
listeners.listen();

export default app;
