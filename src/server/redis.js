/* @flow */

import redis from 'redis';
import bluebird from 'bluebird';

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

let client;

export default {
    getClient() {
        if (!client) {
            client = redis.createClient(process.env.REDIS_URL);
            client.on('error', err => console.log(err)); // eslint-disable-line no-console
        }

        return client;
    },
    quit(cb) {
        if (client) {
            client.quit(cb);
        } else {
            cb();
        }
    },
};
