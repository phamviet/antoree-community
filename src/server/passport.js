/* eslint-disable no-underscore-dangle */
/* @flow */

import _debug from 'debug';
import moment from 'moment';
import passport from 'passport';
import AuthenticationError from 'passport/lib/errors/authenticationerror';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import { Strategy as LinkedInStrategy } from 'passport-linkedin-oauth2';
import { EDITOR } from 'shared/constants/roles';
import { User, UserLogin } from './models';
import AntoreeStrategy from './utils/antoree-passport-strategy';
import config from '../config';

const debug = _debug('app:passport');
const ANTOREE = 'antoree';
const FACEBOOK = 'facebook';

type UserType = {
    id: string,
    email: string,
};

type ProfileType = {
    email: string,
    displayName: string,
    picture: string,
    locale: string,
    timezone: string,
};

type AntoreeAccount = {
    id: number,
    email: string,
    email_verified: number, // 1
    name: string,
    shown_name: string,
    display_name: string,
    first_name: string,
    last_name: string,
    url_avatar: string,
    roles: [{ id: number, name: string }],
    permission_names: [string],
    settings: {
        country: string, // US
        currency: string, // US
        locale: string, // en
        timezone: string,
    },
    _utils: {
        shortDateJsFormat: string,
    },
    gender: number, // 1
    birth_day: string,
    member_since: string,
    skype: ?string,
    address: ?string,
    phone_number: string,
    nationality: ?{
        id: number,
        name: string,
        code: string,
    },
    country_state: ?{
        id: number,
        name: string,
    },
    city: ?{
        id: number,
        name: string,
    },
    teacher_profile: ?{
        old_id: number,
    },
};

// Creates or updates the external login credentials
// and returns the currently authenticated user.
async function login(
    req,
    provider: string,
    key: string,
    profile: ProfileType,
    tokens: any,
): User {
    let user;

    // direct lookup
    if (req.user) {
        user = await User.findById(req.user.id);
    }

    // via login history
    if (!user) {
        const users = await User.findAll({
            where: {
                '$logins.provider$': provider,
                '$logins.key$': key,
            },
            include: [
                {
                    attributes: ['provider', 'key'],
                    model: UserLogin,
                    as: 'logins',
                    required: true,
                },
            ],
        });

        if (users.length) {
            user = users[0];
        }
    }

    // again with email
    if (!user) {
        const result = await User.findOrCreate({
            where: { email: profile.email },
            defaults: profile,
        });

        user = result[0];
    }

    const loginKeys = { userId: user.id, provider, key };
    const logins = await UserLogin.count({ where: loginKeys });

    if (logins === 0) {
        await UserLogin.create({
            ...loginKeys,
            tokens: JSON.stringify(tokens),
        });
    }

    return user;
}

/**
 * Sign in with Facebook.
 */
passport.use(
    new FacebookStrategy(
        {
            clientID: config.auth.facebook.id,
            clientSecret: config.auth.facebook.secret,
            callbackURL: '/login/facebook/return',
            profileFields: ['displayName', 'email', 'locale', 'timezone'],
            passReqToCallback: true,
        },
        async (req, accessToken, refreshToken, profile, done) => {
            try {
                const user = await login(
                    req,
                    FACEBOOK,
                    profile.id,
                    {
                        email:
                            profile._json.email || `${profile.id}@facebook.com`,
                        displayName: profile.displayName,
                        locale: profile._json.locale,
                        timezone: profile._json.timezone,
                    },
                    { accessToken, refreshToken },
                );
                user.setProfilePicture(
                    `https://graph.facebook.com/${profile.id}/picture?type=large`,
                );
                done(null, user);
            } catch (err) {
                console.error(err);
                done(err);
            }
        },
    ),
);

passport.use(
    new AntoreeStrategy(
        {
            tokenURL: config.auth.antoree.tokenURL,
            profileURL: config.auth.antoree.profileURL,
            clientID: config.auth.antoree.id,
            clientSecret: config.auth.antoree.secret,
            passReqToCallback: true,
        },
        async (
            req,
            accessToken,
            refreshToken,
            params,
            profile: AntoreeAccount,
            done,
        ) => {
            debug('Antoree account connected');
            debug(profile);
            try {
                const dateFormat = profile._utils
                    ? profile._utils.shortDateJsFormat
                    : 'DD/MM/YYYY';
                const gender = parseInt(profile.gender, 10);
                const user = await login(
                    req,
                    ANTOREE,
                    profile.id,
                    {
                        email: profile.email,
                        role: profile.roles.some(({ name }) => name === 'admin')
                            ? EDITOR
                            : 0,
                        isTeacher: profile.permission_names.some(
                            name => name === 'be-teacher',
                        ),
                        externalTeacherId: profile.teacher_profile
                            ? profile.teacher_profile.old_id
                            : null,
                        createdAt: moment(profile.member_since, dateFormat),
                    },
                    { accessToken, refreshToken },
                );
                const settings = profile.settings || {};

                await user.update({
                    displayName: profile.display_name,
                    gender: gender > 1 ? '1' : gender.toString(),
                    address: profile.address,
                    birthday: new Date(profile.birth_day),
                    country:
                        settings.country && settings.country !== '--'
                            ? settings.country.toUpperCase()
                            : null,
                    nationality:
                        profile.nationality &&
                        profile.nationality.code.toUpperCase(),
                    city: profile.city && profile.city.name,
                    locale: settings.locale,
                    timezone: settings.timezone,
                });
                user.setProfilePicture(profile.url_avatar);
                done(null, user);
            } catch (err) {
                console.error(err);
                done(err);
            }
        },
    ),
);

// https://github.com/jaredhanson/passport-google-oauth2
passport.use(
    new GoogleStrategy(
        {
            clientID: config.auth.google.id,
            clientSecret: config.auth.google.secret,
            callbackURL: '/login/google/return',
            passReqToCallback: true,
        },
        async (req, accessToken, refreshToken, profile, done) => {
            try {
                const user = await login(
                    req,
                    'google',
                    profile.id,
                    {
                        email: profile.emails[0].value,
                        displayName: profile.displayName,
                        gender: profile.gender === 'male' ? '1' : '0',
                        locale: profile._json.language,
                    },
                    { accessToken, refreshToken },
                );

                if (!user.photoId) {
                    user.setProfilePicture(profile.photos[0].value);
                }

                done(null, user);
            } catch (err) {
                console.error(err);
                done(err);
            }
        },
    ),
);

// https://github.com/auth0/passport-linkedin-oauth2
passport.use(
    new LinkedInStrategy(
        {
            clientID: config.auth.linkedin.id,
            clientSecret: config.auth.linkedin.secret,
            callbackURL: '/login/linkedin/return',
            passReqToCallback: true,
            scope: ['r_emailaddress', 'r_basicprofile'],
        },
        async (req, accessToken, refreshToken, profile, done) => {
            if (!profile._json.emailAddress) {
                return done(new Error('[linkedin] No associated emails found'));
            }

            try {
                const user = await login(
                    req,
                    'linkedin',
                    profile.id,
                    {
                        email: profile._json.emailAddress,
                        displayName: profile.displayName,
                    },
                    { accessToken, refreshToken },
                );

                if (!user.photoId) {
                    user.setProfilePicture(profile._json.pictureUrl);
                }

                return done(null, user);
            } catch (err) {
                return done(err);
            }
        },
    ),
);

passport.serializeUser((user: UserType, done): void => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done): void => {
    let error = null;
    let user = null;
    try {
        user = await User.findById(id, { include: 'photo' });
        if (!user) {
            error = new AuthenticationError('Please login again');
        } else if (user.disabled) {
            error = new AuthenticationError(
                'Your account has been disabled. Please contact administrator for assistance',
            );
        }

        // todo locked check
    } catch (err) {
        error = err;
    }

    return done(error, user);
});

export default passport;
