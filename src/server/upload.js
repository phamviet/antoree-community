/* eslint-disable consistent-return */
import multer, { diskStorage, memoryStorage } from 'multer';
import aws from 'aws-sdk';
import uuid from 'uuid';
import multerS3 from 'multer-s3';
import path from 'path';
import mkdirp from 'mkdirp';
import config from '../config';
import { User, File } from './models';

const { storage } = config;
aws.config.update({ region: 'ap-southeast-1' });
const storageOpts = {
    disk: diskStorage({
        destination: (req, file, cb) => {
            let basePath = storage.dest;
            if (req.user && req.user.id) {
                basePath = path.join(basePath, req.user.id);
            }

            mkdirp.sync(basePath);

            cb(null, basePath);
        },
    }),
    memory: memoryStorage(),
    s3: multerS3({
        s3: new aws.S3(),
        bucket: storage.s3Bucket,
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: (req, file, cb) => {
            cb(null, `${config.appEnv}/${uuid.v4()}`);
        },
    }),
};

const opt = storageOpts[storage.adapter];
if (!opt) {
    throw new Error('Storage configuration was not setup correctly');
}

const upload = multer({ storage: opt });

const uploadFile = () => async (req, res, next) => {
    if (req.user) {
        const user = await User.findById(req.user.id);
        if (user) {
            try {
                const file = await File.receive(user, req.file, req.body);
                return res.json(file);
            } catch (err) {
                return next(err);
            }
        }
    }

    return res.sendStatus(403);
};

const serveUploadedFile = (opts = {}) => async (req, res, next) => {
    if (!req.params.id) {
        throw new TypeError('ID param is required');
    }

    const file = await File.findById(req.params.id);
    if (!file) {
        return res.sendStatus(404);
    }

    if (storage.adapter !== 'disk') {
        // todo support other adapters
        next();
    }

    res.sendFile(
        file.filename,
        {
            headers: {
                'Content-Type': file.mimeType || 'application/octet-stream',
            },
            root: path.join(storage.dest, file.userId),
            ...opts,
        },
        err => {
            if (err) {
                next(err);
            }
        },
    );
};

export { uploadFile, serveUploadedFile };

export default upload;
