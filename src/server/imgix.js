import ImgixClient from 'imgix-core-js';
import assign from 'lodash/assign';
import config from '../config';

const clients = {};
const cdnIndex = [1, 2];

function getRandomCdnIndex() {
    return cdnIndex[Math.floor(Math.random() * cdnIndex.length)];
}

function getClient(index) {
    if (!clients[index]) {
        clients[index] = new ImgixClient(
            assign(config.imgix, {
                host: `antoree-community-images-${index}.imgix.net`,
            }),
        );
    }

    return clients[index];
}

function buildURL(path, params) {
    if (!path) {
        return null;
    }

    const index = getRandomCdnIndex();
    const client = getClient(index);

    return client.buildURL(path, params);
}

const imgx = { buildURL };
export default imgx;
