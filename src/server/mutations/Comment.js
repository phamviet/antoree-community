import { UserError } from 'graphql-errors';
import { Comment } from '../models';
import CommentType from '../types/CommentType';
import CommentInputType from '../types/CommentInputType';

export const saveComment = {
    type: CommentType,
    description: 'Create a comment',
    args: {
        input: { type: CommentInputType },
    },
    async resolve(_, { input }, { user }) {
        if (!user) {
            throw new UserError(
                'Session timed out. Please login and try again.',
            );
        }

        const { id, ...data } = input;
        let comment;

        if (id) {
            comment = await Comment.findById(id);
            if (!comment) {
                throw new Error(`Comment with id "${id}" could not be found`);
            }

            await comment.update(data);
        } else {
            comment = await Comment.create({ authorId: user.id, ...data });
        }

        return comment;
    },
};

export default {
    saveComment,
};
