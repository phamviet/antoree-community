import { GraphQLID, GraphQLNonNull } from 'graphql';
import { UserError } from 'graphql-errors';
import EntityInterface from '../types/EntityInterface';
import CollectionEnumType from '../types/CollectionEnumType';
import EntityTypeEnumType from '../types/EntityTypeEnumType';
import * as models from '../models';

const { Collection, EntityCollection } = models;

export const toggle = {
    type: EntityInterface,
    description: 'Toggle entity collection',
    args: {
        entityId: { type: new GraphQLNonNull(GraphQLID) },
        entityType: { type: new GraphQLNonNull(EntityTypeEnumType) },
        name: { type: new GraphQLNonNull(CollectionEnumType) },
    },
    async resolve(_, { entityId, entityType, name }, { user }) {
        if (!user.isEditor()) {
            throw new UserError('Permission denied');
        }

        const collection = await Collection.findOne({ where: { name } });
        if (!collection) {
            throw new UserError(
                `Collection with name "${name}" could not be found`,
            );
        }

        const entity = await models[entityType].findById(entityId);

        if (!entity) {
            throw new UserError(
                `Entity type "${entityType}" with id "${entityId}" could not be found`,
            );
        }

        const where = { entityId, entityType, collectionId: collection.id };
        const exists = await EntityCollection.count({
            where,
        });

        if (exists) {
            await EntityCollection.destroy({ where });
        } else {
            await EntityCollection.create(where);
        }

        return entity;
    },
};

export default {
    toggle,
};
