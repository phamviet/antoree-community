import url from 'url';
import { GraphQLNonNull } from 'graphql';
import Promise from 'bluebird';
import kebabCase from 'lodash/kebabCase';
import fetch from 'node-fetch';
import qs from 'query-string';
import { UserError } from 'graphql-errors';
import { Post, User } from '../models';
import config from '../../config';
import logger from '../logger';
import CallActionType from '../types/CallActionType';
import LearningRequestInputType from '../types/LearningRequestInputType';

const baseUrl = config.api.antoreeUrl;
const defaultOptions = {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
};

const fakeRequest = () =>
    Promise.resolve({ json: () => Promise.resolve({ _success: true }) });
const request = (path, options) =>
    process.env.NODE_ENV !== 'production'
        ? fakeRequest(path, options)
        : fetch(`${baseUrl}/${path}`, {
              ...defaultOptions,
              ...options,
              headers: {
                  ...defaultOptions.headers,
                  ...(options && options.headers),
              },
          });

export const requestLearning = {
    type: CallActionType,
    description: 'Consultant request',
    args: {
        input: { type: new GraphQLNonNull(LearningRequestInputType) },
    },
    async resolve(_, { input }, { req }) {
        const { postId, ...args } = input;
        const post = await Post.findOne({
            attributes: ['id', 'slug', 'flags'],
            where: { id: postId },
            include: [
                {
                    model: User,
                    as: 'author',
                    required: true,
                    attributes: ['displayName'],
                },
            ],
        });

        if (!post) {
            throw new UserError(`Post with id "${postId}" could not be found`);
        }

        const referer = req.get('referer');
        let meta = {};

        if (referer) {
            const { hostname, query } = url.parse(referer);
            if (query) {
                meta = qs.parse(query);
                const google = /google/i;
                if (google.test(hostname) && !meta.utm_campaign) {
                    meta.utm_campaign = 'FromGoogleSearch';
                }
            }
        }

        meta = {
            utm_source: 'community',
            utm_medium: 'banner_detail',
            ...meta,
            utm_campaign: [
                `${Post.isKid(post.flags)
                    ? 'com-kid'
                    : 'com'}-${post.slug}-by-${kebabCase(
                    post.author.displayName,
                )}`,
                meta.utm_campaign,
            ].join('-'),
        };

        const data = {
            ...args,
            meta: { ...meta, ...args.meta },
        };

        return request('commercial/learning-request', {
            body: JSON.stringify(data),
        })
            .then(res => res.json())
            .then(result => {
                // eslint-disable-next-line no-underscore-dangle
                const success = result ? result._success : false;
                if (success) {
                    if (process.env.NODE_ENV === 'production') {
                        logger.info('New learning request received:', {
                            data,
                            article: post.displayName,
                            link: post.getCanonicalUrl(),
                        });
                    }
                } else {
                    logger.error('Learning request error', result);
                }

                return {
                    success,
                    data,
                };
            });
    },
};

export default {
    requestLearning,
};
