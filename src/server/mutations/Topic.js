import { UserError } from 'graphql-errors';
import { Topic, Collection } from '../models';
import TopicType from '../types/TopicType';
import TopicInputType from '../types/TopicInputType';
import { slugify } from '../utils';

export const saveTopic = {
    type: TopicType,
    description: 'Create a Topic',
    args: {
        input: { type: TopicInputType },
    },
    async resolve(_, { input }) {
        const { id, displayName, collections = [], ...data } = input;
        const trimmedName = displayName.trim();

        if (!trimmedName) {
            throw new UserError('Empty topic name');
        }

        let topic;
        const values = { photoId: null, displayName: trimmedName, ...data };

        if (id) {
            topic = await Topic.findById(id);
            if (!topic) {
                throw new UserError(`Topic with id "${id}" could not be found`);
            }

            await topic.update(values);
        } else {
            const slug = slugify(trimmedName);
            const exists = await Topic.count({
                where: { slug, parentId: null },
            });

            if (exists) {
                throw new UserError(
                    `Topic with name "${input.displayName}" is already taken`,
                );
            }

            topic = await Topic.create({ ...values, slug });
        }

        if (collections.length) {
            const cons = await Collection.findAll({
                where: { displayName: collections },
            });

            await topic.setTopicCollections(cons);
        }

        return topic;
    },
};

export default {
    saveTopic,
};
