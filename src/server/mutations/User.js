import { GraphQLID as ID, GraphQLNonNull } from 'graphql';
import { UserError } from 'graphql-errors';
import { User } from '../models';
import UserType from '../types/UserType';
import UserInputType from '../types/UserInputType';
import UserRoleEnumType from '../types/UserRoleEnumType';

export const save = {
    type: UserType,
    description: 'Update existing User',
    args: {
        input: { type: UserInputType },
    },
    async resolve(_, { input }, { user }) {
        if (!user.isAdministrator() || user.id !== input.id) {
            throw new UserError('Permission denied');
        }

        const { id, ...data } = input;
        const u = await User.findById(id);
        if (!u) {
            throw new UserError(`User with id "${id}" could not be found`);
        }

        await u.update(data);

        return u;
    },
};

export const toggleSubscription = {
    type: UserType,
    description: 'Toggle subscription',
    args: {
        id: { type: new GraphQLNonNull(ID) },
    },
    async resolve(_, { id }, { user }) {
        if (!user) {
            throw new UserError(
                'Session timed out. Please login and try again.',
            );
        }

        const star = await User.findById(id);
        if (!star) {
            throw new UserError(`No user found with id "${id}"`);
        }

        return star
            .hasSubscriber(user.id)
            .then(
                yes =>
                    yes
                        ? star.removeSubscriber(user.id)
                        : star.addSubscriber(user.id),
            )
            .then(() => star);
    },
};

export const toggleRole = {
    type: UserType,
    description: 'Toggle role',
    args: {
        id: { type: new GraphQLNonNull(ID) },
        role: { type: new GraphQLNonNull(UserRoleEnumType) },
    },
    async resolve(_, { id, role }, { user }) {
        if (!user || !user.isAdministrator()) {
            throw new UserError('Permission denied.');
        }

        const u = await User.findOne({
            where: { id },
            attributes: ['id', 'role'],
        });

        if (!u) {
            throw new UserError('User not found');
        }

        u.set(
            'role',
            // eslint-disable-next-line no-bitwise
            u.hasRole(role) ? u.role & ~role : u.role | role,
        );

        return u.save();
    },
};

export default {
    save,
};
