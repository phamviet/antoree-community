/* eslint-disable no-use-before-define */
/* @flow */

import Promise from 'bluebird';
import { GraphQLID as ID, GraphQLNonNull } from 'graphql';
import { UserError } from 'graphql-errors';
import { STATUS_PUBLISHED, FLAG_KID } from 'shared/constants/post';
import { db, Post, File, Topic, Collection, PostUser } from '../models';
import PostType from '../types/PostType';
import PostInputType from '../types/PostInputType';
import PostStatusEnum from '../types/PostStatusEnumType';
import PostFlagEnum from '../types/PostFlagEnumType';
import ReactionTypeEnumType from '../types/ReactionTypeEnumType';

export const save = {
    type: PostType,
    description: 'Create a Post',
    args: {
        input: { type: PostInputType },
    },
    async resolve(_, { input }, { user }) {
        let post;

        if (!user) {
            throw new UserError(
                'Session timed out. Please login and try again.',
            );
        }

        const { id, raw, tags = [], collections = [], ...data } = input;
        const transaction = await db.transaction();
        const options = { transaction };

        if (id) {
            post = await Post.findById(id, options);
            if (!post) {
                transaction.rollback();
                throw new UserError(`No post found with id "${id}"`);
            }

            await post.update(
                {
                    photoId: null,
                    updatedById: user.id,
                    ...data,
                },
                options,
            );
        } else {
            try {
                post = await Post.create(
                    {
                        updatedById: user.id,
                        authorId: user.id,
                        photoId: null,
                        ...data,
                    },
                    options,
                );
            } catch (error) {
                await transaction.rollback();
                throw error;
            }
        }

        const promises = [];

        // raw
        promises.push(post.meta('raw', raw, options));
        promises.push(post.clearCache(options));
        promises.push(linkContentPhotos(post, raw, options));

        // tags
        if (tags.length) {
            promises.push(
                Topic.bulkFindOrCreate(
                    tags.map(tag => ({ ...tag, type: 'tag' })),
                    {
                        transaction,
                    },
                ),
            );
        } else {
            promises.push(post.setTags([], options));
        }

        if (collections.length) {
            promises.push(Collection.findAll({ where: { name: collections } }));
        }

        const nextPromises = [];

        try {
            const result = await Promise.all(promises);
            result.forEach(arg => {
                if (!Array.isArray(arg)) {
                    return;
                }

                if (arg.some(instance => instance instanceof Topic)) {
                    // identify kid post
                    const isKid = arg.some(instance => instance.isKid());
                    post.set(
                        'flags',
                        // eslint-disable-next-line no-bitwise
                        isKid ? post.flags | FLAG_KID : post.flags & ~FLAG_KID,
                    );
                    nextPromises.push(post.setTags(arg, options));
                    nextPromises.push(post.save());
                }

                if (arg.some(instance => instance instanceof Collection)) {
                    nextPromises.push(post.setPostCollections(arg, options));
                }
            });
        } catch (error) {
            await transaction.rollback();
            throw error;
        }

        if (nextPromises.length) {
            try {
                await Promise.all(nextPromises);
            } catch (error) {
                await transaction.rollback();
                throw error;
            }
        }

        await transaction.commit();

        return post;
    },
};

export const changeStatus = {
    type: PostType,
    description: 'Change a post status',
    args: {
        id: { type: new GraphQLNonNull(ID) },
        status: { type: new GraphQLNonNull(PostStatusEnum) },
    },
    async resolve(_, { id, status }, { user }) {
        if (!user) {
            throw new UserError(
                'Session timed out. Please login and try again.',
            );
        }

        const post = await Post.findById(id);
        if (!post) {
            throw new UserError(`No post found with id "${id}"`);
        }

        if (!user.isAllowed(post)) {
            throw new UserError('Permission denied');
        }

        if (post.status !== status) {
            const data = { status };
            if (status === STATUS_PUBLISHED) {
                data.publishedAt = new Date();
            }

            try {
                await post.update(data);
            } catch (error) {
                throw error;
            }
        }

        return post;
    },
};

export const toggleReaction = {
    type: PostType,
    description: 'Toggle user specific reaction',
    args: {
        id: { type: new GraphQLNonNull(ID) },
        type: { type: new GraphQLNonNull(ReactionTypeEnumType) },
    },
    async resolve(_, { id, type }, { user }) {
        if (!user) {
            throw new UserError(
                'Session timed out. Please login and try again.',
            );
        }

        const post = await Post.findById(id);
        if (!post) {
            throw new UserError(`No post found with id "${id}"`);
        }

        const data = { postId: id, type, userId: user.id };
        const row = await PostUser.findOne({ where: data });

        if (!row) {
            await PostUser.create(data);
        } else {
            await row.destroy();
        }

        return post;
    },
};

export const toggleFlag = {
    type: PostType,
    description: 'Toggle Post flag',
    args: {
        id: { type: new GraphQLNonNull(ID) },
        flag: { type: new GraphQLNonNull(PostFlagEnum) },
    },
    async resolve(_, { id, flag }, { user }) {
        if (!user || !user.isEditor()) {
            throw new UserError('Permission denied.');
        }

        const post = await Post.findById(id);
        if (!post) {
            throw new UserError(`No post found with id "${id}"`);
        }

        post.toggleFlag(flag);

        return post.save();
    },
};

export const destroy = {
    type: PostType,
    description: 'Delete a post',
    args: {
        id: { type: ID },
    },
    async resolve(_, { id }, { user }) {
        if (!user) {
            throw new UserError(
                'Session timed out. Please login and try again.',
            );
        }

        const post = await Post.findById(id);
        if (!post) {
            throw new UserError(`No post found with id "${id}"`);
        }

        if (!user.isAllowed(post)) {
            throw new UserError('Permission denied');
        }

        return post.destroy();
    },
};

async function linkContentPhotos(post: Post, raw, options = {}) {
    let content;

    try {
        content = JSON.parse(raw);
    } catch (error) {
        console.error('Could not parse JSON raw content');
        throw error;
    }

    const { entityMap } = content;
    const images = Object.keys(entityMap)
        .filter(key => entityMap[key].type === 'image')
        .map(key => entityMap[key]);

    // unlink all old photos
    await File.update(
        { postId: null },
        { where: { postId: post.id }, ...options },
    );

    if (images.length) {
        const photoIds = images
            .filter(image => image.data.id)
            .map(image => image.data.id);

        return File.update(
            { postId: post.id },
            { where: { id: { $in: photoIds } }, ...options },
        );
    }

    return [];
}

export default {
    save,
};
