const bugsnag = require('bugsnag');
const unhandledRejection = require('unhandled-rejection');
const config = require('../../config');

if (config.bugsnag.serverKey) {
    bugsnag.register(config.bugsnag.serverKey, {
        sendCode: true,
    });
}

// When it comes to unhandled rejections (from Promises) within our app, we can use a library
// called "unhandled-rejection" to do all the heavy lifting. This library gives us the ability to
// catch rejections from within multiple types of Promise implementations.
const rejectionEmitter = unhandledRejection({
    timeout: 20,
});

rejectionEmitter.on('unhandledRejection', error => {
    bugsnag.notify(error);
});

module.exports = bugsnag;
