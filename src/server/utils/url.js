import config from '../../config';

export const urlFor = path => config.siteUrl + path;

export default { urlFor };
