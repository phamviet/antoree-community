/* eslint-disable jsx-a11y/heading-has-content */
import React from 'react';
import classNames from 'classnames';
import { convertToHTML } from 'draft-convert';
import { DETAIL } from 'shared/constants/imageSizes';
import { Inline, Block } from 'lib/draft-js/constants';
import imgix from '../imgix';

export const styleToHTML = style => {
    switch (style) {
        case Inline.ITALIC:
            return <em />;
        case Inline.BOLD:
            return <strong />;
        case Inline.STRIKETHROUGH:
            return <del />;
        case Inline.UNDERLINE:
            return <u />;
        case Inline.HIGHLIGHT:
            return <mark />;
        case Inline.CODE:
            return <code />;
        default:
            return null;
    }
};

export const blockToHTML = block => {
    const blockType = block.type;
    const blockClass = `block-${blockType.toLowerCase()}`;

    switch (blockType) {
        case Block.H1:
            return <h1 className="block-content block-header" />;
        case Block.H2:
            return <h2 className="block-content block-header" />;
        case Block.H3:
            return <h3 className="block-content block-header" />;
        case Block.H4:
            return <h4 className="block-content block-header" />;
        case Block.H5:
            return <h5 className="block-content block-header" />;
        case Block.H6:
            return <h6 className="block-content block-header" />;
        case Block.ATOMIC:
            return {
                start: `<div class="block-content ${blockClass}">`,
                end: '</div>',
            };
        case Block.BLOCKQUOTE:
            return <blockquote className="block-content blockquote" />;
        case Block.OL:
            return {
                element: <li />,
                nest: <ol className="block-content" />,
            };
        case Block.UL:
            return {
                element: <li />,
                nest: <ul className="block-content" />,
            };
        case Block.UNSTYLED: {
            if (block.text.length < 1) {
                return (
                    <p className={`block-content ${blockClass}`}>
                        <br />
                    </p>
                );
            }
            const data = block.data;
            const textAlign = data ? data['text-align'] : '';
            return (
                <p
                    className={classNames('block-content', blockClass, {
                        [`text-${textAlign}`]: textAlign,
                    })}
                />
            );
        }
        default:
            return null;
    }
};

export const entityToHTML = (entity, originalText) => {
    const type = entity.type.toUpperCase();

    if (type === 'LINK') {
        let url = entity.data.url || entity.data.href || '';
        url = url.replace('staging.', '');
        return `
            <a href="${url}" title="${originalText}" target="_blank">
                ${originalText}
            </a>
        `;
    }

    if (type === 'IMAGE' && entity.data.src) {
        const { caption } = entity.data;
        let filename;
        const parts = entity.data.src.split('imgix.net');
        if (parts.length === 2) {
            filename = parts.pop();
        }

        const src = filename
            ? imgix.buildURL(filename, { w: DETAIL.width })
            : entity.data.src;

        const figcaption = caption
            ? `<figcaption class="figure-caption mt-3 text-center">${caption}</figcaption>`
            : '';
        return `
            <figure class="figure d-block text-center">
                <img alt="${caption || ''}" src="${src}" />
                ${figcaption}
            </figure>
        `;
    }

    if (type === 'EMBED') {
        const { src, caption } = entity.data;
        const figcaption = caption
            ? `<figcaption class="figure-caption mt-3 text-center">${caption}</figcaption>`
            : '';
        return `
            <figure class="figure embed-responsive embed-responsive-16by9">
                <iframe src="${src}" allowfullscreen class="embed-responsive-item"></iframe>
                ${figcaption}
            </figure>
        `;
    }

    return originalText;
};

export const options = {
    styleToHTML,
    blockToHTML,
    entityToHTML,
};

export default (contentState, htmlOptions = options) =>
    convertToHTML(htmlOptions)(contentState);
