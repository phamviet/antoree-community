/* eslint-disable no-underscore-dangle */
/* @flow */

import PasswordGrantStrategy from 'passport-oauth2-password-grant';
import { AuthorizationError } from 'passport-oauth2';
import _debug from 'debug';
import logger from '../logger';

const debug = _debug('app:antoree-passport');

type AntoreeResponse = {
    _success: boolean,
    _messages: string,
    _data: object,
};

class AntoreeStrategy extends PasswordGrantStrategy {
    constructor(options, verify) {
        super(options, verify);

        if (!options.profileURL) {
            throw new TypeError('AntoreeStrategy requires a profileURL option');
        }

        this._profileURL = options.profileURL;
    }

    userProfile(accessToken, done) {
        this._oauth2.useAuthorizationHeaderforGET(true);
        this._oauth2.get(this._profileURL, accessToken, (err, body) => {
            if (err) {
                debug(accessToken);
                debug('Antoree remote account fetch failed');
                return done(err);
            }

            let json: AntoreeResponse;

            try {
                json = JSON.parse(body);
            } catch (error) {
                debug(
                    'Antoree unexpected remote account response (JSON.parse failed)',
                );
                debug(body);
                return done(error);
            }

            if (!json._data.user) {
                const error = new TypeError(
                    'Antoree unexpected remote account response (no `user` key)',
                );
                debug(error);
                debug(body);
                return done(error);
            }

            return done(null, json._data.user);
        });
    }

    parseErrorResponse(body, status) {
        super.parseErrorResponse(body, status);
        const json = JSON.parse(body);
        let error = null;

        if (json.error) {
            error = new AuthorizationError(
                json.error_description || json.message,
                json.error,
                json.error_uri,
            );

            logger.error(error.message);
        }

        return error;
    }
}

export default AntoreeStrategy;
