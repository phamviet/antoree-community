import { GraphQLID, GraphQLNonNull } from 'graphql';
import { UserError } from 'graphql-errors';

import UserType from '../types/UserType';
import { User } from '../models';

export default {
    type: UserType,
    args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
    },
    resolve: async (_, { id }) => {
        const user = await User.findOne({
            where: { id },
        });

        if (!user) {
            throw new UserError(`No user found with id "${id}"`);
        }

        return user;
    },
};
