import { GraphQLList, GraphQLID, GraphQLString, GraphQLInt } from 'graphql';
import moment from 'moment';
import DateType from 'graphql-date';
import { STATUS_PUBLISHED } from 'shared/constants/post';
import PostType from '../types/PostType';
import PostStatusEnumType from '../types/PostStatusEnumType';
import OrderByEnumType, { PUBLISHED_AT } from '../types/OrderByEnumType';
import SortEnumType, { DESC } from '../types/SortEnumType';
import { db, knex } from '../models';

export default {
    type: new GraphQLList(PostType),
    args: {
        query: { type: GraphQLString },
        statuses: {
            type: new GraphQLList(PostStatusEnumType),
            defaultValue: STATUS_PUBLISHED,
        },
        notIds: { type: new GraphQLList(GraphQLID) },
        authorIds: { type: new GraphQLList(GraphQLID) },
        userIds: { type: new GraphQLList(GraphQLID) },
        topicIds: { type: new GraphQLList(GraphQLID) },
        topicSlug: { type: GraphQLString },
        publishedBefore: { type: DateType },
        orderBy: { type: OrderByEnumType, defaultValue: PUBLISHED_AT },
        sort: { type: SortEnumType, defaultValue: DESC },
        limit: { type: GraphQLInt, defaultValue: 10 },
        offset: { type: GraphQLInt, defaultValue: 0 },
    },
    // eslint-disable-next-line no-unused-vars
    resolve: async (
        _,
        {
            query,
            notIds,
            authorIds,
            statuses,
            topicIds,
            topicSlug,
            publishedBefore,
            orderBy,
            sort,
            limit,
            offset,
        },
        { notIn, Posts },
    ) => {
        const qb = knex.select('posts.id').from('posts');

        if (query) {
            qb.where('posts.displayName', 'like', `%${query}%`);
        }

        if (!statuses.length) {
            // eslint-disable-next-line no-param-reassign
            statuses = [STATUS_PUBLISHED];
        }

        qb.whereIn('status', statuses);

        if (notIds) {
            qb.whereNotIn('posts.id', notIds);
        }

        if (authorIds) {
            qb.whereIn('posts.authorId', authorIds);
        }

        if (publishedBefore) {
            qb.where('posts.publishedAt', '<', moment.utc(publishedBefore));
        }

        if (topicSlug) {
            qb.innerJoin('posts_tags', 'posts.id', 'posts_tags.postId');
            qb.innerJoin('topics', 'posts_tags.tagId', 'topics.id');
            qb.where('topics.slug', topicSlug);
        } else if (topicIds) {
            qb.innerJoin('posts_tags', 'posts.id', 'posts_tags.postId');
            qb.whereIn('posts_tags.tagId', topicIds);
        }

        // Home exclusive
        if (notIn.size) {
            qb.whereNotIn('posts.id', Array.from(notIn.keys()));
        }

        qb
            .whereNotNull('posts.authorId')
            .whereNull('posts.deletedAt')
            .orderBy(`posts.${orderBy}`, sort)
            .limit(limit)
            .offset(offset);

        return db
            .query(qb.toString(), { type: db.QueryTypes.SELECT })
            .then(posts => posts.map(post => post.id))
            .then(ids => {
                ids.forEach(id => notIn.set(id, true));
                return ids.length ? Posts.loadMany(ids) : [];
            });
    },
};
