import { GraphQLString, GraphQLInt } from 'graphql';
import CommentStatusEnumType from '../types/CommentStatusEnumType';
import CommentType from '../types/CommentType';
import Comment from '../models/Comment';
import OrderByEnumType, { CREATED_AT } from '../types/OrderByEnumType';
import SortEnumType, { ASC } from '../types/SortEnumType';

export default {
    type: CommentType,
    args: {
        postId: { type: GraphQLString },
        status: { type: CommentStatusEnumType },
        orderBy: { type: OrderByEnumType, defaultValue: CREATED_AT },
        sort: { type: SortEnumType, defaultValue: ASC },
        limit: { type: GraphQLInt, defaultValue: 10 },
        offset: { type: GraphQLInt, defaultValue: 0 },
    },
    resolve: async (_, { postId, status, limit, offset, orderBy, sort }) => {
        const where = { limit, offset };
        if (postId) {
            where.postId = postId;
        }

        if (status) {
            where.status = status;
        }

        const comments = await Comment.findAll({
            where,
            order: [[orderBy, sort]],
        });

        return comments;
    },
};
