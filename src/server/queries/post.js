import { GraphQLID, GraphQLString } from 'graphql';
import { UserError } from 'graphql-errors';
import PostType from '../types/PostType';
import Post from '../models/Post';

export default {
    type: PostType,
    args: {
        id: { type: GraphQLID },
        slug: { type: GraphQLString },
    },
    resolve: async (_, { id, slug }) => {
        if (!id && !slug) {
            throw new UserError(`Either "id" or "slug" argument is required`);
        }

        const where = id ? { id } : { slug };
        const post = await Post.findOne({
            where,
        });

        return post;
    },
};
