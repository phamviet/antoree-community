import ViewerType from '../types/ViewerType';
import { User } from '../models';

const viewer = {
    type: ViewerType,
    resolve(root, args, { user }) {
        if (!user) {
            // todo decouple slider query from viewer
            return User.build({ displayName: 'Anonymous' });
        }

        return user;
    },
};

export default viewer;
