import OrderByEnumType, { PUBLISHED_AT } from '../types/OrderByEnumType';
import SortEnumType, { DESC } from '../types/SortEnumType';

export const sortingArgs = {
    orderBy: { type: OrderByEnumType, defaultValue: PUBLISHED_AT },
    sort: { type: SortEnumType, defaultValue: DESC },
};

export default { sortingArgs };
