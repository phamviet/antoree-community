import {
    GraphQLList,
    GraphQLString,
    GraphQLInt,
    GraphQLID,
    GraphQLNonNull,
} from 'graphql';
import { UserError } from 'graphql-errors';
import { Topic } from '../models';
import TopicType from '../types/TopicType';
import TopicTypeEnumType, { TAG } from '../types/TopicTypeEnumType';
import OrderByEnumType, { DISPLAY_NAME } from '../types/OrderByEnumType';
import SortEnumType, { ASC } from '../types/SortEnumType';

export const topicQuery = {
    type: TopicType,
    args: {
        slug: { type: new GraphQLNonNull(GraphQLString) },
    },
    resolve: async (_, { slug }) => {
        const topic = await Topic.findOne({
            where: { slug },
        });

        if (!topic) {
            throw new UserError(`No topic found with slug "${slug}"`);
        }

        return topic;
    },
};

export default {
    type: new GraphQLList(TopicType),
    args: {
        name: { type: GraphQLString },
        type: { type: TopicTypeEnumType, defaultValue: TAG },
        notIn: { type: new GraphQLList(GraphQLID) },
        orderBy: { type: OrderByEnumType, defaultValue: DISPLAY_NAME },
        sort: { type: SortEnumType, defaultValue: ASC },
        limit: { type: GraphQLInt, defaultValue: 10 },
        offset: { type: GraphQLInt, defaultValue: 0 },
    },
    resolve: async (
        _,
        { name, type, notIn = [], orderBy, sort, limit, offset },
    ) => {
        let where = { type };

        if (notIn.length) {
            where = { ...where, id: { $notIn: notIn } };
        }

        if (name) {
            where = { ...where, displayName: { $like: `%${name}%` } };
        }

        const topics = await Topic.findAll({
            where,
            limit,
            offset,
            order: [[orderBy, sort]],
        });

        return topics;
    },
};
