import { GraphQLNonNull, GraphQLString } from 'graphql';
import { UserError } from 'graphql-errors';
import CollectionType from '../types/CollectionType';
import { Collection } from '../models';

export default {
    type: CollectionType,
    args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
    },
    resolve: async (_, { name }) => {
        const entity = await Collection.findOne({
            where: { name },
        });

        if (!entity) {
            throw new UserError(`No collection found with name "${name}"`);
        }

        return entity;
    },
};
