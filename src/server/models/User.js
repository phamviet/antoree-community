/* eslint-disable object-shorthand */
import DataType from 'sequelize';
import { EDITOR, ADMINISTRATOR } from 'shared/constants/roles';
import { USER } from 'shared/constants/entity';
import { REACTION_BOOKMARK } from 'shared/constants';
import Model from '../sequelize';
import knex from '../knex';
import { File as FileModel } from '.';

const User = Model.define(
    'User',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        email: {
            type: DataType.STRING(128),
            validate: { isEmail: true },
        },
        displayName: {
            type: DataType.STRING(64),
            validate: { len: [2, 60] },
        },
        isTeacher: {
            type: DataType.BOOLEAN,
            defaultValue: false,
        },
        externalTeacherId: {
            type: DataType.INTEGER,
        },
        role: {
            type: DataType.INTEGER,
            defaultValue: 0,
        },
        birthday: {
            type: DataType.DATE,
        },
        gender: {
            type: DataType.ENUM(['0', '1']),
        },
        disabled: {
            type: DataType.BOOLEAN,
            defaultValue: false,
        },
        lockedTo: {
            type: DataType.DATE,
            allowNull: true,
        },
        bio: {
            type: DataType.STRING,
        },
        skype: {
            type: DataType.STRING,
        },
        address: {
            type: DataType.STRING,
        },
        nationality: {
            type: DataType.STRING(64),
        },
        country: {
            type: DataType.STRING(64),
        },
        city: {
            type: DataType.STRING(128),
        },
        state: {
            type: DataType.STRING(128),
        },
        locale: {
            type: DataType.STRING(32),
        },
        timezone: {
            type: DataType.STRING(64),
        },
        oldId: {
            type: DataType.INTEGER,
            index: true,
        },
    },
    {
        indexes: [{ fields: ['email'] }],
        getterMethods: {
            entityType() {
                return USER;
            },
        },
        tableName: 'users',
    },
);

User.associate = ({ Post, Topic, PostUser, Comment, File, UserLogin }) => {
    User.hasMany(Post, {
        foreignKey: 'authorId',
        as: 'posts',
    });

    User.hasMany(Comment, {
        foreignKey: 'authorId',
        as: 'comments',
    });

    User.hasMany(File, {
        foreignKey: 'userId',
        as: 'files',
    });

    User.belongsToMany(Topic, {
        as: 'topics',
        through: 'users_topics',
        foreignKey: 'userId',
        timestamps: true,
        updatedAt: false,
    });

    User.belongsToMany(Post, {
        through: {
            model: PostUser,
            unique: false,
        },
        foreignKey: 'userId',
    });

    User.belongsToMany(User, {
        as: 'subscribers',
        through: 'users_subscribers',
        foreignKey: 'userId',
        timestamps: true,
        updatedAt: false,
    });

    User.hasMany(UserLogin, {
        foreignKey: 'userId',
        as: 'logins',
        onDelete: 'CASCADE',
    });

    User.belongsTo(File, {
        as: 'photo',
        foreignKey: 'photoId',
        constraints: false, // http://docs.sequelizejs.com/class/lib/associations/base.js~Association.html
    });
};

User.prototype.setProfilePicture = async function setProfilePicture(remote) {
    let photo = await this.getPhoto();
    if (!photo) {
        photo = await FileModel.create({
            remote,
            attachedTo: 'user',
            userId: this.id,
        });
        await this.update({ photoId: photo.id });
    } else {
        await photo.update({ remote });
    }

    return photo;
};

User.prototype.isAllowed = async function isAllowed(obj) {
    if (obj.authorId === this.id) {
        return true;
    }

    return this.role === EDITOR;
};

User.prototype.hasRole = function hasRole(role) {
    // eslint-disable-next-line no-bitwise
    return (this.role & role) === role;
};

User.prototype.isAdministrator = function isAdministrator() {
    return this.hasRole(ADMINISTRATOR);
};

User.prototype.isEditor = function isEditor() {
    return this.hasRole(EDITOR);
};

User.prototype.getBookmarkedPostIds = async function getBookmarkedPostIds({
    limit = 10,
    offset,
}) {
    const userId = this.id;
    const qb = knex
        .select('postId')
        .from('posts_users')
        .where({ userId, type: REACTION_BOOKMARK })
        .orderBy('createdAt', 'desc')
        .limit(limit);

    if (offset) {
        qb.offset(offset);
    }

    return Model.query(qb.toString(), {
        type: Model.QueryTypes.SELECT,
    }).then(posts => posts.map(post => post.postId));
};

export default User;
