import DataType from 'sequelize';
import {
    REACTION_LIKE,
    REACTION_BOOKMARK,
    REACTION_MENTION,
} from 'shared/constants';
import Model from '../sequelize';

const PostUser = Model.define(
    'PostUser',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        userId: {
            type: DataType.UUID,
            unique: 'post_user',
        },
        postId: {
            type: DataType.UUID,
            unique: 'post_user',
        },
        type: {
            type: DataType.ENUM(
                REACTION_LIKE,
                REACTION_BOOKMARK,
                REACTION_MENTION,
            ),
            unique: 'post_user',
        },
    },
    {
        tableName: 'posts_users',
        timestamps: true,
        updatedAt: false,
    },
);

export default PostUser;
