import DataType from 'sequelize';
import Model from '../sequelize';

const EntityCollection = Model.define(
    'EntityCollection',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        entityId: {
            type: DataType.UUID,
            allowNull: false,
        },
        entityType: {
            type: DataType.ENUM('Post', 'Topic', 'User'),
            allowNull: false,
        },
        collectionId: {
            type: DataType.UUID,
            allowNull: false,
        },
        sortOrder: {
            type: DataType.INTEGER,
            defaultValue: 0,
        },
    },
    {
        tableName: 'entities_collections',
        indexes: [
            {
                fields: ['entityId', 'entityType', 'collectionId'],
                unique: true,
            },
        ],
        timestamps: true,
    },
);

/**
 * Association
 */
EntityCollection.associate = ({ Post }) => {
    EntityCollection.belongsTo(Post, {
        foreignKey: 'entityId',
        as: 'post',
        onDelete: 'CASCADE',
    });
};

export default EntityCollection;
