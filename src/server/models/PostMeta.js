import DataType from 'sequelize';
import Model from '../sequelize';

const PostMeta = Model.define(
    'PostMeta',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        name: {
            type: DataType.STRING(64),
            allowNull: false,
        },
        value: {
            type: DataType.TEXT('long'),
        },
    },
    {
        tableName: 'posts_meta',
        indexes: [
            {
                fields: ['postId', 'name'],
            },
        ],
    },
);

/**
 * Associations
 */
PostMeta.associate = ({ Post }) => {
    PostMeta.belongsTo(Post, {
        foreignKey: 'postId',
    });
};

export default PostMeta;
