import DataType from 'sequelize';
import Promise from 'bluebird';
import { TOPIC } from 'shared/constants/entity';
import { FLAG_KID } from 'shared/constants/topic';
import Model from '../sequelize';
import { flagsContainFlag } from './utils';
import { slugify } from '../utils';

const Topic = Model.define(
    'Topic',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        displayName: {
            type: DataType.STRING(128),
        },
        slug: {
            type: DataType.STRING(191),
        },
        type: {
            type: DataType.ENUM('tag'),
            defaultValue: null, // topic
        },
        flags: {
            type: DataType.INTEGER,
            defaultValue: 0,
        },
        description: {
            type: DataType.TEXT('tiny'),
        },
        oldId: {
            type: DataType.INTEGER,
            index: true,
        },
    },
    {
        indexes: [
            { fields: ['slug', 'parentId'], unique: true },
            { fields: ['type'] },
        ],
        getterMethods: {
            entityType() {
                return TOPIC;
            },
        },
        tableName: 'topics',
    },
);

/**
 * @param options
 * @return func (topic)
 */
function doFindOrCreate(options) {
    return ({ displayName, ...other }) => {
        const attr = { slug: slugify(displayName) };
        const where = other.id ? { id: other.id } : attr;

        return Topic.findOrCreate({
            where,
            defaults: { displayName, ...attr, ...other },
            ...options,
        });
    };
}

Topic.bulkFindOrCreate = async (topics, options = {}) => {
    const filteredTopics = topics
        .map(topic => ({
            ...topic,
            displayName: topic.displayName.trim(),
        }))
        .filter(topic => topic.displayName !== '');

    const results = await Promise.map(filteredTopics, doFindOrCreate(options));
    return results.map(args => args[0]);
};

/**
 * Associations
 */
Topic.associate = ({ User, Post, ...models }) => {
    Topic.hasOne(Topic, { as: 'parent' });

    Topic.belongsToMany(Topic, {
        as: 'children',
        through: 'topics_relationships',
        foreignKey: 'topicId',
        otherKey: 'childId',
        timestamps: true,
        updatedAt: false,
    });

    Topic.belongsTo(models.File, {
        as: 'photo',
        foreignKey: 'photoId',
    });

    Topic.belongsToMany(User, {
        through: 'users_topics',
        foreignKey: 'topicId',
    });

    Topic.belongsToMany(Post, {
        as: 'posts',
        through: 'posts_tags',
        foreignKey: 'tagId',
        otherKey: 'postId',
        onDelete: 'RESTRICT', // !important
        timestamps: true,
        updatedAt: false,
    });

    Post.belongsToMany(models.Collection, {
        as: 'topicCollections',
        through: {
            model: models.EntityCollection,
            unique: false,
            scope: {
                entityType: 'topic',
            },
        },
        foreignKey: 'entityId',
        otherKey: 'collectionId',
        constraints: false,
    });
};

/**
 * Create topic slug
 */
Topic.hook('beforeCreate', topic => {
    if (!topic.slug) {
        topic.set('slug', slugify(topic.displayName));
    }
});

Topic.prototype.isKid = function isKid() {
    return flagsContainFlag(this.flags, FLAG_KID);
};

export default Topic;
