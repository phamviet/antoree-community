import DataType from 'sequelize';
import { PENDING, APPROVED } from 'shared/constants';
import Model from '../sequelize';

const Comment = Model.define(
    'Comment',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        content: {
            type: DataType.TEXT,
        },
        ip: {
            type: DataType.STRING(64),
        },
        status: {
            type: DataType.ENUM(PENDING, APPROVED),
            defaultValue: APPROVED,
        },
        approvedAt: {
            type: DataType.DATE,
        },
    },
    {
        tableName: 'comments',
    },
);

/**
 * Associations
 */
Comment.associate = ({ User, Post }) => {
    Comment.belongsTo(User, {
        as: 'author',
        foreignKey: 'authorId',
    });

    Comment.belongsTo(Post, {
        as: 'post',
        foreignKey: 'postId',
    });

    Comment.belongsTo(Comment, {
        as: 'parent',
        foreignKey: 'parentId',
    });

    Comment.hasMany(Comment, {
        foreignKey: 'parentId',
        as: 'children',
    });
};

/**
 */
Comment.hook('beforeSave', comment => {
    if (Comment.status === APPROVED && !Comment.approvedAt) {
        comment.set('approvedAt', new Date());
    }
});

export default Comment;
