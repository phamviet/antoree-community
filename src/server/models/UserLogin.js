import DataType from 'sequelize';
import Model from '../sequelize';

const UserLogin = Model.define(
    'UserLogin',
    {
        provider: {
            type: DataType.STRING(50),
            primaryKey: true,
        },
        key: {
            type: DataType.STRING(100),
            primaryKey: true,
        },
        tokens: {
            type: DataType.TEXT,
        },
    },
    {
        tableName: 'users_logins',
    },
);

export default UserLogin;
