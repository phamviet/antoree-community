import DataType from 'sequelize';
import Model from '../sequelize';

const PostTag = Model.define(
    'PostTag',
    {
        postId: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
        tagId: {
            type: DataType.UUID,
            allowNull: false,
            primaryKey: true,
        },
    },
    {
        tableName: 'posts_tags',
        timestamps: true,
        updatedAt: false,
    },
);

/**
 * Association
 */
PostTag.associate = ({ Post }) => {
    PostTag.belongsTo(Post, {
        foreignKey: 'postId',
        as: 'post',
        onDelete: 'CASCADE',
    });
};

export default PostTag;
