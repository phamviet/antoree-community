/* @flow */

import DataType from 'sequelize';
import Model from '../sequelize';
import imgix from '../imgix';

const File = Model.define(
    'File',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        filename: {
            type: DataType.STRING(128),
        },
        mimeType: {
            type: DataType.STRING(64),
        },
        type: {
            type: DataType.ENUM('photo', 'video', 'file'),
        },
        width: {
            type: DataType.INTEGER,
        },
        height: {
            type: DataType.INTEGER,
        },
        size: {
            type: DataType.BIGINT,
        },
        attachedTo: {
            type: DataType.ENUM('user', 'post', 'topic', 'collection'),
        },
        remote: {
            type: DataType.STRING,
            validate: {
                isUrl: true,
            },
        },
    },
    {
        tableName: 'files',
        paranoid: true,
        getterMethods: {
            url() {
                return this.getCanonicalUrl();
            },
        },
    },
);

File.associate = ({ User, Post }) => {
    File.belongsTo(User, {
        foreignKey: 'userId',
        as: 'user',
        onDelete: 'SET NULL',
    });

    File.belongsTo(Post, {
        foreignKey: 'postId',
        as: 'post',
    });
};

type FileInput = {
    fieldname: string,
    originalname: string,
    filename: string,
    destination: string,
    mimetype: string,
    size: number,
    path: string,
    encoding: string,
    location: string, // s3
    key: string, // s3
    contentType: string, // s3
    buffer: string,
};

/**
 * @param fileInput FileInput
 * @return File
 */
File.fromFileInput = (fileInput: FileInput): File => {
    const { filename, mimetype, size } = fileInput;
    const data = { filename, mimeType: mimetype, size };
    if (fileInput.key) {
        data.filename = fileInput.key;
        data.id =
            fileInput.key.indexOf('/') !== -1 // remove prefix
                ? fileInput.key.split('/').pop()
                : fileInput.key;
    }

    if (fileInput.contentType) {
        data.mimeType = fileInput.contentType;
    }

    return File.build(data);
};

/**
 * @param user
 * @param fileInput FileInput
 * @param params object
 * @return File
 */
File.receive = async (user, fileInput: FileInput, params = {}): File => {
    const file = File.fromFileInput(fileInput);

    file.set('userId', user.id);
    Object.keys(params).forEach(name => file.set(name, params[name]));
    await file.save();

    return file;
};

File.prototype.getCanonicalUrl = function getCanonicalUrl(params = {}) {
    if (this.remote) {
        return this.remote;
    }

    if (this.filename) {
        return imgix.buildURL(this.filename, params);
    }

    return `/file/${this.id}`;
};

export default File;
