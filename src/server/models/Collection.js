import Promise from 'bluebird';
import DataType from 'sequelize';
import {
    HOME_MASTHEAD_POSTS,
    HOME_FEATURED_POSTS,
} from 'shared/constants/post';
import { HOME_TOPICS_MENU, HOME_FEATURED_TOPICS } from 'shared/constants/topic';
import { COLLECTION } from 'shared/constants/entity';
import Model from '../sequelize';

const seeds = [
    {
        name: HOME_TOPICS_MENU,
        limit: 5,
    },
    {
        name: HOME_FEATURED_TOPICS,
        limit: 6,
    },
    {
        name: HOME_MASTHEAD_POSTS,
        limit: 5,
    },
    {
        name: HOME_FEATURED_POSTS,
        limit: 9,
    },
];

const Collection = Model.define(
    'Collection',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        name: {
            type: DataType.STRING(64),
            unique: true,
        },
        description: {
            type: DataType.STRING,
        },
        query: {
            type: DataType.TEXT,
        },
        limit: {
            type: DataType.INTEGER,
            defaultValue: 9,
        },
        offset: {
            type: DataType.INTEGER,
            defaultValue: 0,
        },
    },
    {
        tableName: 'collections',
        getterMethods: {
            entityType() {
                return COLLECTION;
            },
            displayName() {
                return this.name;
            },
        },
    },
);

Model.addHook('afterBulkSync', () =>
    Promise.map(seeds, ({ name, ...other }) =>
        Collection.findOrCreate({
            where: { name },
            defaults: { name, ...other },
        }),
    ),
);

/**
 * Associations
 */
Collection.associate = model => {
    Collection.belongsTo(model.File, {
        as: 'photo',
        foreignKey: 'photoId',
    });

    // for direct accessing to PostTag table
    Collection.hasMany(model.EntityCollection, {
        as: 'entityCollection',
        foreignKey: 'collectionId',
    });
};

export default Collection;
