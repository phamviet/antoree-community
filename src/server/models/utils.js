/* eslint-disable import/prefer-default-export */

export function flagsContainFlag(flags, flag) {
    // eslint-disable-next-line no-bitwise
    return (parseInt(flags, 10) & flag) === flag;
}
