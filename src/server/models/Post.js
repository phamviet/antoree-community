/* eslint-disable no-bitwise */
import DataType from 'sequelize';
import moment from 'moment';
import _debug from 'debug';
import {
    STATUS_PUBLISHED,
    STATUS_PENDING,
    STATUS_DRAFT,
    TYPE_POST,
    TYPE_ENUM,
    FLAG_KID,
    FLAG_FEATURED,
} from 'shared/constants/post';
import { POST } from 'shared/constants/entity';
import { flagsContainFlag } from './utils';
import { slugify } from '../utils';
import db from '../sequelize';
import knex from '../knex';
import events from '../events';
import PostMeta from './PostMeta';
import config from '../../config';

const debug = _debug('model:post');
const tableName = 'posts';

const Post = db.define(
    'Post',
    {
        id: {
            type: DataType.UUID,
            defaultValue: DataType.UUIDV4,
            primaryKey: true,
        },
        displayName: {
            type: DataType.TEXT,
        },
        slug: {
            type: DataType.STRING(191),
        },
        type: {
            type: DataType.ENUM(TYPE_ENUM),
            defaultValue: TYPE_POST,
        },
        status: {
            type: DataType.ENUM(STATUS_DRAFT, STATUS_PENDING, STATUS_PUBLISHED),
            defaultValue: STATUS_DRAFT,
        },
        flags: {
            type: DataType.INTEGER,
            defaultValue: 0,
        },
        locale: {
            type: DataType.ENUM('en', 'vi'),
        },
        anonymous: {
            type: DataType.BOOLEAN,
            defaultValue: true,
        },
        description: {
            type: DataType.TEXT,
        },
        publishedAt: {
            type: DataType.DATE,
        },
        viewCount: {
            type: DataType.INTEGER,
        },
        oldId: {
            type: DataType.INTEGER,
            index: true,
        },
    },
    {
        indexes: [
            {
                fields: ['slug'],
                unique: true,
            },
            {
                fields: ['type'],
            },
            {
                fields: ['anonymous'],
            },
            {
                fields: ['status'],
            },
            {
                fields: ['deletedAt'],
            },
        ],
        getterMethods: {
            entityType() {
                return POST;
            },
        },
        paranoid: true,
        // http://docs.sequelizejs.com/manual/tutorial/models-definition.html#optimistic-locking
        version: true,
        tableName,
    },
);

/**
 * Create post slug
 */
Post.hook('beforeCreate', post => {
    debug('Calling beforeCreate');
    if (!post.slug) {
        post.set(
            'slug',
            `${slugify(post.displayName)}-${post.id.split('-').shift()}`,
        );
    }
});

/**
 */
Post.hook('beforeSave', post => {
    if (post.status === STATUS_PUBLISHED && !post.publishedAt) {
        post.set('publishedAt', new Date());
    }
});

/**
 * Trigger events on published/unpublished
 */
Post.hook('afterSave', post => {
    if (post.changed('status')) {
        if (post.status === STATUS_PUBLISHED) {
            events.emit('post:published', post);
        } else if (post.previous('status') === STATUS_PUBLISHED) {
            events.emit('post:unpublished', post);
        }
    }
});

/**
 * Unset slug on destroying
 */
Post.hook('beforeDestroy', (post, options) => {
    if (post.isSoftDeleted() && !options.force) {
        post.set('slug', null);
    }
});

/**
 * Association
 */
Post.associate = ({ User, PostUser, PostTag, Comment, File, ...models }) => {
    Post.belongsTo(User, {
        foreignKey: 'authorId',
        as: 'author',
        onDelete: 'SET NULL',
    });

    Post.belongsTo(User, {
        foreignKey: 'publishedById',
        as: 'publishedBy',
        onDelete: 'SET NULL',
    });

    Post.belongsTo(User, {
        foreignKey: 'updatedById',
        as: 'updatedBy',
        onDelete: 'SET NULL',
    });

    Post.belongsToMany(models.Topic, {
        as: 'tags',
        through: {
            model: PostTag,
        },
        foreignKey: 'postId',
        otherKey: 'tagId',
    });

    // for direct accessing to PostTag table
    Post.hasMany(PostTag, {
        as: 'postTag',
        foreignKey: 'postId',
        onDelete: 'CASCADE',
    });

    Post.belongsToMany(User, {
        as: 'likers',
        through: {
            model: PostUser,
            unique: false,
            scope: {
                type: 'like',
            },
        },
        foreignKey: 'postId',
    });

    Post.hasMany(PostUser, {
        as: 'post_liker',
        scope: {
            type: 'like',
        },
        foreignKey: 'postId',
    });

    Post.hasMany(PostUser, {
        as: 'post_bookmark',
        scope: {
            type: 'bookmark',
        },
        foreignKey: 'postId',
    });

    Post.belongsToMany(User, {
        through: {
            model: PostUser,
            unique: false,
            scope: {
                type: 'mention',
            },
        },
        foreignKey: 'postId',
    });

    Post.hasMany(Comment, {
        foreignKey: 'postId',
        as: 'comments',
    });

    Post.hasMany(models.PostMeta, {
        foreignKey: 'postId',
        as: 'meta',
        onDelete: 'CASCADE',
    });

    Post.hasMany(File, {
        foreignKey: 'postId',
        scope: {
            type: 'photo',
        },
        as: 'photos',
    });

    Post.belongsTo(File, {
        as: 'photo',
        foreignKey: 'photoId',
        constraints: false, // http://docs.sequelizejs.com/class/lib/associations/base.js~Association.html
    });

    Post.belongsToMany(models.Collection, {
        as: 'postCollections',
        through: {
            model: models.EntityCollection,
            unique: false,
            scope: {
                entityType: POST,
            },
        },
        foreignKey: 'entityId',
        otherKey: 'collectionId',
        constraints: false,
    });
};

/* Static methods */
Post.getHref = function getHref(slug) {
    return `/article/${slug}/`;
};

Post.isKid = function isKidPost(flags) {
    return flagsContainFlag(flags, FLAG_KID);
};

Post.isFeatured = function isFeatured(flags) {
    return flagsContainFlag(flags, FLAG_FEATURED);
};

/**
 * @return Builder
 */
Post.buildQuery = function buildQuery({
    notIds,
    authorIds,
    statuses,
    tagIds,
    publishedBefore,
    limit,
    offset = 0,
    ...args
}) {
    const qb = knex.from(tableName);
    const query = args.query ? args.query.trim() : null;

    if (query) {
        qb.where('posts.displayName', 'like', `%${query}%`);
    }

    if (statuses) {
        qb.whereIn('status', statuses);
    }

    if (args.flag) {
        qb.whereRaw('(flags & ?) > 0', parseInt(args.flag, 10));
    }

    if (notIds) {
        qb.whereNotIn('posts.id', notIds);
    }

    if (authorIds) {
        qb.whereIn('posts.authorId', authorIds);
    }

    if (tagIds) {
        qb.innerJoin('posts_tags', 'posts.id', 'posts_tags.postId');
        qb.whereIn('posts_tags.tagId', tagIds);
    }

    if (publishedBefore) {
        qb.where('posts.publishedAt', '<', moment.utc(publishedBefore));
    }

    if (limit) {
        qb.limit(limit).offset(offset);
    }

    qb.whereNotNull('posts.authorId').whereNull('posts.deletedAt');

    return qb;
};

Post.prototype.meta = async function meta(name, value, options = {}) {
    if (value === undefined) {
        const val = await this.getMeta({ where: { name } });
        return val.length ? val[0].value : null;
    }

    const data = { name, postId: this.id };
    const entity = await PostMeta.findOne({
        where: data,
        ...options,
    });

    if (!entity) {
        return PostMeta.create({ ...data, value }, options);
    }

    return entity.update({ value }, options);
};

/**
 * Clear post cache: html, oldHtml
 * @param options
 * @return {Promise}
 */
Post.prototype.clearCache = async function clearCache(options = {}) {
    return PostMeta.destroy({
        where: { postId: this.id, name: { $in: ['html', 'oldHtml'] } },
        ...options,
    });
};

Post.prototype.getRelatedPostIds = async function relatedPosts({ limit = 3 }) {
    const postId = this.id;
    const qb = knex
        .select('id')
        .from('posts')
        .innerJoin('posts_tags', 'posts.id', 'posts_tags.postId')
        .whereIn('posts_tags.tagId', function whereIn() {
            this.select('tagId').from('posts_tags').where('postId', postId);
        })
        .whereNot('posts.id', postId)
        .whereNotNull('posts.authorId')
        .whereNull('posts.deletedAt')
        .groupBy('posts.id')
        .orderBy('posts.publishedAt', 'desc')
        .limit(limit);

    return db
        .query(qb.toString(), {
            type: db.QueryTypes.SELECT,
        })
        .then(posts => posts.map(post => post.id));
};

Post.prototype.getHref = function getHref() {
    return Post.getHref(this.slug);
};

Post.prototype.getCanonicalUrl = function getCanonicalUrl() {
    return config.siteUrl + Post.getHref(this.slug);
};

Post.prototype.hasFlag = function hasFlag(flag) {
    return flagsContainFlag(this.flags, flag);
};

Post.prototype.toggleFlag = function toggleFlag(flag) {
    let flags;
    if (this.hasFlag(flag)) {
        flags = this.flags & ~flag;
    } else {
        flags = this.flags | flag;
    }

    return this.set('flags', flags);
};

export default Post;
