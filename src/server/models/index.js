/* eslint-disable no-undef,prettier/prettier */
import db from '../sequelize';
import knex from '../knex';
import User from './User';
import Post from './Post';
import Topic from './Topic';
import Comment from './Comment';
import File from './File';
import PostUser from './PostUser';
import PostTag from './PostTag';
import PostMeta from './PostMeta';
import UserLogin from './UserLogin';
import Collection from './Collection';
import EntityCollection from './EntityCollection';

function sync(...args) {
    return db.sync(...args);
}

const models = {
    User,
    UserLogin,
    Topic,
    Post,
    Comment,
    File,
    PostUser,
    PostTag,
    PostMeta,
    Collection,
    EntityCollection,
};

Object.keys(models).forEach(name => {
    if ('associate' in models[name]) {
        models[name].associate(models);
    }
});

export default {
    sync,
};

export {
    db,
    knex,
    User,
    Post,
    File,
    Topic,
    UserLogin,
    PostUser,
    PostTag,
    Comment,
    Collection,
    EntityCollection,
}
