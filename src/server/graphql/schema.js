import { GraphQLSchema as Schema, GraphQLObjectType } from 'graphql';

import viewer from '../queries/viewer';
import topics, { topicQuery as topic } from '../queries/topics';
import posts from '../queries/posts';
import post from '../queries/post';
import user from '../queries/user';
import collection from '../queries/collection';
import comments from '../queries/comments';
import intl from '../queries/intl';

import {
    save as savePost,
    changeStatus as changePostStatus,
    toggleReaction as togglePostReaction,
    destroy as deletePost,
    toggleFlag as togglePostFlag,
} from '../mutations/Post';
import {
    save as saveUser,
    toggleSubscription as toggleUserSubscription,
    toggleRole as toggleUserRole,
} from '../mutations/User';
import { saveTopic } from '../mutations/Topic';
import { saveComment } from '../mutations/Comment';
import { toggle as toggleCollection } from '../mutations/Collection';
import { requestLearning } from '../mutations/Antoree';

const schema = new Schema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            viewer,
            intl,
            topics,
            topic,
            posts,
            post,
            user,
            comments,
            collection,
        },
    }),
    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            savePost,
            changePostStatus,
            togglePostReaction,
            togglePostFlag,
            deletePost,
            saveTopic,
            saveUser,
            toggleUserSubscription,
            toggleUserRole,
            saveComment,
            toggleCollection,
            requestLearning,
        },
    }),
});

export default schema;
