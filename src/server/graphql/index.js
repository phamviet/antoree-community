import { printSchema } from 'graphql';
import { maskErrors, defaultHandler } from 'graphql-errors';
import expressGraphQL from 'express-graphql';
import schema from './schema';
import DataLoaders from '../DataLoaders';
import bugsnag from '../utils/bugsnag';

const errorHandler = error => {
    bugsnag.notify(error);
    return defaultHandler(error);
};

maskErrors(schema, errorHandler);

export const graphqlOptions = request => {
    const startTime = Date.now();
    return {
        schema,
        rootValue: {},
        context: {
            user: request.user,
            req: request,
            notIn: new Map(),
            ...DataLoaders.create({ request }),
        },
        pretty: __DEV__,
        extensions({ operationName }) {
            return {
                runTime: Date.now() - startTime,
                operationName,
            };
        },
        formatError: error => ({
            message: error.message,
            state: error.originalError && error.originalError.state,
            locations: error.locations,
            path: error.path,
        }),
    };
};

export default app => {
    app.get('/graphql/schema', (req, res) => {
        res.type('text/plain').send(printSchema(schema));
    });

    app.use(
        '/graphql',
        expressGraphQL(req => ({
            ...graphqlOptions(req),
            graphiql: __DEV__,
        })),
    );
};
