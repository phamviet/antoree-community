import assign from 'lodash/assign';
import passport from '../passport';

// External login providers. Also see passport.js.
const providers = [
    {
        provider: 'facebook',
        options: { scope: ['email', 'user_location'] },
    },
    {
        provider: 'google',
        options: { scope: 'profile email' },
    },
    {
        provider: 'linkedin',
        options: {},
    },
];

const returnTo = (req, res, next) => {
    if (!req.session.returnTo) {
        req.session.returnTo = req.query.returnTo || req.body.returnTo || '/';
    }
    next();
};

const authOptions = {
    successReturnToOrRedirect: true,
    failureFlash: true,
    failureRedirect: '/login',
};

export default app => {
    providers.forEach(({ provider, options }) => {
        app.get(
            `/login/${provider}`,
            returnTo,
            passport.authenticate(provider, { failureFlash: true, ...options }),
        );

        app.get(`/login/${provider}/return`, (req, res, next) =>
            passport.authenticate(provider, authOptions)(req, res, next),
        );
    });

    app.post('/login', returnTo, (req, res, next) => {
        const { username, password } = req.body;
        if (username && password) {
            passport.authenticate(
                'password-grant',
                assign(authOptions, {
                    username,
                    password,
                }),
            )(req, res, next);
        } else {
            req.flash('error', 'Please enter your credentials');
            res.redirect('/login');
        }
    });

    app.get('/logout', (req, res) => {
        req.logout();
        res.redirect(req.query.returnTo || '/');
    });
};
