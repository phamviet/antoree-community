// # CacheControl Middleware
// Usage: cacheControl(profile), where profile is one of 'public' or 'private'
// After: checkIsPrivate
// Before: routes
//
// Allows each app to declare its own default caching rules

import isString from 'lodash/isString';
import config from '../../config';

const profiles = {
    public: `public, max-age=${config.caching.frontend.maxAge}`,
    private:
        'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0',
};

export default options => {
    let output;
    // eslint-disable-next-line no-prototype-builtins
    if (isString(options) && profiles.hasOwnProperty(options)) {
        output = profiles[options];
    }

    return (req, res, next) => {
        if (output) {
            if (res.isPrivatePage) {
                res.set({ 'Cache-Control': profiles.private });
            } else {
                res.set({ 'Cache-Control': output });
            }
        }
        next();
    };
};
