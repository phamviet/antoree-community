/* @flow */
/*
 * Data loaders to be used with GraphQL resolve() functions. For example:
 *
 *   resolve(post, args, { users }) {
 *     return users.load(post.authorId);
 *   }
 *
 * For more information visit https://github.com/facebook/dataloader
 */

import DataLoader from 'dataloader';
import Promise from 'bluebird';
import { REACTION_LIKE, REACTION_BOOKMARK } from 'shared/constants';
import {
    User,
    Post,
    PostUser,
    PostTag,
    Comment,
    File,
    Topic,
    db,
    knex,
} from './models';

// Appends type information to an object, e.g. { id: 1 } => { __type: 'User', id: 1 };
function assignType(obj: any, type: string) {
    // eslint-disable-next-line no-param-reassign, no-underscore-dangle
    obj.__type = type; // have to return original obj
    return obj;
}

function mapTo(keys, keyFn, type, rows) {
    if (!rows) return mapTo.bind(null, keys, keyFn, type);
    const group = new Map(keys.map(key => [key, null]));
    rows.forEach(row => group.set(keyFn(row), assignType(row, type)));
    return Array.from(group.values());
}

function mapToMany(keys, keyFn, type, rows) {
    if (!rows) return mapToMany.bind(null, keys, keyFn, type);
    const group = new Map(keys.map(key => [key, []]));
    rows.forEach(row => group.get(keyFn(row)).push(assignType(row, type)));
    return Array.from(group.values());
}

function mapToValues(keys, keyFn, valueFn, rows) {
    if (!rows) return mapToValues.bind(null, keys, keyFn, valueFn);
    const group = new Map(keys.map(key => [key, null]));
    rows.forEach(row => group.set(keyFn(row), valueFn(row)));
    return Array.from(group.values());
}

export default {
    create: ({ request }) => ({
        Users: new DataLoader(keys =>
            User.findAll({ where: { id: { $in: keys } } }).then(
                mapTo(keys, x => x.id, 'User'),
            ),
        ),
        Posts: new DataLoader(keys =>
            Post.findAll({ where: { id: { $in: keys } } }).then(
                mapTo(keys, x => x.id, 'Post'),
            ),
        ),
        Topics: new DataLoader(keys =>
            Topic.findAll({ where: { id: { $in: keys } } }).then(
                mapTo(keys, x => x.id, 'Topic'),
            ),
        ),
        /**
         * todo: make it work
         */
        TopicsByPostId: new DataLoader(keys =>
            PostTag.findAll({
                where: { postId: { $in: keys } },
            }).then(postTags =>
                Promise.map(postTags, postTag =>
                    this.Topics.load(postTag.tagId),
                ),
            ),
        ),
        Files: new DataLoader(keys =>
            File.findAll({ where: { id: { $in: keys } } }).then(
                mapTo(keys, x => x.id, 'File'),
            ),
        ),
        CommentsByPostId: new DataLoader(keys =>
            // TO DO PAGINATION
            Comment.findAll({
                where: { postId: { $in: keys } },
                order: [['createdAt', 'DESC']],
            }).then(mapToMany(keys, x => x.postId, 'Comment')),
        ),
        PostCommentsCount: new DataLoader(keys =>
            Post.findAll({
                attributes: [
                    'id',
                    [db.fn('COUNT', db.col('comments.postId')), 'count'],
                ],
                where: { id: { $in: keys } },
                group: 'id',
                include: [
                    {
                        model: Comment,
                        attributes: [],
                        as: 'comments',
                    },
                ],
            }).then(mapToValues(keys, x => x.id, x => x.get('count'))),
        ),
        PostLikesCount: new DataLoader(keys =>
            Post.findAll({
                attributes: [
                    'id',
                    [db.fn('COUNT', db.col('post_liker.userId')), 'count'],
                ],
                where: { id: { $in: keys } },
                group: 'id',
                include: [
                    {
                        model: PostUser,
                        attributes: [],
                        as: 'post_liker',
                    },
                ],
            }).then(mapToValues(keys, x => x.id, x => x.get('count'))),
        ),
        PostIsLiked: new DataLoader(keys => {
            if (!request.user) {
                return Promise.resolve(new Array(keys.length).fill(false));
            }

            const qb = knex
                .table('posts_users')
                .select(
                    'posts_users.postId as id',
                    knex.raw('count(posts_users.userId) as count'),
                )
                .groupBy('posts_users.postId')
                .where('posts_users.type', REACTION_LIKE)
                .where('posts_users.userId', request.user.id)
                .whereIn('posts_users.postId', keys);

            return db
                .query(qb.toString(), { type: db.QueryTypes.SELECT })
                .then(mapToValues(keys, x => x.id, x => x.count > 0))
                .then(values => values.map(v => !!v)); // null => boolean
        }),
        PostIsBookmarked: new DataLoader(keys => {
            if (!request.user) {
                return Promise.resolve(new Array(keys.length).fill(false));
            }

            const qb = knex
                .table('posts_users')
                .select(
                    'posts_users.postId as id',
                    knex.raw('count(posts_users.userId) as count'),
                )
                .groupBy('posts_users.postId')
                .where('posts_users.type', REACTION_BOOKMARK)
                .where('posts_users.userId', request.user.id)
                .whereIn('posts_users.postId', keys);

            return db
                .query(qb.toString(), { type: db.QueryTypes.SELECT })
                .then(mapToValues(keys, x => x.id, x => x.count > 0))
                .then(values => values.map(v => !!v)); // null => boolean
        }),
        UserIsSubscribed: new DataLoader(keys => {
            if (!request.user) {
                return Promise.resolve(new Array(keys.length).fill(false));
            }

            const qb = knex
                .table('users_subscribers')
                .select(
                    'users_subscribers.userId as id',
                    knex.raw('count(users_subscribers.subscriberId) as count'),
                )
                .whereIn('users_subscribers.userId', keys)
                .groupBy('users_subscribers.userId')
                .where('users_subscribers.subscriberId', request.user.id);

            return db
                .query(qb.toString(), { type: db.QueryTypes.SELECT })
                .then(mapToValues(keys, x => x.id, x => x.count > 0))
                .then(values => values.map(v => !!v)); // null => boolean
        }),
    }),
};
