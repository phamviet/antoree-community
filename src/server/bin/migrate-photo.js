/* eslint-disable no-console,no-unused-vars */
/*
* Use with caution!!!
* */
import Promise from 'bluebird';
import aws from 'aws-sdk';
import stream from 'stream';
import download from 'download';
import { File, db } from '../models';
import config from '../../config';

aws.config.update({ region: 'ap-southeast-1' });
const s3 = new aws.S3();

// disable logging
// db.options.logging = () => {};
const limit = null;

function uploadFromStream(file) {
    const pass = new stream.PassThrough();

    const params = {
        Bucket: config.storage.s3Bucket,
        Key: `${config.appEnv}/${file.id}`,
        Body: pass,
    };

    s3.upload(params, (err, data) => {
        if (!err) {
            return file.update({ remote: null, filename: data.key });
        }

        return err;
    });

    return pass;
}

async function doIt(file) {
    console.log(`Downloading ${file.remote}`);
    const streamMe = download(file.remote);
    streamMe.pipe(uploadFromStream(file));
    streamMe.catch(error => {
        // console.error(error);
        if (error.statusCode === 404) {
            console.log('404 Not found');
            return file.destroy();
        }

        return error;
    });

    return streamMe;
}

async function migrate() {
    return File.findAll({
        where: { filename: null, remote: { $not: null } },
        limit,
    }).then(files => Promise.map(files, file => doIt(file)));
}

migrate()
    // .then(() => db.close())
    .then(() => console.log('Migrate completed'))
    .catch(err => {
        if (err.statusCode !== 404) {
            console.error(err);
        }
    });
