/* eslint-disable no-console,no-unused-vars */
/*
* Use with caution!!!
* */
import Promise from 'bluebird';
import Sequelize from 'sequelize';
import { User, Topic, Post, File, db } from '../models';

// disable logging
db.options.logging = () => {};

const source = new Sequelize(process.env.SOURCE_DATABASE_URL, {
    define: {
        freezeTableName: true,
    },
});

async function migrateUser() {
    const users = await source.query(
        'SELECT u.id as oldId, u.name as displayName, u.email, bio, gender, date_of_birth as birthday, address, city, country, nationality, timezone, u.created_at as createdAt, profile_picture FROM users u INNER JOIN blog_articles b ON u.id = b.`auth_id` GROUP BY oldId',
        { model: User, type: source.QueryTypes.SELECT },
    );

    try {
        await Promise.all(
            users.map(u => {
                const { profile_picture, ...data } = u.dataValues;
                return User.findOrCreate({
                    where: { oldId: u.oldId },
                    defaults: {
                        ...data,
                        birthday:
                            data.birthday &&
                            data.birthday.toString() === 'Invalid Date'
                                ? null
                                : u.birthday,
                        country: u.country === '--' ? null : u.country,
                        gender: u.gender === 'male' ? '1' : '0',
                        nationality:
                            u.nationality === '--' ? null : u.nationality,
                    },
                }).spread((user, _) =>
                    File.create(
                        {
                            remote: profile_picture,
                            userId: user.id,
                            attachedTo: 'user',
                            type: 'photo',
                        },
                        { validate: false },
                    ).then(file => user.setPhoto(file)),
                );
            }),
        );
    } catch (error) {
        console.error(error);
    }
}

async function migrateTopic() {
    const topics = await source.query(
        "SELECT c.id as oldId, t.name as displayName, description from blog_categories c INNER JOIN blog_category_translations t ON t.cat_id = c.id AND locale = 'en' WHERE c.type = 0;",
        { model: Topic, type: source.QueryTypes.SELECT },
    );

    try {
        await Promise.all(
            topics.map(t =>
                Topic.findOrCreate({
                    where: { oldId: t.oldId },
                    defaults: { ...t.dataValues, type: 'tag' },
                }),
            ),
        );
    } catch (error) {
        console.error(error);
    }
}

async function migratePost() {
    const posts = await source.query(
        'SELECT DISTINCT(categories.art_id) as oldId, a.auth_id as oldAuhtorId, a.featured_image, t.title as displayName, t.slug, t.meta_description as description, t.content as html, a.created_at as createdAt, views as viewCount FROM blog_categories_articles categories' +
            ' INNER JOIN blog_articles a ON a.id = categories.art_id' +
            ' INNER JOIN blog_article_translations t ON a.id = t.art_id' +
            ' WHERE a.status = 1 AND categories.cat_id IN (select c.id from blog_categories c where c.type = 0)',
        { model: Post, type: source.QueryTypes.SELECT },
    );

    try {
        await Promise.all(
            posts.map(p => {
                const {
                    html,
                    oldAuhtorId,
                    featured_image,
                    ...data
                } = p.dataValues;
                return Post.findOrCreate({
                    where: { oldId: data.oldId },
                    defaults: {
                        ...data,
                        slug: `${data.slug}-${data.oldId}`,
                        status: 'PENDING',
                    },
                }).spread((post, _) =>
                    Promise.all([
                        post.meta('oldHtml', html),
                        User.findOne({
                            where: { oldId: oldAuhtorId },
                        }).then(user =>
                            Promise.all([
                                post.setAuthor(user),
                                File.create(
                                    {
                                        remote: featured_image,
                                        type: 'photo',
                                        attachedTo: 'post',
                                        userId: user.id,
                                    },
                                    { validate: false },
                                ).then(file =>
                                    Post.update(
                                        { photoId: file.id },
                                        { where: { id: post.id } },
                                    ),
                                ),
                            ]),
                        ),
                        source
                            .query(
                                'SELECT cat_id from blog_categories_articles WHERE art_id = :postId',
                                {
                                    replacements: { postId: post.oldId },
                                    type: source.QueryTypes.SELECT,
                                },
                            )
                            .then(rows => rows.map(row => row.cat_id))
                            .then(ids =>
                                Topic.findAll({
                                    where: { oldId: { $in: ids } },
                                }),
                            )
                            .then(tags => post.setTags(tags)),
                    ]),
                );
            }),
        );
    } catch (error) {
        console.error(error);
    }
}

const promise = db.sync({ force: true }).catch(err => console.error(err.stack));
promise
    .then(() => Promise.all([migrateUser(), migrateTopic()]))
    .then(() => migratePost())
    .then(() => Promise.all([source.close(), db.close()]))
    .then(() => console.log('Migrate completed'));
