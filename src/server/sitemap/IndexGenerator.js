import xml from 'xml';
import moment from 'moment';
import utils from '../utils';
import localUtils from './utils';

const XMLNS_DECLS = {
    _attr: {
        xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
    },
};

const generateSiteMapUrlElements = resources =>
    resources.map(resource => {
        const url = utils.url.urlFor(`/sitemap-${resource.name}.xml`);
        const lastModified = resource.lastModified;

        return {
            sitemap: [
                { loc: url },
                { lastmod: moment(lastModified).toISOString() },
            ],
        };
    });

class IndexGenerator {
    constructor({ resources = {} }) {
        this.resources = resources;
    }

    getIndexXml() {
        const urlElements = generateSiteMapUrlElements(this.resources);
        const data = {
            // Concat the elements to the _attr declaration
            sitemapindex: [XMLNS_DECLS].concat(urlElements),
        };

        // Return the xml
        return localUtils.getDeclarations() + xml(data);
    }
}

export default IndexGenerator;
