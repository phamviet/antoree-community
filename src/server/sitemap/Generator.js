/* eslint-disable class-methods-use-this */
import Promise from 'bluebird';
import path from 'path';
import xml from 'xml';
import moment from 'moment';
import _ from 'lodash';
import assign from 'object-assign';
import events from '../events';
import localUtils from './utils';

const CHANGE_FREQ = 'weekly';

// Sitemap specific xml namespace declarations that should not change
const XMLNS_DECLS = {
    _attr: {
        xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
        'xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1',
    },
};

class Generator {
    name = 'base';
    constructor(opts = {}) {
        _.extend(
            this,
            assign(
                {},
                {
                    lastModified: 0,
                    getData: () => Promise.resolve([]),
                    getPriority: () => 1.0,
                    getUrlLocation: () => '/',
                    getLastModified: () => 0,
                    validate: () => true,
                    changeFreq: () => CHANGE_FREQ,
                    addOrUpdateEvents: [],
                    removeEvents: [],
                },
                opts,
            ),
        );

        this.nodeLookup = {};
        this.nodeTimeLookup = {};
        this.siteMapContent = '';
    }

    init() {
        return this.refreshAll()
            .then(() => this.bindEvents())
            .then(() => events.emit('sitemap:initialized', this));
    }

    bindEvents() {
        events.onMany(this.addOrUpdateEvents, this.addOrUpdateUrl);
        events.onMany(this.removeEvents, this.removeUrl);
    }

    refreshAll() {
        return this.getData()
            .then(data => this.generateXmlFromData(data))
            .then(generatedXml => (this.siteMapContent = generatedXml));
    }

    addOrUpdateUrl = model => {
        const node = this.createUrlNodeFromDatum(model);

        if (node) {
            this.updateLastModified(model);
            // TODO: Check if the node values changed, and if not don't regenerate
            this.updateLookups(model, node);
            this.updateXmlFromNodes();
            events.emit('sitemap:updated', this);
        }
    };

    removeUrl = model => {
        this.removeFromLookups(model);
        this.lastModified = Date.now();
        this.updateXmlFromNodes();
        events.emit('sitemap:updated', this);
    };

    generateXmlFromData(data) {
        _.reduce(
            data,
            (nodeArr, datum) => {
                const node = this.createUrlNodeFromDatum(datum);

                if (node) {
                    this.updateLastModified(datum);
                    this.updateLookups(datum, node);
                    nodeArr.push(node);
                }

                return nodeArr;
            },
            [],
        );

        return this.generateXmlFromNodes();
    }

    generateXmlFromNodes() {
        const timedNodes = _.map(
            this.nodeLookup,
            (node, id) => ({
                id,
                // Using negative here to sort newest to oldest
                ts: -(this.nodeTimeLookup[id] || 0),
                node,
            }),
            [],
        );
        // Sort nodes by timestamp
        const sortedNodes = _.sortBy(timedNodes, 'ts');

        // Grab just the nodes
        const urlElements = _.map(sortedNodes, 'node');
        const data = {
            // Concat the elements to the _attr declaration
            urlset: [XMLNS_DECLS].concat(urlElements),
        };

        // Return the xml
        return localUtils.getDeclarations() + xml(data);
    }

    createUrlNodeFromDatum(datum) {
        if (!this.validate(datum)) {
            return false;
        }

        const url = this.getUrlLocation(datum);
        const priority = this.getPriority(datum);

        const node = {
            url: [
                { loc: url },
                {
                    lastmod: moment(this.getLastModified(datum)).toISOString(),
                },
                { changefreq: this.changeFreq(datum) },
                { priority },
            ],
        };

        const imgNode = this.createImageNodeFromDatum(datum);

        if (imgNode) {
            node.url.push(imgNode);
        }

        return node;
    }

    createImageNodeFromDatum(datum) {
        // Check for cover first because user has cover but the rest only have image
        const image = datum.photo;

        if (!image) {
            return null;
        }

        // Grab the image url
        const imageUrl = this.getUrlForImage(image);

        // Verify the url structure
        if (!this.validateImageUrl(imageUrl)) {
            return null;
        }

        // Create the weird xml node syntax structure that is expected
        const imageEl = [
            { 'image:loc': imageUrl },
            { 'image:caption': path.basename(imageUrl) },
        ];

        // Return the node to be added to the url xml node
        return {
            'image:image': imageEl,
        };
    }

    updateXmlFromNodes() {
        const content = this.generateXmlFromNodes();

        this.setSiteMapContent(content);

        return content;
    }

    getUrlForImage(image) {
        return image.getCanonicalUrl({ w: 600, h: 315 });
    }

    validateImageUrl(imageUrl) {
        return !!imageUrl;
    }

    setSiteMapContent(content) {
        this.siteMapContent = content;
    }

    updateLastModified(datum) {
        const lastModified = this.getLastModified(datum);

        if (lastModified > this.lastModified) {
            this.lastModified = lastModified;
        }
    }

    updateLookups(datum, node) {
        this.nodeLookup[datum.id] = node;
        this.nodeTimeLookup[datum.id] = this.getLastModified(datum);
    }

    removeFromLookups(datum) {
        let lookup = this.nodeLookup;
        delete lookup[datum.id];

        lookup = this.nodeTimeLookup;
        delete lookup[datum.id];
    }
}

export const createGenerator = ({ name, ...opts }) => {
    const resource = new Generator(opts);
    resource.name = name;

    return resource;
};

export default Generator;
