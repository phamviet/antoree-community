import Promise from 'bluebird';
import { STATUS_PUBLISHED } from 'shared/constants/post';
import events from '../events';
import SiteMapManager from './SiteMapManager';
import { Post } from '../models';
import { urlFor } from '../utils/url';
import { createGenerator } from './Generator';

const maxAge = 3600 * 24; // 1 day

const posts = createGenerator({
    name: 'posts',
    getData: () =>
        Post.findAll({
            attributes: ['id', 'slug', 'status', 'publishedAt'],
            where: { status: STATUS_PUBLISHED },
        }),
    getPriority: () => 0.8, // todo use 0.9 for slider's posts
    getLastModified: post => post.publishedAt,
    getUrlLocation: post => urlFor(Post.getHref(post.slug)),
    validate: post => post.status === STATUS_PUBLISHED,
    addOrUpdateEvents: ['post:published'],
    removeEvents: ['post:unpublished'],
});

const misc = createGenerator({
    name: 'misc',
    getData: () =>
        Promise.resolve([
            {
                path: '/',
                lastModified: Date.now(),
                changeFreq: 'daily',
                priority: 1.0,
            },
        ]),
    getPriority: ({ priority }) => priority,
    getLastModified: ({ path, lastModified }) =>
        path === '/' ? posts.lastModified : lastModified,
    getUrlLocation: ({ path }) => urlFor(path),
    changeFreq: ({ changeFreq }) => changeFreq,
});

// Home's lastModified is based on post lastModified
events.onMany(['sitemap:initialized', 'sitemap:updated'], updatedSitemap => {
    if (updatedSitemap.name === posts.name) {
        misc.lastModified = updatedSitemap.lastModified;
        misc.refreshAll();
    }
});

const sitemap = new SiteMapManager({ resources: [misc, posts] });

const verifyResourceType = (req, res, next) => {
    if (!sitemap.hasResource(req.params.resource)) {
        return res.sendStatus(404);
    }

    return next();
};

export default app => {
    app.get('/sitemap.xml', (req, res, next) => {
        let siteMapXml = sitemap.getIndexXml();
        res.set({
            'Cache-Control': `public, max-age=${maxAge}`,
            'Content-Type': 'text/xml',
        });

        // CASE: returns null if sitemap is not initialized as below
        if (!siteMapXml) {
            sitemap
                .init()
                .then(() => {
                    siteMapXml = sitemap.getIndexXml();
                    res.send(siteMapXml);
                })
                .catch(next);
        } else {
            res.send(siteMapXml);
        }
    });

    app.get('/sitemap-:resource.xml', verifyResourceType, (req, res, next) => {
        const type = req.params.resource;
        const page = 1;
        let siteMapXml = sitemap.getSiteMapXml(type, page);

        res.set({
            'Cache-Control': `public, max-age=${maxAge}`,
            'Content-Type': 'text/xml',
        });

        // CASE: returns null if sitemap is not initialized as below
        if (!siteMapXml) {
            sitemap
                .init()
                .then(() => {
                    siteMapXml = sitemap.getSiteMapXml(type, page);
                    res.send(siteMapXml);
                })
                .catch(next);
        } else {
            res.send(siteMapXml);
        }
    });
};
