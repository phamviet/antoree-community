import Promise from 'bluebird';
import IndexGenerator from './IndexGenerator';

class SiteMapManager {
    constructor(opts = {}) {
        this.initialized = false;

        this.resources = opts.resources || [];
        this.index =
            opts.index || new IndexGenerator({ resources: this.resources });
    }

    init() {
        return Promise.map(this.resources, resource =>
            resource.init(),
        ).then(() => {
            this.initialized = true;
        });
    }

    getIndexXml() {
        if (!this.initialized) {
            return '';
        }

        return this.index.getIndexXml();
    }

    getSiteMapXml(type) {
        if (!this.initialized || !this.hasResource(type)) {
            return null;
        }

        return this.getResource(type).siteMapContent;
    }

    hasResource(name) {
        return this.resources.some(resource => resource.name === name);
    }

    getResource(name) {
        return this.resources
            .filter(resource => resource.name === name)
            .shift();
    }
}

export default SiteMapManager;
