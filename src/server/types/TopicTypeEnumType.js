import { GraphQLEnumType } from 'graphql';

export const TOPIC = null;
export const TAG = 'tag';

const TopicTypeEnumType = new GraphQLEnumType({
    name: 'TopicType',
    values: {
        TOPIC: { value: TOPIC },
        TAG: { value: TAG },
    },
});

export default TopicTypeEnumType;
