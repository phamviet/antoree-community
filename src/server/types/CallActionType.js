import { GraphQLObjectType as ObjectType, GraphQLBoolean } from 'graphql';
import GraphQLJSON from 'graphql-type-json';

const CallActionType = new ObjectType({
    name: 'CallAction',
    fields: () => ({
        success: { type: GraphQLBoolean },
        data: { type: GraphQLJSON },
    }),
});

export default CallActionType;
