/**
 * Antoree Community
 *
 * Copyright © 2017-present Antoree
 */

import {
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLNonNull as NonNull,
    GraphQLInputObjectType,
    GraphQLList,
} from 'graphql';

import TopicTypeEnumType from './TopicTypeEnumType';
import CollectionEnumType from './CollectionEnumType';

const TopicInputType = new GraphQLInputObjectType({
    name: 'TopicInput',
    fields: {
        id: { type: ID },
        displayName: { type: new NonNull(StringType) },
        description: { type: StringType },
        photoId: { type: ID },
        type: { type: TopicTypeEnumType },
        collections: { type: new GraphQLList(CollectionEnumType) },
    },
});

export default TopicInputType;
