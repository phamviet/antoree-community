import { GraphQLEnumType } from 'graphql';

export const DISPLAY_NAME = 'displayName';
export const CREATED_AT = 'createdAt';
export const UPDATED_AT = 'updatedAt';
export const PUBLISHED_AT = 'publishedAt';
export const APPROVED_AT = 'approvedAt';

const OrderByEnumType = new GraphQLEnumType({
    name: 'OrderBy',
    values: {
        DISPLAY_NAME: { value: DISPLAY_NAME },
        CREATED_AT: { value: CREATED_AT },
        UPDATED_AT: { value: UPDATED_AT },
        PUBLISHED_AT: { value: PUBLISHED_AT },
        APPROVED_AT: { value: APPROVED_AT },
    },
});

export default OrderByEnumType;
