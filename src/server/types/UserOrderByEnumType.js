import { GraphQLEnumType } from 'graphql';
import { DISPLAY_NAME, CREATED_AT, UPDATED_AT } from './OrderByEnumType';

const UserOrderByEnumType = new GraphQLEnumType({
    name: 'UserOrderByEnum',
    values: {
        DISPLAY_NAME: { value: DISPLAY_NAME },
        CREATED_AT: { value: CREATED_AT },
        UPDATED_AT: { value: UPDATED_AT },
    },
});

export default UserOrderByEnumType;
