import { GraphQLEnumType } from 'graphql';
import {
    FEED,
    DETAIL,
    CARD_HORIZONTAL,
    CARD_VERTICAL,
} from 'shared/constants/imageSizes';

const ImageSizeEnumType = new GraphQLEnumType({
    name: 'ImageSizeEnum',
    values: {
        FEED: { value: FEED },
        DETAIL: { value: DETAIL },
        CARD_HORIZONTAL: { value: CARD_HORIZONTAL },
        CARD_VERTICAL: { value: CARD_VERTICAL },
    },
});

export default ImageSizeEnumType;
