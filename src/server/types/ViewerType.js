import {
    GraphQLObjectType as ObjectType,
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLInt,
    GraphQLList,
} from 'graphql';
import { UserError } from 'graphql-errors';
import { forwardConnectionArgs } from 'graphql-relay';
import {
    UserConnection,
    connectionArgs as userConnectionArgs,
    resolveConnection as resolveUserConnection,
} from '../types/UserType';
import PostType, {
    PostConnection,
    postConnectionArgs,
    resolveConnection as resolvePostConnection,
} from './PostType';
import { knex, Post } from '../models';
import { sortingArgs } from '../queries';
import UserRoleEnumType from './UserRoleEnumType';

const ViewerType = new ObjectType({
    name: 'Viewer',
    fields: {
        id: { type: ID, resolve: ({ id }) => `viewer-${id}` },
        email: { type: StringType },
        displayName: { type: StringType },
        role: { type: UserRoleEnumType },
        timezone: { type: StringType },
        search: {
            type: PostConnection,
            description: 'Post list',
            args: {
                ...forwardConnectionArgs,
                first: { ...forwardConnectionArgs.first, defaultValue: 10 },
                ...postConnectionArgs,
                ...sortingArgs,
            },
            resolve(_, args, cxt) {
                const qb = Post.buildQuery(args);
                return resolvePostConnection(qb, args, cxt);
            },
        },
        users: {
            type: UserConnection,
            description: 'User list',
            args: userConnectionArgs,
            resolve: (_, args, ctx) => {
                const qb = knex.from('users');
                const query = args.query ? args.query.trim() : '';

                if (query) {
                    if (query.indexOf('@') > 0) {
                        qb.where('email', '=', query);
                    } else {
                        qb.where('displayName', 'like', `%${query}%`);
                    }
                }

                if (args.disabled) {
                    qb.where('disabled', '=', 1);
                }

                if (args.locked) {
                    qb.whereNotNull('lockedTo');
                }

                if (args.role) {
                    qb.whereRaw('(role & ?) > 0', parseInt(args.role, 10));
                }

                return resolveUserConnection(qb, args, ctx);
            },
        },
        bookmarks: {
            type: new GraphQLList(PostType),
            args: {
                limit: {
                    type: GraphQLInt,
                    defaultValue: 10,
                },
                offset: {
                    type: GraphQLInt,
                },
            },
            resolve: (me, args, { user, Posts }) => {
                if (!user || user.id !== me.id) {
                    throw new UserError('Permission denied');
                }

                return me
                    .getBookmarkedPostIds(args)
                    .then(ids => (ids ? Posts.loadMany(ids) : []));
            },
        },
    },
});

export default ViewerType;
