/**
 * Antoree Community
 *
 * Copyright © 2017-present Antoree
 */

import {
    GraphQLID as ID,
    GraphQLNonNull as NonNull,
    GraphQLString as StringType,
    GraphQLInputObjectType,
} from 'graphql';

const UserInputType = new GraphQLInputObjectType({
    name: 'UserInput',
    fields: {
        id: { type: new NonNull(ID) },
        displayName: { type: StringType },
        photoId: { type: ID },
        bio: { type: StringType },
    },
});

export default UserInputType;
