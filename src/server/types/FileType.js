import {
    GraphQLObjectType as ObjectType,
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLNonNull as NonNull,
    GraphQLInt,
} from 'graphql';

import imgix from '../imgix';
import FileTypeEnumType from './FileTypeEnumType';

const FileType = new ObjectType({
    name: 'File',
    fields: () => ({
        id: { type: new NonNull(ID) },
        filename: { type: StringType },
        mimeType: { type: StringType },
        type: { type: FileTypeEnumType },
        remote: { type: StringType },
        url: {
            type: StringType,
            args: {
                w: {
                    type: GraphQLInt,
                },
                h: {
                    type: GraphQLInt,
                },
                fit: {
                    type: StringType,
                },
            },
            resolve: (file, args) =>
                file.filename ? imgix.buildURL(file.filename, args) : file.url,
        },
    }),
});

export default FileType;
