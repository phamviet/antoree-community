import { GraphQLEnumType } from 'graphql';
import {
    STATUS_PUBLISHED,
    STATUS_PENDING,
    STATUS_DRAFT,
} from 'shared/constants/post';

const PostStatusEnumType = new GraphQLEnumType({
    name: 'PostStatusEnum',
    values: {
        DRAFT: { value: STATUS_DRAFT },
        PENDING: { value: STATUS_PENDING },
        PUBLISHED: { value: STATUS_PUBLISHED },
    },
});

export default PostStatusEnumType;
