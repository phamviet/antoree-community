import {
    GraphQLObjectType as ObjectType,
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLNonNull as NonNull,
    GraphQLList,
} from 'graphql';

import DateType from 'graphql-date';
import UserType from './UserType';
import PostType from './PostType';
import CommentStatusEnumType from './CommentStatusEnumType';

const CommentType = new ObjectType({
    name: 'Comment',
    fields: () => ({
        id: { type: new NonNull(ID) },
        content: { type: new NonNull(StringType) },
        author: {
            type: UserType,
            resolve: (comment, _, { Users }) => Users.load(comment.authorId),
        },
        post: {
            type: PostType,
            resolve: (comment, _, { Posts }) => Posts.load(comment.postId),
        },
        status: { type: CommentStatusEnumType },
        children: {
            type: new GraphQLList(CommentType),
            resolve: comment => comment.getChildren(),
        },
        createdAt: { type: DateType },
        updatedAt: { type: DateType },
    }),
});

export default CommentType;
