import {
    GraphQLObjectType as ObjectType,
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLNonNull as NonNull,
} from 'graphql';
import { forwardConnectionArgs } from 'graphql-relay';
import { TOPIC } from 'shared/constants/entity';
import EntityInterface from './EntityInterface';
import EntityTypeEnum from './EntityTypeEnumType';
import TopicTypeEnumType from './TopicTypeEnumType';
import FileType from './FileType';
import {
    PostConnection,
    postConnectionArgs,
    resolveConnection,
} from './PostType';
import { Post } from '../models';
import { sortingArgs } from '../queries';

const TopicType = new ObjectType({
    name: TOPIC,
    interfaces: () => [EntityInterface],
    fields: () => ({
        id: { type: new NonNull(ID) },
        entityType: { type: new NonNull(EntityTypeEnum) },
        displayName: { type: new NonNull(StringType) },
        href: {
            type: new NonNull(StringType),
            resolve: topic => `/topic/${topic.slug}/`,
        },
        slug: { type: new NonNull(StringType) },
        description: { type: StringType },
        type: { type: TopicTypeEnumType },
        photo: {
            type: FileType,
            resolve: (topic, _, { Files }) =>
                topic.photoId ? Files.load(topic.photoId) : null,
        },
        posts: {
            type: PostConnection,
            args: {
                ...forwardConnectionArgs,
                first: { ...forwardConnectionArgs.first, defaultValue: 10 },
                ...postConnectionArgs,
                ...sortingArgs,
            },
            resolve(topic, args, cxt) {
                const qb = Post.buildQuery({ ...args, tagIds: [topic.id] });
                return resolveConnection(qb, args, cxt);
            },
        },
    }),
});

export default TopicType;
