import { GraphQLEnumType } from 'graphql';
import { FLAG_FEATURED } from 'shared/constants/post';

const PostFlagEnumType = new GraphQLEnumType({
    name: 'PostFlagEnum',
    values: {
        FLAG_FEATURED: { value: FLAG_FEATURED },
    },
});

export default PostFlagEnumType;
