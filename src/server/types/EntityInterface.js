import {
    GraphQLInterfaceType,
    GraphQLID as ID,
    GraphQLNonNull as NonNull,
    GraphQLString,
} from 'graphql';
import { POST, TOPIC, USER } from 'shared/constants/entity';
import EntityTypeEnumType from './EntityTypeEnumType';
import PostType from './PostType';
import TopicType from './TopicType';
import UserType from './UserType';
import FileType from './FileType';

const EntityInterface = new GraphQLInterfaceType({
    name: 'EntityInterface',
    description: 'A entity type',
    fields: () => ({
        id: { type: new NonNull(ID) },
        entityType: { type: new NonNull(EntityTypeEnumType) },
        displayName: { type: new NonNull(GraphQLString) },
        href: { type: new NonNull(GraphQLString) },
        photo: { type: FileType },
    }),
    resolveType(e) {
        if (e.entityType === POST) {
            return PostType;
        }

        if (e.entityType === TOPIC) {
            return TopicType;
        }

        if (e.entityType === USER) {
            return UserType;
        }

        throw new Error(`Could not resolve entity type "${e.entityType}"`);
    },
});

export default EntityInterface;
