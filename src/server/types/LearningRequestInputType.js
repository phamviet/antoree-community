import {
    GraphQLString as StringType,
    GraphQLNonNull,
    GraphQLInt,
    GraphQLInputObjectType,
    GraphQLBoolean,
} from 'graphql';
import { GraphQLEmail, GraphQLUUID } from 'graphql-custom-types';

const LearningRequestInputType = new GraphQLInputObjectType({
    name: 'LearningRequestInput',
    fields: {
        postId: { type: new GraphQLNonNull(GraphQLUUID) },
        name: { type: new GraphQLNonNull(StringType) },
        email: { type: new GraphQLNonNull(GraphQLEmail) },
        phone: { type: new GraphQLNonNull(StringType) },
        register: { type: GraphQLInt, defaultValue: 0 },
        subscribed: { type: GraphQLBoolean, defaultValue: true },
    },
});

export default LearningRequestInputType;
