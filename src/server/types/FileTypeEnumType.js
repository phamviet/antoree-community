import { GraphQLEnumType } from 'graphql';

const FileTypeEnumType = new GraphQLEnumType({
    name: 'FileType',
    values: {
        UNKNOWN: { value: null },
        PHOTO: { value: 'photo' },
        VIDEO: { value: 'video' },
    },
});

export default FileTypeEnumType;
