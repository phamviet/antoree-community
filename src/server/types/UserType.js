import {
    GraphQLObjectType as ObjectType,
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLNonNull as NonNull,
    GraphQLInt,
    GraphQLBoolean,
    GraphQLList,
} from 'graphql';

import {
    forwardConnectionArgs,
    connectionDefinitions,
    connectionFromArraySlice,
    cursorToOffset,
} from 'graphql-relay';
import { UserError } from 'graphql-errors';
import moment from 'moment';
import countryData from 'country-data';
import { USER } from 'shared/constants/entity';
import EntityInterface from './EntityInterface';
import EntityTypeEnum from './EntityTypeEnumType';
import FileType from './FileType';
import PostType from './PostType';
import UserRoleEnumType from './UserRoleEnumType';
import UserOrderByEnumType from './UserOrderByEnumType';
import SortEnumType, { ASC } from './SortEnumType';

const { countries } = countryData;

const connectionArgs = {
    ...forwardConnectionArgs,
    first: { ...forwardConnectionArgs.first, defaultValue: 10 },
    query: { type: StringType },
    role: { type: UserRoleEnumType },
    disabled: { type: GraphQLBoolean },
    locked: { type: GraphQLBoolean },
    orderBy: { type: UserOrderByEnumType },
    sort: { type: SortEnumType, defaultValue: ASC },
};

const getCountryName = code => {
    if (!code) {
        return code;
    }
    const validCode = code.toUpperCase();
    return countries[validCode] ? countries[validCode].name : null;
};

const UserType = new ObjectType({
    name: USER,
    interfaces: () => [EntityInterface],
    fields: {
        id: { type: new NonNull(ID) },
        entityType: { type: new NonNull(EntityTypeEnum) },
        email: { type: new NonNull(StringType) },
        displayName: { type: new NonNull(StringType) },
        href: {
            type: new NonNull(StringType),
            resolve: user => `/profile/${user.id}/`,
        },
        photo: {
            type: FileType,
            resolve: (user, _, { Files }) =>
                user.photoId ? Files.load(user.photoId) : null,
        },
        isSubscribed: {
            type: GraphQLBoolean,
            resolve: (user, _, { UserIsSubscribed }) =>
                UserIsSubscribed.load(user.id),
        },
        isAdministrator: {
            type: GraphQLBoolean,
            resolve: user => user.isAdministrator(),
        },
        isEditor: {
            type: GraphQLBoolean,
            resolve: user => user.isEditor(),
        },
        role: { type: UserRoleEnumType },
        address: { type: StringType },
        age: {
            type: GraphQLInt,
            resolve: user => {
                if (user.birthday) {
                    const age = moment().diff(user.birthday, 'years');
                    if (age > 0) {
                        return age;
                    }
                }

                return null;
            },
        },
        city: { type: StringType },
        country: {
            type: StringType,
            resolve: user => getCountryName(user.country),
        },
        nationality: {
            type: StringType,
            resolve: user => getCountryName(user.nationality),
        },
        timezone: { type: StringType },
        isTeacher: { type: GraphQLBoolean },
        teachingProfileUrl: {
            type: StringType,
            resolve: user =>
                user.externalTeacherId
                    ? `https://antoree.com/vi/giao-vien/${user.externalTeacherId}`
                    : 'https://antoree.com/vi/tim-giao-vien',
        },
        bookmarks: {
            type: new GraphQLList(PostType),
            args: {
                limit: {
                    type: GraphQLInt,
                    defaultValue: 10,
                },
                offset: {
                    type: GraphQLInt,
                },
            },
            resolve: (me, args, { user, Posts }) => {
                if (!user || !user.isEditor()) {
                    throw new UserError('Permission denied');
                }

                return me
                    .getBookmarkedPostIds(args)
                    .then(ids => (ids ? Posts.loadMany(ids) : []));
            },
        },
    },
});

const { connectionType: UserConnection } = connectionDefinitions({
    nodeType: UserType,
    connectionFields: {
        totalCount: { type: new NonNull(GraphQLInt) },
    },
});

const resolveConnection = async (qb, args, { Users }) => {
    const limit = args.first;
    const offset = args.after ? cursorToOffset(args.after) + 1 : 0;
    const countQuery = qb.clone();

    if (args.orderBy) {
        qb.orderBy(args.orderBy, args.sort);
    }

    if (limit) {
        qb.limit(limit).offset(offset);
    }

    const [data, totalCount] = await Promise.all([
        qb
            .select('id')
            .then(rows => rows.map(x => x.id))
            .then(ids => (ids.length ? Users.loadMany(ids) : [])),
        countQuery.count('* as count').then(x => x[0].count),
    ]);

    return {
        ...connectionFromArraySlice(data, args, {
            sliceStart: offset,
            arrayLength: totalCount,
        }),
        totalCount,
    };
};

export { UserConnection, connectionArgs, resolveConnection };
export default UserType;
