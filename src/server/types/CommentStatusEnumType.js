import { GraphQLEnumType } from 'graphql';
import { APPROVED, PENDING } from 'shared/constants';

const CommentStatusEnumType = new GraphQLEnumType({
    name: 'CommentStatus',
    values: {
        PENDING: { value: APPROVED },
        APPROVED: { value: PENDING },
    },
});

export default CommentStatusEnumType;
