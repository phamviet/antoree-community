/**
 * Antoree Community
 *
 * Copyright © 2017-present Antoree
 */

import {
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLNonNull as NonNull,
    GraphQLInputObjectType,
    GraphQLList,
} from 'graphql';

import TagInputType from './TopicInputType';
import PostStatusEnumType from './PostStatusEnumType';
import CollectionEnumType from './CollectionEnumType';

const PostInputType = new GraphQLInputObjectType({
    name: 'PostInput',
    fields: {
        id: { type: ID },
        displayName: { type: new NonNull(StringType) },
        description: { type: StringType },
        slug: { type: StringType },
        raw: { type: StringType },
        tags: { type: new GraphQLList(TagInputType) },
        collections: { type: new GraphQLList(CollectionEnumType) },
        photoId: { type: ID },
        authorId: { type: ID },
        status: { type: PostStatusEnumType },
    },
});

export default PostInputType;
