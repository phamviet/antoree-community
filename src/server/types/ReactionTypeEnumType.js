import { GraphQLEnumType } from 'graphql';
import { REACTION_LIKE, REACTION_BOOKMARK } from 'shared/constants';

const ReactionTypeEnumType = new GraphQLEnumType({
    name: 'ReactionType',
    values: {
        LIKE: { value: REACTION_LIKE },
        BOOKMARK: { value: REACTION_BOOKMARK },
    },
});

export default ReactionTypeEnumType;
