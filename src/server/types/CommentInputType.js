/**
 * Antoree Community
 *
 * Copyright © 2017-present Antoree
 */

import {
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLNonNull as NonNull,
    GraphQLInputObjectType,
} from 'graphql';

const CommentInputType = new GraphQLInputObjectType({
    name: 'CommentInputType',
    fields: {
        id: { type: ID },
        content: { type: new NonNull(StringType) },
        parentId: { type: StringType },
        status: { type: StringType },
        postId: { type: StringType },
        authorId: { type: StringType },
    },
});

export default CommentInputType;
