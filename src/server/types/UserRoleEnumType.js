import { GraphQLEnumType } from 'graphql';
import { USER, EDITOR, ADMINISTRATOR } from 'shared/constants/roles';

const UserRoleEnumType = new GraphQLEnumType({
    name: 'UserRoleEnum',
    values: {
        USER: { value: USER },
        EDITOR: { value: EDITOR },
        ADMINISTRATOR: { value: ADMINISTRATOR },
    },
});

export default UserRoleEnumType;
