import { GraphQLEnumType } from 'graphql';
import { USER, TOPIC, POST, COLLECTION } from 'shared/constants/entity';

const EntityTypeEnumType = new GraphQLEnumType({
    name: 'EntityTypeEnum',
    values: {
        POST: { value: POST },
        TOPIC: { value: TOPIC },
        USER: { value: USER },
        COLLECTION: { value: COLLECTION },
    },
});

export default EntityTypeEnumType;
