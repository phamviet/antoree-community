import {
    GraphQLObjectType as ObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt,
    GraphQLBoolean,
} from 'graphql';
import {
    connectionDefinitions,
    connectionFromArraySlice,
    cursorToOffset,
} from 'graphql-relay';
import convertFromRawToDraftState from 'draft-js/lib/convertFromRawToDraftState';
import DateType from 'graphql-date';
import GraphQLJSON from 'graphql-type-json';
import { POST } from 'shared/constants/entity';
import { PUBLISHED } from 'shared/constants';
import { Post } from '../models';
import exportHtml from '../utils/draft-exporter';
import EntityInterface from './EntityInterface';
import EntityTypeEnum from './EntityTypeEnumType';
import UserType from './UserType';
import TopicType from './TopicType';
import FileType from './FileType';
import CommentType from './CommentType';
import PostStatusEnumType from './PostStatusEnumType';
import PostFlagEnumType from './PostFlagEnumType';

const postConnectionArgs = {
    query: { type: GraphQLString },
    flag: { type: PostFlagEnumType },
    statuses: {
        type: new GraphQLList(PostStatusEnumType),
        defaultValue: PUBLISHED,
    },
    notIds: { type: new GraphQLList(GraphQLID) },
    authorIds: { type: new GraphQLList(GraphQLID) },
    topicIds: { type: new GraphQLList(GraphQLID) },
    publishedBefore: { type: DateType },
};

const PostType = new ObjectType({
    name: POST,
    interfaces: () => [EntityInterface],
    fields: () => ({
        id: { type: new GraphQLNonNull(GraphQLID) },
        entityType: { type: new GraphQLNonNull(EntityTypeEnum) },
        displayName: { type: new GraphQLNonNull(GraphQLString) },
        href: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: post => post.getHref(),
        },
        url: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: post => post.getCanonicalUrl(),
        },
        slug: { type: new GraphQLNonNull(GraphQLString) },
        authorId: { type: GraphQLString },
        author: {
            type: UserType,
            resolve: (post, _, { Users }) => Users.load(post.authorId),
        },
        tags: {
            type: new GraphQLList(TopicType),
            // resolve: (post, _, { TopicsByPostId }) =>
            //     TopicsByPostId.load(post.id),
            resolve: (post, _, { Topics }) =>
                post
                    .getPostTag()
                    .then(postTags => postTags.map(t => t.tagId))
                    .then(ids => (ids ? Topics.loadMany(ids) : [])),
        },
        comments: {
            type: new GraphQLList(CommentType),
            resolve: (post, _, { CommentsByPostId }) =>
                CommentsByPostId.load(post.id),
        },
        photoId: { type: GraphQLString },
        photo: {
            type: FileType,
            resolve: (post, _, { Files }) =>
                post.photoId ? Files.load(post.photoId) : null,
        },
        photos: {
            type: new GraphQLList(FileType),
        },
        relatedPosts: {
            type: new GraphQLList(PostType),
            args: {
                limit: {
                    type: GraphQLInt,
                    defaultValue: 3,
                },
            },
            resolve: (post, args, { Posts }) =>
                post
                    .getRelatedPostIds(args)
                    .then(ids => (ids ? Posts.loadMany(ids) : [])),
        },
        description: {
            type: GraphQLString,
        },
        html: {
            type: GraphQLString,
            args: {
                fromRaw: {
                    type: GraphQLBoolean,
                    defaultValue: false,
                    description: 'Ignore cached html',
                },
            },
            resolve: async (post, { fromRaw }) => {
                let html;

                // export performance testing
                if (!fromRaw) {
                    html = await post.meta('html');
                }

                if (!html) {
                    const raw = await post.meta('raw');
                    if (raw) {
                        try {
                            const contentState = convertFromRawToDraftState(
                                JSON.parse(raw),
                            );
                            html = exportHtml(contentState);
                        } catch (err) {
                            console.error(err);
                        }
                    }
                }

                if (html) {
                    post.meta('html', html);
                } else {
                    html = post.meta('oldHtml');
                }

                return html;
            },
        },
        raw: {
            type: GraphQLJSON,
            resolve: async post => {
                const raw = await post.meta('raw');
                if (raw) {
                    return raw;
                }

                return post.meta('oldHtml');
            },
        },
        status: { type: PostStatusEnumType },
        isKid: {
            type: GraphQLBoolean,
            resolve: post => Post.isKid(post.flags),
        },
        isFeatured: {
            type: GraphQLBoolean,
            resolve: post => Post.isFeatured(post.flags),
        },
        isLiked: {
            type: GraphQLBoolean,
            resolve: (post, _, { PostIsLiked }) => PostIsLiked.load(post.id),
        },
        isBookmarked: {
            type: GraphQLBoolean,
            resolve: (post, _, { PostIsBookmarked }) =>
                PostIsBookmarked.load(post.id),
        },
        likeCount: {
            type: GraphQLInt,
            resolve: (post, _, { PostLikesCount }) =>
                PostLikesCount.load(post.id),
        },
        commentCount: {
            type: GraphQLInt,
            resolve: (post, _, { PostCommentsCount }) =>
                PostCommentsCount.load(post.id),
        },
        shareCount: { type: GraphQLInt, resolve: () => 0 },
        oldId: { type: GraphQLString },
        createdAt: { type: DateType },
        publishedAt: { type: DateType },
        updatedAt: { type: DateType },
    }),
});

const { connectionType: PostConnection } = connectionDefinitions({
    nodeType: PostType,
    connectionFields: {
        totalCount: { type: new GraphQLNonNull(GraphQLInt) },
    },
});

const resolveConnection = async (qb, args, { Posts, notIn }) => {
    const limit = args.first;
    const offset = args.after ? cursorToOffset(args.after) + 1 : 0;
    const countQuery = qb.clone();

    if (args.orderBy) {
        qb.orderBy(args.orderBy, args.sort);
    }

    if (limit) {
        qb.limit(limit).offset(offset);
    }

    const [data, totalCount] = await Promise.all([
        qb.select('id').then(rows => rows.map(x => x.id)).then(ids => {
            ids.forEach(id => notIn.set(id, true));
            return ids.length ? Posts.loadMany(ids) : [];
        }),
        countQuery.count('* as count').then(x => x[0].count),
    ]);

    return {
        ...connectionFromArraySlice(data, args, {
            sliceStart: offset,
            arrayLength: totalCount,
        }),
        totalCount,
    };
};
export { PostConnection, postConnectionArgs, resolveConnection };
export default PostType;
