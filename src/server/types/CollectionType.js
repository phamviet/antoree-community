import {
    GraphQLObjectType as ObjectType,
    GraphQLID as ID,
    GraphQLString as StringType,
    GraphQLNonNull as NonNull,
    GraphQLList,
    GraphQLInt,
} from 'graphql';

import { TOPIC, USER, COLLECTION } from 'shared/constants/entity';
import { PUBLISHED } from 'shared/constants';
import EntityInterface from './EntityInterface';
import PostType from './PostType';
import TopicType from './TopicType';
import EntityTypeEnum from './EntityTypeEnumType';
import FileType from './FileType';
import SortEnumType, { ASC } from '../types/SortEnumType';
import OrderByEnumType, { CREATED_AT } from '../types/OrderByEnumType';
import { Post } from '../models';

const args = {
    sort: { type: SortEnumType, defaultValue: ASC },
    orderBy: { type: OrderByEnumType, defaultValue: CREATED_AT },
    limit: {
        type: GraphQLInt,
        defaultValue: 10,
    },
};

const entityResolver = (
    collection,
    { sort, orderBy, limit },
    { Topics, Users },
) =>
    collection
        .getEntityCollection({
            limit,
            order: [[orderBy, sort]],
        })
        .then(entityCollection => [
            entityCollection.length ? entityCollection[0].entityType : null,
            entityCollection.map(t => t.entityId),
        ])
        .then(([entityType, ids]) => {
            if (entityType && ids.length) {
                if (entityType === TOPIC) {
                    return Topics.loadMany(ids);
                }

                if (entityType === USER) {
                    return Users.loadMany(ids);
                }
            }

            return [];
        });

const CollectionType = new ObjectType({
    name: COLLECTION,
    interfaces: () => [EntityInterface],
    fields: {
        id: { type: new NonNull(ID) },
        entityType: { type: new NonNull(EntityTypeEnum) },
        displayName: { type: new NonNull(StringType) },
        href: {
            type: new NonNull(StringType),
            resolve: collection => `/collection/${collection.id}`,
        },
        description: { type: StringType },
        entities: {
            type: new GraphQLList(EntityInterface),
            args,
            resolve: entityResolver,
        },
        posts: {
            type: new GraphQLList(PostType),
            args,
            resolve: (collection, { sort, orderBy, limit }, { notIn, Posts }) =>
                collection
                    .getEntityCollection({
                        where: {
                            entityId: { $notIn: Array.from(notIn.keys()) },
                            '$post.status$': PUBLISHED,
                        },
                        include: [
                            {
                                model: Post,
                                attributes: ['id'],
                                as: 'post',
                            },
                        ],
                        limit,
                        order: [[orderBy, sort]],
                    })
                    .then(entityCollection =>
                        entityCollection.map(t => {
                            notIn.set(t.entityId, true);
                            return t.entityId;
                        }),
                    )
                    .then(ids => (ids.length ? Posts.loadMany(ids) : [])),
        },
        topics: {
            type: new GraphQLList(TopicType),
            args,
            resolve: entityResolver,
        },
        photo: {
            type: FileType,
            resolve: (user, _, { Files }) =>
                user.photoId ? Files.load(user.photoId) : null,
        },
    },
});

export default CollectionType;
