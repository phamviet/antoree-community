import { GraphQLEnumType } from 'graphql';
import {
    HOME_MASTHEAD_POSTS,
    HOME_FEATURED_POSTS,
} from 'shared/constants/post';
import { HOME_TOPICS_MENU, HOME_FEATURED_TOPICS } from 'shared/constants/topic';

const CollectionEnumType = new GraphQLEnumType({
    name: 'CollectionEnum',
    values: {
        HOME_TOPICS_MENU: { value: HOME_TOPICS_MENU },
        HOME_FEATURED_TOPICS: { value: HOME_FEATURED_TOPICS },
        HOME_MASTHEAD_POSTS: { value: HOME_MASTHEAD_POSTS },
        HOME_FEATURED_POSTS: { value: HOME_FEATURED_POSTS },
    },
});

export default CollectionEnumType;
