import { GraphQLEnumType } from 'graphql';

export const ASC = 'ASC';
export const DESC = 'DESC';

const SortEnumType = new GraphQLEnumType({
    name: 'Sort',
    values: {
        ASC: { value: ASC },
        DESC: { value: DESC },
    },
});

export default SortEnumType;
