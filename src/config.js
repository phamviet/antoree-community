/* eslint-disable prettier/prettier */
/* eslint-disable max-len */

if (process.env.BROWSER) {
    throw new Error(
        'Do not import `config.js` from inside the client-side code.',
    );
}

module.exports = {
    // Node.js app
    port: process.env.PORT || 3000,
    appEnv: process.env.APP_ENV || process.env.NODE_ENV || 'staging',
    siteUrl: process.env.SITE_URL || 'http://localhost:3000',
    imgix: {
        secureURLToken: 'Awu6gHyhchRsjYQv',
    },

    locales: [
        /* @intl-code-template '${lang}-${COUNTRY}', */
        'en-US',
        'vi-VN',
    ],
    defaultLocale: 'vi-VN',
    // API Gateway
    api: {
        // API URL to be used in the client-side code
        clientUrl: process.env.API_CLIENT_URL || '',
        // API URL to be used in the server-side code
        serverUrl:
            process.env.API_SERVER_URL ||
            `http://localhost:${process.env.PORT || 3000}`,
        antoreeUrl: 'https://api.antoree.com/v1',
    },

    // Database
    databaseUrl: process.env.DATABASE_URL || 'mysql://root:root@localhost/antoree-community-staging',

    // Database
    storage: {
        dest: process.env.STORAGE_DEST || './uploads',
        s3Bucket: process.env.AWS_S3_BUCKET || 'community.antoree.com',
        adapter: 's3'
    },

    // Web analytics
    analytics: {
        // https://analytics.google.com/
        googleTrackingId: process.env.GOOGLE_TRACKING_ID, // UA-XXXXX-X
        fbPixelId: process.env.FACEBOOK_PIXEL_ID,
    },


    // Authentication
    secretKey: process.env.JWT_SECRET || '20170723',
    auth: {
        antoree: {
            tokenURL: 'https://gate.antoree.com/oauth/token',
            profileURL: 'https://api.antoree.com/v1/community/account',
            id: '3',
            secret: 'Jb1u1z4mrsGVN1ET1ohgIb2dmsar1FWLD1pShMgo',
            scope: '*',
        },

        jwt: { secret: process.env.JWT_SECRET || '20170723' },

        // https://developers.facebook.com/apps/113930882616196
        facebook: {
            id: '113930882616196',
            secret: 'f041eb8bf70f8c968e252469b0070cd7',
        },

        // https://console.cloud.google.com/apis/credentials?project=antoree-1307
        google: {
            id: '509551645180-ge37oukirdkrki2gntdebmo2te56hcbb.apps.googleusercontent.com',
            secret:'s66evpaOFPDHTzaSZjygAeZ6',
        },

        // https://www.linkedin.com/developer/apps/5071985/auth
        linkedin: {
            id:'81pszj4mdnl6xy',
            secret:'FdE2nAjVhZJCOByo',
        },
    },

    caching: {
        frontend: {
            maxAge: 3600,
        }
    },
    bugsnag: {
        clientKey: process.env.BUGSNAG_CLIENT,
        serverKey: process.env.BUGSNAG_SERVER,
    }
};
