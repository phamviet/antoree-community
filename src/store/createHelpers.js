export default function createHelpers({ apolloClient, fetch, history }) {
    return {
        fetch,
        history,
        client: apolloClient,
    };
}
