/* eslint-disable no-param-reassign */
/**
 *
 */

import Router from 'universal-router';
import { USER_UPDATE } from 'reducers/user';
import routes from './routes';
import {
    AuthenticatedNavigation,
    ComposeNavigation,
} from './components/Navigation';

export default new Router(routes, {
    resolveRoute(context, params) {
        const { route, store } = context;
        const state = store.getState();
        const userContext = {};
        // Notice state.user is always an object
        if (state.user && state.user.id) {
            store.dispatch({
                type: USER_UPDATE,
                data: {
                    navigation:
                        route.name === 'compose'
                            ? ComposeNavigation
                            : AuthenticatedNavigation,
                },
            });
        }

        if (typeof route.load === 'function') {
            return route
                .load()
                .then(action =>
                    action.default({ ...context, userContext }, params),
                );
        }

        if (typeof context.route.action === 'function') {
            return context.route.action({ ...context, userContext }, params);
        }

        return null;
    },
});
