import { ENTITY_STORE } from 'reducers/entities';
import { COLLECTION_SET } from 'reducers/collections';
import topicCollectionQuery from './topicCollectionQuery.graphql';
import postsQuery from './postsQuery.graphql';
import { HOME_TOPICS_MENU } from '../constants/topic';

export default function createQuery({ store, client }) {
    const queries = {
        async fetchMenuItems() {
            try {
                const { data } = await client.query({
                    query: topicCollectionQuery,
                    variables: { name: HOME_TOPICS_MENU },
                });
                return data.collection.topics;
            } catch (error) {
                console.error(error);
            }
            return [];
        },
        async fetchPosts({ variables }) {
            try {
                const { data } = await client.query({
                    query: postsQuery,
                    variables,
                });
                return data.posts;
            } catch (error) {
                console.error(error);
            }
            return [];
        },
        // helpers
        collectionSet(key, data) {
            store.dispatch({
                type: ENTITY_STORE,
                data,
            });

            store.dispatch({
                type: COLLECTION_SET,
                key,
                data,
            });
        },
    };

    return queries;
}
