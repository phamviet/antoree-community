export const DETAIL = {
    width: 1024,
    height: 500,
};

export const FEED = {
    width: 670,
    height: 250,
};

export const CARD_HORIZONTAL = {
    width: 348,
    height: 100,
};

export const CARD_VERTICAL = {
    width: 200,
    height: 250,
};

export const PROFILE = {
    width: 160,
    height: 160,
};

export default {
    FEED,
    DETAIL,
    CARD_HORIZONTAL,
    CARD_VERTICAL,
    PROFILE,
};
