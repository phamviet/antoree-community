/* eslint-disable import/prefer-default-export */

export const POST = 'Post';
export const TOPIC = 'Topic';
export const USER = 'User';
export const COLLECTION = 'Collection';

export const ARRAY_ENTITY_TYPES = [POST, TOPIC, USER, COLLECTION];
