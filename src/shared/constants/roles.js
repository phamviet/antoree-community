export const USER = 1;
export const EDITOR = 1;
export const ADMINISTRATOR = 8;

export default {
    [USER]: 'USER',
    [EDITOR]: 'EDITOR',
    [ADMINISTRATOR]: 'ADMINISTRATOR',
};
