/* eslint-disable import/prefer-default-export */

export const STATUS_PUBLISHED = 'PUBLISHED';
export const STATUS_PENDING = 'PENDING';
export const STATUS_DRAFT = 'DRAFT';

export const TYPE_POST = 'post';
export const TYPE_GALLERY = 'gallery';
export const TYPE_VIDEO = 'video';
export const TYPE_AUDIO = 'audio';
export const TYPE_IMAGE = 'image';
export const TYPE_LINK = 'link';

export const FLAG_KID = 1;
export const FLAG_FEATURED = 2;
export const flags = {
    [FLAG_FEATURED]: 'FLAG_FEATURED',
};

export const PUBLIC = 'PUBLIC';
export const SHAREABLE = 'SHAREABLE';
export const PRIVATE = 'PRIVATE';

export const HOME_MASTHEAD_POSTS = 'HOME_MASTHEAD_POSTS';
export const HOME_FEATURED_POSTS = 'HOME_FEATURED_POSTS';
export const HOME_LATEST_POSTS = 'HOME_LATEST_POSTS';

export const TYPE_ENUM = [
    TYPE_POST,
    TYPE_GALLERY,
    TYPE_VIDEO,
    TYPE_AUDIO,
    TYPE_IMAGE,
    TYPE_LINK,
];

export const VISIBILITY_ENUM = [PUBLIC, SHAREABLE, PRIVATE];
