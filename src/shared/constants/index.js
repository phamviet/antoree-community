export const PUBLISHED = 'PUBLISHED';
export const PENDING = 'PENDING';
export const APPROVED = 'APPROVED';
export const DRAFT = 'DRAFT';

export const REACTION_LIKE = 'like';
export const REACTION_BOOKMARK = 'bookmark';
export const REACTION_MENTION = 'mention';

export const SITE_TITLE = 'Antoree Community';
