/**
 * Antoree Community
 *
 * Copyright © 2017-present Antoree
 */

import { db } from './server/models';
import config from './config';
// import redis from './server/redis';
import app from './server/app';

//
// Launch the server
// -----------------------------------------------------------------------------
let server;

if (!module.hot) {
    server = app.listen(config.port, () => {
        console.info(
            `The server is running at http://localhost:${config.port}/`,
        );
    });
}

// Shutdown Node.js app gracefully
function handleExit(options, err) {
    if (options.cleanup) {
        const actions = [db.close];
        if (server) {
            actions.push(server.close);
        }
        actions.forEach((close, i) => {
            try {
                close(() => {
                    if (i === actions.length - 1) process.exit();
                });
            } catch (e) {
                if (i === actions.length - 1) process.exit();
            }
        });
    }

    // eslint-disable-next-line no-console
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
}

//
// Hot Module Replacement
// -----------------------------------------------------------------------------
if (module.hot) {
    app.hot = module.hot;
    module.hot.accept('./router');
}

process.on('exit', handleExit.bind(null, { cleanup: true }));
process.on('SIGINT', handleExit.bind(null, { exit: true }));
process.on('SIGTERM', handleExit.bind(null, { exit: true }));
process.on('uncaughtException', handleExit.bind(null, { exit: true }));

export default app;
