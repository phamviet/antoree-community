/* eslint-disable import/prefer-default-export */

export const SET_RUNTIME_VARIABLE = 'SET_RUNTIME_VARIABLE';
export const UNSET_RUNTIME_VARIABLE = 'UNSET_RUNTIME_VARIABLE';
export const NAVBAR_ACTION = 'NAVBAR_ACTION';
export const NAVBAR_RIGHT = 'NAVBAR_RIGHT';

/* Post */
export const FETCH_POST = 'FETCH_POST';
export const COMPOSE_POST = 'COMPOSE_POST';
export const COMPOSE_COMMENT = 'COMPOSE_COMMENT';

export const MODAL_KEY = 'modals';
