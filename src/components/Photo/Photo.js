/* eslint-disable react/require-default-props,react/prop-types,jsx-a11y/alt-text */
import React from 'react';
import PropTypes from 'prop-types';
import Img from 'components/Img';

const Photo = ({ filename, remote, width = 64, height = 64, ...other }) => {
    if (remote) {
        const { style, className } = other;
        return (
            <img
                width={width}
                height={height}
                src={remote}
                style={style}
                className={className}
            />
        );
    }

    if (filename) {
        return <Img path={filename} {...{ width, height, ...other }} />;
    }

    return (
        <div
            style={{
                width,
                height,
                backgroundColor: 'rgba(158, 158, 158, 0.16)',
                ...other.style,
            }}
        />
    );
};

Photo.propTypes = {
    id: PropTypes.string,
    remote: PropTypes.string,
    filename: PropTypes.string,
};

export default Photo;
