import React from 'react';
import PropTypes from 'prop-types';
import Input from 'reactstrap/lib/Input';
import pick from 'lodash/pick';

const ENTER_KEY_CODE = 13;
const ESC_KEY_CODE = 27;

export default class SearchInput extends React.Component {
    static defaultProps = {
        commitOnBlur: false,
        initialValue: undefined,
    };
    static propTypes = {
        commitOnBlur: PropTypes.bool.isRequired,
        initialValue: PropTypes.string,
        onChange: PropTypes.func.isRequired,
    };

    state = {
        isEditing: false,
        text: this.props.initialValue || '',
    };

    emitChanges = () => {
        const newText = this.state.text.trim();
        this.props.onChange(newText);
    };
    handleBlur = () => {
        if (this.props.commitOnBlur) {
            this.emitChanges();
        }
    };
    handleChange = e => {
        this.setState({ text: e.target.value });
    };
    handleKeyDown = e => {
        if (this.props.onChange && e.keyCode === ESC_KEY_CODE) {
            this.setState({ text: '' });
            this.props.onChange('');
        } else if (e.keyCode === ENTER_KEY_CODE) {
            this.emitChanges();
        }
    };

    render() {
        return (
            <Input
                onBlur={this.handleBlur}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDown}
                value={this.state.text}
                {...pick(this.props, ['size', 'placeholder'])}
            />
        );
    }
}
