/* eslint-disable prettier/prettier */

import React from 'react';
import PropTypes from 'prop-types';
import serialize from 'serialize-javascript';
import bootstrap from 'scss/build/bootstrap.min.css';
import { SITE_TITLE } from 'shared/constants';
import config from '../config';

/* eslint-disable react/no-danger */

class Html extends React.Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        styles: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string.isRequired,
                cssText: PropTypes.string.isRequired,
            }).isRequired,
        ),
        scripts: PropTypes.arrayOf(PropTypes.string.isRequired),
        app: PropTypes.object.isRequired, // eslint-disable-line
        user: PropTypes.object, // eslint-disable-line
        meta: PropTypes.object, // eslint-disable-line
        children: PropTypes.string.isRequired,
    };

    static defaultProps = {
        styles: [],
        meta: {},
        scripts: [],
    };

    render() {
        const { title, description, meta, styles, scripts, app, children } = this.props;
        return (
            <html className="no-js" lang={app.lang}>
            <head>
                <meta charSet="utf-8"/>
                <meta httpEquiv="x-ua-compatible" content="ie=edge"/>
                <title>
                    { title ? `${title} - ${SITE_TITLE}` : SITE_TITLE}
                </title>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <meta name="description" content={description}/>
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} />
                {Object.keys(meta).map(key => (meta[key] && <meta key={key} property={key} content={meta[key]} />))}
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:title" content={title} />
                <meta name="twitter:description" content={description} />
                <link rel='dns-prefetch' href='//fonts.googleapis.com' />
                {scripts.map(script =>
                    <link key={script} rel="preload" href={script} as="script"/>,
                )}
                <link rel="apple-touch-icon" href="https://static.community.antoree.com/images/apple-touch-icon.png" />
                <link rel="stylesheet" href={bootstrap} />
                {styles.map(style =>
                    <style
                        key={style.id}
                        id={style.id}
                        dangerouslySetInnerHTML={{ __html: style.cssText }}
                    />,
                )}
            </head>
            <body>
            <div id="app" dangerouslySetInnerHTML={{ __html: children }}/>
            <script
                dangerouslySetInnerHTML={{ __html: `window.App=${serialize(app)}` }}
            />
            <link href="https://fonts.googleapis.com/css?family=Lora:400,700|Open+Sans:400,600,700&amp;subset=vietnamese" rel="stylesheet" />
            <link href="https://diegoddox.github.io/react-redux-toastr/7.1/react-redux-toastr.min.css" rel="stylesheet" type="text/css" />
            {scripts.map(script => <script key={script} src={script}/>)}
            {config.bugsnag.clientKey &&
            <script
                src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-3.min.js"
                data-apikey={config.bugsnag.clientKey}
            />}
            </body>
            </html>
        );
    }
}

export default Html;
