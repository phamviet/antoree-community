import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Row, Col, Button } from 'reactstrap';
import messages from 'components/messages';

const LoadMore = ({ visible, ...other }) => {
    if (!visible) {
        return null;
    }

    return (
        <Row>
            <Col sm={12} md={{ size: 6, offset: 3 }}>
                <Button role="button" block {...other}>
                    <FormattedMessage {...messages.seeMore} />
                </Button>
            </Col>
        </Row>
    );
};

LoadMore.propTypes = {
    visible: PropTypes.bool,
};

LoadMore.defaultProps = {
    visible: true,
};

export default LoadMore;
