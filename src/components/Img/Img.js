import React from 'react';
import PropTypes from 'prop-types';
import Imgix from 'react-imgix';
import startsWith from 'lodash/startsWith';
import { DETAIL } from 'shared/constants/imageSizes';
import imgUrl from 'utils/imgUrl';

const Img = ({ path, ...other }) =>
    <Imgix
        aggressiveLoad
        src={startsWith(path, 'http') ? path : imgUrl(path)}
        {...other}
    />;

Img.propTypes = {
    path: PropTypes.string.isRequired,
};

Img.defaultProps = {
    size: DETAIL,
};

export default Img;
