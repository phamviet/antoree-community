import React from 'react';
import PropTypes from 'prop-types';
import { FormattedRelative } from 'react-intl';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Author, { AuthorComponent } from '../Author';
import theme from '../Post/Theme.css';
import s from './PostDetail.css';

const PostDetail = ({
    displayName,
    author,
    html,
    publishedAt,
    createdAt,
    oldId,
    className,
    // eslint-disable-next-line react/prop-types
    children,
}) =>
    <div className={className}>
        <div className={s.postHeader}>
            <h1 className="font-weight-bold">
                {displayName}
            </h1>
            <div className="my-5">
                <Author
                    size={50}
                    {...author}
                    subtitle={
                        <FormattedRelative value={publishedAt || createdAt} />
                    }
                />
            </div>
        </div>

        <div className={theme.root}>
            <div
                className={oldId ? s.customContent : ''} // eslint-disable-next-line react/no-danger
                dangerouslySetInnerHTML={{ __html: html }}
            />
            {children}
        </div>
    </div>;

PostDetail.propTypes = {
    displayName: PropTypes.string.isRequired,
    publishedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    html: PropTypes.string,
    author: PropTypes.shape(AuthorComponent.propTypes).isRequired,
    oldId: PropTypes.string,
    className: PropTypes.string,
};

PostDetail.defaultProps = {
    html: '',
    createdAt: undefined,
    oldId: undefined,
    className: undefined,
};

export default withStyles(s, theme)(PostDetail);
