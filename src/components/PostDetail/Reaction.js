import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { compose } from 'redux';
import { Nav, NavItem } from 'reactstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { ShareButtons, ShareCounts, generateShareIcon } from 'react-share';
import {
    connectReaction,
    LikeButton,
    BookmarkButton,
    TextButton,
} from 'components/Reactions';
import { FormattedMessage } from 'react-intl';
import messages from 'components/messages';
import s from './Reaction.css';

const { FacebookShareCount } = ShareCounts;
const { FacebookShareButton } = ShareButtons;
const FacebookIcon = generateShareIcon('facebook');

export const ReactionComponent = ({
    likeCount,
    isLiked,
    isBookmarked,
    disabled,
    onLikeClick,
    onBookmarkClick,
    className,
    ...other
}) =>
    <div className={classNames(className, s.root)}>
        <Nav>
            <LikeButton
                onClick={onLikeClick}
                active={isLiked}
                disabled={disabled || !onLikeClick}
            />
            <TextButton>
                {likeCount.toString()}
            </TextButton>
        </Nav>
        <Nav className="align-items-center">
            <NavItem>
                <div className="d-inline-block text-center text-slate px-3 mr-3">
                    <FormattedMessage {...messages.share} />
                </div>
            </NavItem>
            {other.url &&
                <NavItem>
                    <FacebookShareButton
                        url={other.url}
                        quote={other.description}
                    >
                        <FacebookIcon size={26} round />
                    </FacebookShareButton>
                </NavItem>}
            {other.url &&
                <NavItem>
                    <div className="d-inline-block text-center text-slate px-1">
                        <FacebookShareCount url={other.url} />
                    </div>
                </NavItem>}
            <BookmarkButton
                onClick={onBookmarkClick}
                active={isBookmarked}
                disabled={disabled || !onBookmarkClick}
            />
        </Nav>
    </div>;

ReactionComponent.propTypes = {
    likeCount: PropTypes.number,
    isLiked: PropTypes.bool,
    isBookmarked: PropTypes.bool,
    disabled: PropTypes.bool,
    onLikeClick: PropTypes.func,
    onBookmarkClick: PropTypes.func,
    className: PropTypes.string,
};

ReactionComponent.defaultProps = {
    likeCount: 0,
    isLiked: false,
    isBookmarked: false,
    disabled: false,
    onLikeClick: undefined,
    onBookmarkClick: undefined,
    className: undefined,
};

export default compose(withStyles(s), connectReaction)(ReactionComponent);
