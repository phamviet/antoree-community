import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Nav, NavItem } from 'reactstrap';
import Chip from '../Chip';
import Link from '../Link';

const Tags = ({ tags, className }) =>
    <Nav className={classnames('flex-wrap', className)}>
        {(tags || []).map(({ id, displayName, href }) =>
            <NavItem key={id}>
                <Chip>
                    <Link to={href} title={displayName}>
                        {displayName}
                    </Link>
                </Chip>
            </NavItem>,
        )}
    </Nav>;

Tags.propTypes = {
    tags: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            displayName: PropTypes.string.isRequired,
            href: PropTypes.string.isRequired,
        }),
    ),
    className: PropTypes.string,
};

Tags.defaultProps = {
    tags: [],
    className: undefined,
};

export default Tags;
