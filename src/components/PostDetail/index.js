import PostDetail from './PostDetail';
import Reaction from './Reaction';
import Tags from './Tags';

export { Reaction, Tags };
export default PostDetail;
