/* eslint-disable react/prop-types */
import React from 'react';

export default ({ href, children }) =>
    <a href={href} target="_blank">
        {children}
    </a>;
