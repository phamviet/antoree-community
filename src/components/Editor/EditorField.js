import React from 'react';
import PropTypes from 'prop-types';

import { convertToRaw, convertFromRaw, EditorState } from 'draft-js';
import { fromHTML } from 'utils/draftConvert';
import PostEditor from '../Post/Editor';

class EditorField extends React.Component {
    static propTypes = {
        // value: PropTypes.string.isRequired,
        // fixme why fucking FileList is given on file selection?
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
            .isRequired,
        onChange: PropTypes.func.isRequired,
    };

    state = {
        editor: false,
        editorState: EditorState.createEmpty(),
    };

    componentDidMount() {
        this.toEditorContent(this.props.value);
    }

    componentWillReceiveProps(nextProps) {
        // on form reset
        if (nextProps.value === '') {
            this.setState({
                editorState: EditorState.createEmpty(),
            });
        }
    }

    shouldComponentUpdate(nextProps) {
        return typeof nextProps.value === 'string';
    }

    onEditorStateChange = editorState => {
        this.setState({
            editorState,
        });

        const content = editorState.getCurrentContent();
        const raw = convertToRaw(content);

        this.props.onChange(JSON.stringify(raw));
    };

    toEditorContent(value) {
        let editorState = this.state.editorState;

        try {
            const raw = JSON.parse(value);
            editorState = raw
                ? EditorState.createWithContent(convertFromRaw(raw))
                : EditorState.createEmpty();
        } catch (error) {
            editorState = fromHTML(value);
        }

        this.setState({
            editor: true,
            editorState,
        });
    }

    render() {
        const { editor, editorState } = this.state;
        const { value, onChange, ...props } = this.props;

        if (!editor) {
            return <div style={{ minHeight: '600px' }} />;
        }

        return (
            <PostEditor
                editorState={editorState}
                onChange={this.onEditorStateChange}
                {...props}
            />
        );
    }
}

export default EditorField;
