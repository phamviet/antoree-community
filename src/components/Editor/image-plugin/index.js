import decorateComponentWithProps from 'decorate-component-with-props';
import addImage from './modifiers/addImage';
import ImageComponent from './Image';

const defaultTheme = {
    image: '',
};

export default (config = {}) => {
    const theme = config.theme ? config.theme : defaultTheme;
    let Image = config.imageComponent || ImageComponent;
    if (config.decorator) {
        Image = config.decorator(Image);
    }
    const ThemedImage = decorateComponentWithProps(Image, { theme });

    return {
        blockRendererFn: (
            block,
            { getEditorState, setEditorState, getReadOnly, setReadOnly },
        ) => {
            if (block.getType() === 'atomic') {
                const contentState = getEditorState().getCurrentContent();
                const key = block.getEntityAt(0);
                if (!key) {
                    return null;
                }

                const entity = contentState.getEntity(key);
                const type = entity.getType();
                if (type.toUpperCase() === 'IMAGE') {
                    return {
                        component: ThemedImage,
                        editable: false,
                        props: {
                            getEditorState,
                            setEditorState,
                            setReadOnly,
                            getReadOnly,
                            entity,
                        },
                    };
                }
            }

            return null;
        },
        addImage,
    };
};

export const Image = ImageComponent;
