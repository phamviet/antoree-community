/* eslint-disable react/prop-types, jsx-a11y/no-redundant-roles, jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react';
import classNames from 'classnames';
import { EditorState } from 'draft-js';

export default class Image extends Component {
    state = {
        editMode: false,
    };
    toggleEditMode = () => {
        this.setState({ editMode: !this.state.editMode });
    };
    handleFocus = () => {
        this.props.blockProps.setReadOnly(true);
    };
    handleBlur = () => {
        this.props.blockProps.setReadOnly(false);
    };
    handleChange = e => {
        const { setEditorState } = this.props.blockProps;
        const entityKey = this.props.block.getEntityAt(0);
        const newContentState = this.props.contentState.mergeEntityData(
            entityKey,
            { caption: e.target.value },
        );

        setEditorState(EditorState.createWithContent(newContentState));
    };
    render() {
        const { className, ...otherProps } = this.props;
        const { blockProps: { entity, getReadOnly }, theme } = otherProps;

        const { src, caption } = entity.getData();
        const readOnly = getReadOnly();

        return (
            <div
                className={classNames(
                    'figure',
                    'd-block',
                    'text-center',
                    className,
                    theme.image,
                )}
            >
                {src &&
                    <img
                        className="figure-img img-fluid"
                        role="presentation"
                        alt=""
                        src={src}
                    />}
                {readOnly
                    ? <figcaption className="figure-caption text-center">
                          {caption}
                      </figcaption>
                    : <textarea
                          placeholder="Enter caption"
                          value={caption}
                          cols={1}
                          onChange={this.handleChange}
                          onFocus={this.handleFocus}
                          onBlur={this.handleBlur}
                          className="form-control figure-caption text-center block-input"
                      />}
            </div>
        );
    }
}
