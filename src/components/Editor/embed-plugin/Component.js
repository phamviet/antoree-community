/* eslint-disable react/prop-types,jsx-a11y/iframe-has-title */
import React from 'react';
import { Entity } from 'draft-js';

export default ({ block }) => {
    const { src } = Entity.get(block.getEntityAt(0)).getData();

    return (
        <div className="embed-responsive embed-responsive-16by9">
            <iframe src={src} className="embed-responsive-item" />
        </div>
    );
};
