import React from 'react';
import PropTypes from 'prop-types';
import merge from 'lodash/merge';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import { FormattedMessage } from 'react-intl';
import messages from 'components/messages';
import { toggleSubscription } from 'actions/user';

const defaultProps = { block: true, outline: true, color: 'primary' };

export const FollowButtonComponent = ({ isSubscribed, ...other }) =>
    <Button role="button" {...merge(defaultProps, other)}>
        {isSubscribed
            ? <FormattedMessage {...messages.unfollow} />
            : <FormattedMessage {...messages.follow} />}
    </Button>;

FollowButtonComponent.propTypes = {
    // eslint-disable-next-line react/no-unused-prop-types
    id: PropTypes.string.isRequired,
    isSubscribed: PropTypes.bool,
};

FollowButtonComponent.defaultProps = {
    isSubscribed: false,
};

export default connect(
    ({ entities }, { id, isSubscribed }) => ({
        isSubscribed: entities[id] ? entities[id].isSubscribed : isSubscribed,
    }),
    (dispatch, { id }) => ({
        onClick: () => dispatch(toggleSubscription(id)),
    }),
)(FollowButtonComponent);
