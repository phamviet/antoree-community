import React from 'react';
import PropTypes from 'prop-types';
import pick from 'lodash/pick';
import { Button } from 'reactstrap';
import { FormattedMessage } from 'react-intl';
import messages from 'components/messages';

export const StudyRequestButtonComponent = ({
    isTeacher,
    teachingProfileUrl,
    ...other
}) =>
    isTeacher
        ? <Button
              tag="a"
              target="blank"
              rel="nofollow"
              href={teachingProfileUrl}
              {...pick(other, ['block', 'outline', 'color', 'onClick', 'tag'])}
          >
              <FormattedMessage {...messages.studyWithThisTeacher} />
          </Button>
        : null;

StudyRequestButtonComponent.propTypes = {
    isTeacher: PropTypes.bool,
    teachingProfileUrl: PropTypes.string,
};

StudyRequestButtonComponent.defaultProps = {
    teachingProfileUrl: '',
    isTeacher: false,
};

export default StudyRequestButtonComponent;
