import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Link from 'components/Link';
import { Media, Badge } from 'reactstrap';
import FollowButton from './FollowButton';
import StudyRequestButton from './StudyRequestButton';
import s from './Author.css';
import Avatar, { AvatarComponent } from '../Avatar';

export const AuthorComponent = ({
    displayName,
    href,
    isTeacher,
    photo,
    size,
    subtitle,
    bold,
}) =>
    <Media className="align-items-center">
        <Media left className="mr-2 mr-md-3">
            <Avatar {...photo} size={size} className="media-object" />
        </Media>
        <Media body>
            <Media className={classNames(s.title, { [s.textNormal]: !bold })}>
                <Link title={displayName} to={href}>
                    {displayName}
                </Link>
                {isTeacher &&
                    <Badge
                        className={classNames('ml-2', s.badge)}
                        color="primary"
                    >
                        Teacher
                    </Badge>}
            </Media>
            <small className="text-slate">
                {subtitle}
            </small>
        </Media>
    </Media>;

AuthorComponent.propTypes = {
    href: PropTypes.string.isRequired,
    photo: PropTypes.shape(AvatarComponent.propTypes),
    displayName: PropTypes.string.isRequired,
    isTeacher: PropTypes.bool,
    subtitle: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    size: PropTypes.number,
    bold: PropTypes.bool,
};

AuthorComponent.defaultProps = {
    photo: undefined,
    isTeacher: false,
    bold: false,
    size: 50,
    subtitle: null,
};

export { FollowButton, StudyRequestButton };
export default withStyles(s)(AuthorComponent);
