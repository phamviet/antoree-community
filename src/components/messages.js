import { defineMessages } from 'react-intl';

export default defineMessages({
    home: {
        id: 'home',
        defaultMessage: 'Home',
    },
    follow: {
        id: 'follow',
        defaultMessage: 'Follow',
    },
    unfollow: {
        id: 'unfollow',
        defaultMessage: 'Unfollow',
    },
    studyWithThisTeacher: {
        id: 'studyWithThisTeacher',
        description: 'Study with this teacher label',
        defaultMessage: 'Study with this teacher',
    },
    share: {
        id: 'share',
        defaultMessage: 'Share',
    },
    seeMore: {
        id: 'seeMore',
        defaultMessage: 'See more',
    },
    relatedPosts: {
        id: 'relatedPosts',
        description: 'relatedPosts label',
        defaultMessage: 'Related Reads',
    },
    register: {
        id: 'register',
        defaultMessage: 'Register',
    },
    close: {
        id: 'close',
        defaultMessage: 'Close',
    },
    fullName: {
        id: 'fullName',
        description: 'Form name field',
        defaultMessage: 'Name',
    },
    phone: {
        id: 'phone',
        description: 'Form phone field',
        defaultMessage: 'Phone',
    },
    requiredMessage: {
        id: 'validate.required',
        defaultMessage: 'Please enter your :attribute',
    },
    invalidEmailMessage: {
        id: 'validate.email',
        defaultMessage: 'Invalid email',
    },
    subscribeMessage: {
        id: 'subscribeMessage',
        defaultMessage: 'I agree to receive emails from Antoree.com',
    },
    antoreeSignature: {
        id: 'antoreeSignature',
        defaultMessage:
            '{antoreeEnglish} được thành lập tại Singapore bởi {antoreeInternational} với mô hình học trực tuyến 1 kèm 1 có sứ mệnh kết nối người học và người dạy tiếng anh trên toàn thế giới.',
    },
});
