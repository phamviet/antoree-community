/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import truncate from 'lodash/truncate';
import { FormattedRelative } from 'react-intl';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Container, Media, Button } from 'reactstrap';
import Author, { AuthorComponent } from '../Author';
import s from './HomeSlideItem.css';
import Link from '../Link';

export const HomeSlideItemComponent = ({
    displayName,
    href,
    author,
    photo,
    publishedAt,
    className,
}) =>
    <div className={className}>
        <Container className={s.coverContainer}>
            <div
                className={s.cover}
                style={{
                    backgroundImage: `${photo
                        ? `url(${photo.url}&w=1110&h=355&fit=crop&crop=top)`
                        : 'none'}`,
                }}
            />
        </Container>
        <Container>
            <h2 className={classNames('h3', s.title)}>
                <Link title={displayName} to={href}>
                    {displayName}
                </Link>
            </h2>
            <div>
                <Author
                    subtitle={<FormattedRelative value={publishedAt} />}
                    {...author}
                    size={40}
                />
            </div>
        </Container>
    </div>;

HomeSlideItemComponent.propTypes = {
    displayName: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    photo: PropTypes.shape({
        url: PropTypes.string.isRequired,
    }),
    publishedAt: PropTypes.string.isRequired,
    author: PropTypes.shape(AuthorComponent.propTypes).isRequired,
    className: PropTypes.string,
};

HomeSlideItemComponent.defaultProps = {
    photo: undefined,
    className: undefined,
};

// eslint-disable-next-line react/prop-types
export const IndicatorComponent = ({ index, displayName, onClick }) =>
    <div className={s.indicator} onClick={onClick}>
        <Media className="align-items-center">
            <Media
                tag={Button}
                outline
                role="button"
                left
                className={s.indicatorNumber}
            >
                {`0${index + 1}`}
            </Media>
            <Media body>
                <Media heading className={s.indicatorTitle}>
                    {truncate(displayName, { length: 55, separator: ' ' })}
                </Media>
            </Media>
        </Media>
    </div>;

export const Indicator = withStyles(s)(IndicatorComponent);
export default withStyles(s)(HomeSlideItemComponent);
