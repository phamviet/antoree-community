import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import slickStyles from 'slick-carousel/slick/slick.css';
import Slider from 'react-slick';
import s from './HomeSlider.css';
import HomeSlideItem, {
    Indicator,
    HomeSlideItemComponent,
} from './HomeSlideItem';

// eslint-disable-next-line react/prop-types
const Slick = ({ getItem, children, ...other }) => {
    const settings = {
        customPaging(i) {
            const item = getItem(i);
            return <Indicator index={i} {...item} />;
        },
        dots: true,
        dotsClass: classNames('nav', s.indicators),
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: false,
        arrows: false,
        autoplay: true,
        pauseOnHover: false,
        autoplaySpeed: 5000,
        lazyLoad: true,
    };

    return (
        <Slider {...settings} {...other}>
            {children}
        </Slider>
    );
};

class HomeSlider extends Component {
    static propTypes = {
        posts: PropTypes.arrayOf(
            PropTypes.shape(HomeSlideItemComponent.propTypes),
        ).isRequired,
    };
    render() {
        const { posts } = this.props;
        return (
            <div>
                {posts.length > 0
                    ? <Slick getItem={i => posts[i]}>
                          {(posts || []).map(post =>
                              <div key={post.id}>
                                  <HomeSlideItem {...post} />
                              </div>,
                          )}
                      </Slick>
                    : null}
            </div>
        );
    }
}

export default withStyles(s, slickStyles)(HomeSlider);
