import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { Button } from 'reactstrap';
import UploadIcon from 'react-icons/lib/fa/upload';

class Uploader extends Component {
    static propTypes = {
        onUploadCallback: PropTypes.func.isRequired,
        onChange: PropTypes.func.isRequired,
    };

    onClick = e => {
        e.preventDefault();
        // eslint-disable-next-line react/no-find-dom-node
        ReactDOM.findDOMNode(this.fileInput).click();
    };

    inputChange = e => {
        const file = e.target.files[0];
        this.props.onUploadCallback({ file }, (error, f) => {
            if (!error) {
                this.props.onChange(f);
            } else {
                console.error(error);
            }
            // this.fileInput.value = '';
        });
    };

    render() {
        return (
            <div>
                <Button type="button" size="sm" onClick={this.onClick}>
                    Upload <UploadIcon />
                </Button>
                <div className="hidden">
                    <input
                        type="file"
                        accept="image/*"
                        ref={fileInput => {
                            this.fileInput = fileInput;
                        }}
                        onChange={this.inputChange}
                        style={{ display: 'none' }}
                    />
                </div>
            </div>
        );
    }
}

export default Uploader;
