import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TimelineCard, { TimelineCardComponent } from './TimelineCard';

class Timeline extends Component {
    static propTypes = {
        posts: PropTypes.arrayOf(
            PropTypes.shape(TimelineCardComponent.propTypes),
        ),
    };

    static defaultProps = {
        posts: [],
    };

    render() {
        return (
            <div>
                {(this.props.posts || [])
                    .map(item => <TimelineCard key={item.id} {...item} />)}
            </div>
        );
    }
}

export default connect(({ entities, collections }, { profile }) => ({
    posts: (collections[`timeline-${profile.id}`] || [])
        .map(id => entities[id]),
}))(Timeline);
