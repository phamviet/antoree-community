import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { compose } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Col, Row, Badge, Button } from 'reactstrap';
import Photo from 'components/Photo';
import LocationIcon from 'components/Icons/LocationIcon';
import GenderIcon from 'components/Icons/GenderIcon';
import VideoIcon from 'components/Icons/VideoIcon';
import ListenIcon from 'components/Icons/ListenIcon';
import { FollowButton, StudyRequestButton } from 'components/Author';
import { PROFILE } from 'shared/constants/imageSizes';
import s from './ProfileHeader.css';

const ProfileHeader = ({
    id,
    displayName,
    isSubscribed,
    photo,
    address,
    age,
    isMyProfile,
    ...profile
}) =>
    <Row>
        <Col md={2}>
            <div className={s.profileImage}>
                <Photo {...photo} {...PROFILE} />
                {false &&
                    <div>
                        <Button className={`btn ${s.btnListen}`}>
                            <ListenIcon />
                        </Button>
                        <Button className={`btn ${s.btnVideo}`}>
                            <VideoIcon />
                        </Button>
                    </div>}
            </div>
        </Col>
        <Col md={10}>
            <Row>
                <Col>
                    <h4>
                        {displayName}
                        {profile.isTeacher &&
                            <Badge
                                className={classNames('ml-2', s.status)}
                                color="primary"
                            >
                                Available
                            </Badge>}
                    </h4>
                    {address
                        ? <p className={s.location}>
                              <LocationIcon />
                              <span>
                                  {address}
                              </span>
                          </p>
                        : null}
                    {age
                        ? <p className={s.age}>
                              <GenderIcon male />
                              <span>
                                  {age}
                              </span>
                          </p>
                        : null}
                </Col>
                <Col xs={12} md={3}>
                    {!isMyProfile &&
                        <FollowButton
                            block
                            id={id}
                            isSubscribed={isSubscribed}
                        />}
                    <StudyRequestButton {...profile} block color="primary" />
                </Col>
            </Row>
        </Col>
    </Row>;

ProfileHeader.propTypes = {
    id: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    address: PropTypes.string,
    age: PropTypes.number,
    isSubscribed: PropTypes.bool,
    isMyProfile: PropTypes.bool,
    ...Photo.propTypes,
};

ProfileHeader.defaultProps = {
    address: null,
    age: null,
    isSubscribed: false,
    isMyProfile: false,
};

export default compose(
    connect(({ user }, { id }) => ({ isMyProfile: user && user.id === id })),
    withStyles(s),
)(ProfileHeader);
