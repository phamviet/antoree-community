/* eslint-disable react/sort-comp,react/no-unused-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
    TabContent,
    Container,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
} from 'reactstrap';
import classNames from 'classnames';
import TimelineCard, { TimelineCardComponent } from './TimelineCard';
import s from './ProfileTabs.css';

const defaultTabs = [
    {
        name: 'timeline',
        label: 'Timeline',
    },
];

// eslint-disable-next-line no-unused-vars
const teacherTabs = [
    {
        name: 'teaching',
        label: 'Teaching Profile',
    },
    {
        name: 'review',
        label: 'Reviews',
    },
];

const authenticatedTabs = [
    {
        name: 'bookmark',
        label: 'Bookmarks',
    },
];

class ProfileTabs extends Component {
    state = {
        isMyProfile: false,
        activeTab: 'timeline',
        tabs: [],
    };

    static propTypes = {
        profile: PropTypes.shape({
            id: PropTypes.string.isRequired,
        }).isRequired,
        user: PropTypes.shape({
            id: PropTypes.string.isRequired,
        }),
        timelinePosts: PropTypes.arrayOf(
            PropTypes.shape(TimelineCardComponent.propTypes),
        ),
    };

    static defaultProps = {
        timelinePosts: [],
        user: null,
    };

    componentWillReceiveProps(props) {
        this.refreshState(props);
    }

    refreshState(props) {
        const { user, profile } = props;
        const isMyProfile = user && user.id === profile.id;
        let tabs = defaultTabs;
        if (isMyProfile) {
            tabs = tabs.concat(authenticatedTabs);
        }

        this.setState({
            isMyProfile,
            tabs,
        });
    }

    toggle = tab => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab,
            });
        }
    };

    render() {
        const { timelinePosts } = this.props;
        return (
            <div>
                <Container>
                    <Nav tabs className={s.profileTabs}>
                        {this.state.tabs.map(({ name, label }) =>
                            <NavItem key={name}>
                                <NavLink
                                    className={classNames({
                                        active: this.state.activeTab === name,
                                    })}
                                    onClick={() => {
                                        this.toggle(name);
                                    }}
                                >
                                    {label}
                                </NavLink>
                            </NavItem>,
                        )}
                    </Nav>
                </Container>
                <TabContent
                    activeTab={this.state.activeTab}
                    className={s.profileTabContent}
                >
                    <TabPane tabId="timeline">
                        <Container>
                            <Row className="justify-content-md-center">
                                {timelinePosts.map(item =>
                                    <TimelineCard key={item.id} {...item} />,
                                )}
                            </Row>
                        </Container>
                    </TabPane>
                    <TabPane tabId="bookmark" />
                </TabContent>
            </div>
        );
    }
}

export default compose(
    connect(({ user, entities, collections }, { profile }) => ({
        user,
        timelinePosts: (collections[`timeline-${profile.id}`] || [])
            .map(id => entities[id]),
    })),
    withStyles(s),
)(ProfileTabs);
