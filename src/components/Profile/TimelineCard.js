/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
    Nav,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from 'reactstrap';
import { FormattedRelative } from 'react-intl';
import Link from 'components/Link';
import {
    connectReaction,
    LikeButton,
    BookmarkButton,
    TextButton,
} from 'components/Reactions';
import FaAngleDown from 'react-icons/lib/fa/angle-down';
import Author, { AuthorComponent } from 'components/Author';
import { CardThumbComponent } from 'components/Post/CardThumb';
import s from './TimelineCard.css';

const CardFooter = ({
    likeCount,
    isLiked,
    isBookmarked,
    disabled,
    onLikeClick,
    onBookmarkClick,
}) =>
    <Nav className="align-items-center justify-content-between">
        <Nav>
            <LikeButton
                onClick={onLikeClick}
                active={isLiked}
                disabled={disabled || !onLikeClick}
            />
            <TextButton>
                {likeCount.toString()}
            </TextButton>
        </Nav>
        <BookmarkButton
            onClick={onBookmarkClick}
            active={isBookmarked}
            disabled={disabled || !onBookmarkClick}
        />
    </Nav>;

const CardImage = ({ displayName, href, src }) =>
    <Link
        to={href}
        role="button"
        title={displayName}
        className="d-block w-full h-250 backgroundCover"
        style={{
            backgroundImage: src ? `url("${src}")` : 'none',
        }}
    />;

const ConnectedCardFooter = connectReaction(CardFooter);

export class TimelineCardComponent extends Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        href: PropTypes.string.isRequired,
        displayName: PropTypes.string.isRequired,
        publishedAt: PropTypes.string.isRequired,
        description: PropTypes.string,
        author: PropTypes.shape(AuthorComponent.propTypes).isRequired,
        photo: PropTypes.shape(CardThumbComponent.propTypes),
    };

    static defaultProps = {
        description: '',
        photo: [],
    };

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false,
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen,
        });
    }

    render() {
        const { displayName, href, photo, author, ...props } = this.props;
        return (
            <div className={s.timelineItem}>
                <div className={`border-bottom-0 ${s.author}`}>
                    <div className={s.person}>
                        <Author
                            {...author}
                            displayName={author.displayName}
                            subtitle={
                                <FormattedRelative
                                    value={props.publishedAt || props.createdAt}
                                />
                            }
                            photo={author.photo}
                        />
                    </div>
                    {false &&
                        <div className="float-right dropdown timeline-dropdown">
                            <Dropdown
                                isOpen={this.state.dropdownOpen}
                                toggle={this.toggle}
                            >
                                <DropdownToggle nav>
                                    <FaAngleDown />
                                </DropdownToggle>
                                <DropdownMenu className={s.menuDropDown}>
                                    <DropdownItem>Edit article</DropdownItem>
                                    <DropdownItem>Delete article</DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                        </div>}
                </div>
                <div className="h-auto">
                    <CardImage
                        displayName={displayName}
                        href={href}
                        src={photo.feedUrl}
                    />
                </div>
                <div className={`border-top-0 ${s.content}`}>
                    <h4>
                        <Link to={href}>
                            {displayName}
                        </Link>
                    </h4>
                    <p>
                        {props.description}
                    </p>

                    <ConnectedCardFooter {...props} />
                </div>
            </div>
        );
    }
}

export default withStyles(s)(TimelineCardComponent);
