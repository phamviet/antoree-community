/* eslint-disable react/require-default-props */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DeleteIcon from 'react-icons/lib/md/cancel';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Chip.css';

const getStyles = () => ({
    deleteIcon: {
        display: 'inline-block',
        color: 'rgba(0, 0, 0, 0.26)',
        fill: 'rgba(0, 0, 0, 0.26)',
        height: '16px',
        width: '16px',
        cursor: 'pointer',
    },
});

class Chip extends Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
        onRequestDelete: PropTypes.func,
    };

    handleClickDeleteIcon = event => {
        // Stop the event from bubbling up to the `Chip`
        event.stopPropagation();
        this.props.onRequestDelete(event);
    };

    render() {
        const { children, onRequestDelete, ...other } = this.props;
        const deletable = !!onRequestDelete;
        const styles = getStyles();

        const deleteIcon = deletable
            ? <DeleteIcon
                  style={styles.deleteIcon}
                  onClick={this.handleClickDeleteIcon}
              />
            : null;

        return (
            <div className={s.root} {...other}>
                <div className={s.label}>
                    {' '}{children}{' '}
                </div>
                <div className="ml-1">
                    {deleteIcon}
                </div>
            </div>
        );
    }
}

export default withStyles(s)(Chip);
