/* eslint-disable react/require-default-props */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Container, Row, Col, Navbar, Nav } from 'reactstrap';
import { NAVBAR_ACTION, NAVBAR_RIGHT } from '../../constants';
import { AnonymousNavigation } from '../Navigation';
import Link from '../Link';
import logo from './logo.svg';
import s from './Header.css';

class Header extends Component {
    static propTypes = {
        navigation: PropTypes.func,
        fixed: PropTypes.string,
        runtime: PropTypes.shape({
            [NAVBAR_ACTION]: PropTypes.element,
            [NAVBAR_RIGHT]: PropTypes.element,
        }),
    };

    static defaultProps = {
        navigation: AnonymousNavigation,
        fixed: undefined,
    };

    state = {
        isOpen: false,
    };

    render() {
        const { fixed, navigation } = this.props;
        const runtime = {
            [NAVBAR_ACTION]: null,
            ...this.props.runtime,
        };
        return (
            <header>
                <Navbar fixed={fixed} light className={s.navbar}>
                    <Container className={s.container}>
                        <Row className="align-items-center">
                            <Col tag={Link} className="navbar-brand" to="/">
                                <img
                                    alt="Antoree Community"
                                    src={logo}
                                    className={s.logo}
                                />
                            </Col>
                            <Col
                                tag={Nav}
                                navbar
                                className="ml-auto flex-row align-items-center justify-content-end"
                            >
                                {runtime[NAVBAR_ACTION]
                                    ? runtime[NAVBAR_ACTION]
                                    : null}
                                {!!navigation &&
                                    React.createElement(navigation)}
                            </Col>
                        </Row>
                    </Container>
                </Navbar>
            </header>
        );
    }
}

export default compose(
    connect(({ runtime, user }) => ({
        runtime,
        navigation: user ? user.navigation : undefined,
    })),
    withStyles(s),
)(Header);
