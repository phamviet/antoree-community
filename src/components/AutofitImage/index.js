import React from 'react';
import PropTypes from 'prop-types';

const AutofitImage = ({
    src,
    bgSize,
    width,
    height,
    positionX,
    positionY,
    // eslint-disable-next-line react/prop-types
    children,
    ...other
}) => {
    const style = {
        width,
        height,
        backgroundImage: `url('${src}')`,
        margin: 'auto',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: `${positionX} ${positionY}`,
        backgroundSize: bgSize,
    };
    return (
        <div style={style} {...other}>
            {children}
        </div>
    );
};

AutofitImage.propTypes = {
    src: PropTypes.string.isRequired,
    bgSize: PropTypes.oneOf(['cover', 'contain', 'auto']),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    positionX: PropTypes.oneOf(['left', 'center', 'right']),
    positionY: PropTypes.oneOf(['left', 'center', 'right']),
};

AutofitImage.defaultProps = {
    bgSize: 'auto',
    width: '100%',
    height: '100%',
    positionX: 'center',
    positionY: 'center',
};

export default AutofitImage;
