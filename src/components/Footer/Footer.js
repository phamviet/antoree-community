import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import classNames from 'classnames';
import ScrollToTop from 'react-scroll-up';
import s from './Footer.css';
import FooterMenu from './FooterMenu';
import FooterBottom from './FooterBottom';
import gototop from './gototop.png';

class Footer extends React.Component {
    render() {
        return (
            <footer className={classNames('nav-inverse', s.root)}>
                <ScrollToTop style={{ zIndex: 1 }} showUnder={160}>
                    <img width="60" height="60" src={gototop} alt="Go to top" />
                </ScrollToTop>
                <div className="container">
                    <FooterMenu />
                    <hr className="my-4" />
                    <FooterBottom />
                </div>
            </footer>
        );
    }
}

export default withStyles(s)(Footer);
