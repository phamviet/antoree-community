import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { setLocale } from 'actions/intl';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavDropdown,
} from 'reactstrap';
import { FormattedMessage, defineMessages } from 'react-intl';
import s from './LanguageSwitcher.css';

const messages = defineMessages({
    'en-US': {
        id: 'en-US',
        defaultMessage: 'English',
    },
    'vi-VN': {
        id: 'vi-VN',
        defaultMessage: 'Vietnamese',
    },
});

// todo setLocale action not work on Header, wtf?
class LanguageSwitcher extends React.Component {
    state = {
        open: false,
    };

    toggle = () => {
        this.setState({
            open: !this.state.open,
        });
    };

    render() {
        // eslint-disable-next-line no-shadow
        const { availableLocales, currentLocale } = this.props;
        return (
            <NavDropdown
                className={s.root}
                isOpen={this.state.open}
                toggle={this.toggle}
                dropup
            >
                <DropdownToggle
                    className={s.dropdownToggle}
                    role="button"
                    caret
                    outline
                >
                    <FormattedMessage {...messages[currentLocale]} />
                </DropdownToggle>
                <DropdownMenu>
                    {availableLocales.map(locale =>
                        <DropdownItem key={locale}>
                            <a href={`?lang=${locale}`}>
                                <FormattedMessage {...messages[locale]} />
                            </a>
                        </DropdownItem>,
                    )}
                </DropdownMenu>
            </NavDropdown>
        );
    }
}

LanguageSwitcher.propTypes = {
    currentLocale: PropTypes.string.isRequired,
    availableLocales: PropTypes.arrayOf(PropTypes.string).isRequired,
};

const mapState = state => ({
    availableLocales: state.runtime.availableLocales,
    currentLocale: state.intl.locale,
});

const mapDispatch = {
    setLocale,
};

export default compose(connect(mapState, mapDispatch), withStyles(s))(
    LanguageSwitcher,
);
