import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { injectIntl, FormattedMessage, defineMessages } from 'react-intl';
import LanguageSwitcher from './LanguageSwitcher';
import s from './FooterBottom.css';

const messages = defineMessages({
    terms: {
        id: 'footer.terms',
        defaultMessage: 'Term of uses',
    },
    termsLink: {
        id: 'footer.termsLink',
        defaultMessage: 'https://antoree.com/en/help/terms-of-use-6',
    },
    privacy: {
        id: 'footer.privacy',
        defaultMessage: 'Privacy policy',
    },
    privacyLink: {
        id: 'footer.privacyLink',
        defaultMessage: 'https://antoree.com/en/help/privacy-policy-7',
    },
});

class FooterBottom extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false,
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen,
        });
    }

    render() {
        // eslint-disable-next-line react/prop-types
        const { intl } = this.props;
        return (
            <div
                className={`d-flex flex-column-reverse flex-sm-row mt-3 ${s.root}`}
            >
                <div className="mt-3 mt-sm-0">
                    © {new Date().getFullYear()} Antoree Pte.Ltd
                </div>
                <Nav className="flex-column flex-sm-row align-items-center">
                    <NavItem>
                        <NavLink
                            href={intl.formatMessage(messages.privacyLink)}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <FormattedMessage {...messages.privacy} />
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            href={intl.formatMessage(messages.termsLink)}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <FormattedMessage {...messages.terms} />
                        </NavLink>
                    </NavItem>
                    <LanguageSwitcher />
                </Nav>
            </div>
        );
    }
}

export default withStyles(s)(injectIntl(FooterBottom));
