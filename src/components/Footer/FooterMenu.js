import React from 'react';
import { compose } from 'redux';
import classNames from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Row, Col, Nav, NavItem, NavLink } from 'reactstrap';
import { injectIntl, defineMessages } from 'react-intl';
import FaFacebook from 'react-icons/lib/fa/facebook';
import FaInstagram from 'react-icons/lib/fa/instagram';
import FaYoutubePlay from 'react-icons/lib/fa/youtube-play';
import FaSkype from 'react-icons/lib/fa/skype';
import zaloIcon from './zalo.png';
import s from './FooterMenu.css';

const messages = defineMessages({
    home: {
        id: 'footer.home',
        defaultMessage: 'Home',
    },
    careers: {
        id: 'footer.careers',
        defaultMessage: 'Careers',
    },
    help: {
        id: 'footer.help',
        defaultMessage: 'Help',
    },
    helpLink: {
        id: 'footer.helpLink',
        defaultMessage: 'https://antoree.com/en/tro-giup/thong-tin-co-ban-287',
    },
    about: {
        id: 'footer.about',
        defaultMessage: 'About us',
    },
    aboutLink: {
        id: 'footer.aboutLink',
        defaultMessage: 'https://antoree.com/en/help/thong-tin-co-ban-287',
    },
    contact: {
        id: 'footer.contact',
        defaultMessage: 'Contact us',
    },
    contactLink: {
        id: 'footer.contactLink',
        defaultMessage: 'https://antoree.com/en/contact-us',
    },
});

const getMenuItems = ({ intl }) => [
    {
        label: intl.formatMessage(messages.home),
        href: '/',
    },
    {
        label: intl.formatMessage(messages.careers),
        href: 'https://careers.antoree.com',
        target: '_blank',
    },
    {
        label: intl.formatMessage(messages.help),
        href: intl.formatMessage(messages.helpLink),
        target: '_blank',
    },
    {
        label: intl.formatMessage(messages.about),
        href: intl.formatMessage(messages.aboutLink),
        target: '_blank',
    },
    {
        label: intl.formatMessage(messages.contact),
        href: intl.formatMessage(messages.contactLink),
        target: '_blank',
    },
];

const socials = [
    {
        label: 'Facebook',
        icon: <FaFacebook width={18} height={18} />,
        href: 'https://www.facebook.com/antoree.co/',
        target: '_blank',
    },
    {
        label: 'Zalo',
        icon: (
            <span
                className={s.zalo}
                style={{ backgroundImage: `url("${zaloIcon}")` }}
            />
        ),
        href:
            'http://media.zalo.me/articles?zarsrc=7&pageId=269438753585304410',
        target: '_blank',
    },
    {
        label: 'Instagram',
        icon: <FaInstagram width={18} height={18} />,
        href: 'https://www.instagram.com/antoree.english',
        target: '_blank',
    },
    {
        label: 'Youtube',
        icon: <FaYoutubePlay width={18} height={18} />,
        href: 'https://www.youtube.com/channel/UCFFoOzIv-jDYUNfrgx4KRkA',
        target: '_blank',
    },
    {
        label: 'Skype',
        icon: <FaSkype width={18} height={18} />,
        href: 'skype:antoree.cc21?chat',
    },
];

const FooterMenu = props =>
    <Row className={classNames('justify-content-between')}>
        <Col xs={12} sm="auto">
            <Nav className="nav-left flex-column flex-sm-row">
                {getMenuItems(props).map(({ label, ...other }) =>
                    <NavItem className="d-block d-sm-inline-block" key={label}>
                        <NavLink
                            className="text-center"
                            title={label}
                            {...other}
                        >
                            {label}
                        </NavLink>
                    </NavItem>,
                )}
            </Nav>
        </Col>
        <Col xs={12} sm="auto">
            <Nav className="nav-right justify-content-center justify-content-md-end">
                {socials.map(({ icon, label, ...other }) =>
                    <NavItem key={other.href}>
                        <NavLink title={label} {...other}>
                            {icon}
                        </NavLink>
                    </NavItem>,
                )}
            </Nav>
        </Col>
    </Row>;

export default compose(withStyles(s), injectIntl)(FooterMenu);
