/* eslint-disable jsx-a11y/no-redundant-roles */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { NavItem } from 'reactstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Icon.css';

const reactButton = ({ onClick, disabled, active, text, children }) => {
    const props = {
        disabled,
        className: classNames(s.icon, active ? 'text-primary' : 'text-slate', {
            [s.text]: text,
        }),
    };

    if (!disabled && onClick && !text) {
        props.onClick = onClick;
        // eslint-disable-next-line react/prop-types
        props.role = 'button';
    }

    return (
        <NavItem {...props}>
            {children}
        </NavItem>
    );
};

reactButton.propTypes = {
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    active: PropTypes.bool,
    text: PropTypes.bool,
    children: PropTypes.node,
};

reactButton.defaultProps = {
    onClick: undefined,
    disabled: false,
    active: false,
    text: false,
    children: undefined,
};

const ReactButton = withStyles(s)(reactButton);

export function createButton(props) {
    return <ReactButton {...props} />;
}

export default ReactButton;
