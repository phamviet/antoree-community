import React from 'react';
import HeartOutlineIcon from 'components/Icons/HeartOutline';
import HeartIcon from 'components/Icons/Heart';
import { createButton } from './createButton';

export default ({ onClick, disabled, active }) =>
    createButton({
        onClick,
        disabled,
        active,
        stroke: true,
        children: active ? <HeartIcon /> : <HeartOutlineIcon />,
    });
