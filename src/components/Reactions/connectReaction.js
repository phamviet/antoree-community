import { connect } from 'react-redux';
import { toggleLike, toggleBookmark } from 'actions/post';

export default component =>
    connect(null, (dispatch, { id }) => ({
        onLikeClick: () => dispatch(toggleLike(id)),
        onBookmarkClick: () => dispatch(toggleBookmark(id)),
    }))(component);
