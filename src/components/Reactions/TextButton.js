import { createButton } from './createButton';

export default ({ children }) =>
    createButton({
        text: true,
        children,
    });
