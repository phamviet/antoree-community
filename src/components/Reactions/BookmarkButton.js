import React from 'react';
import BookmarkIcon from 'components/Icons/Bookmark';
import BookmarkOutlineIcon from 'components/Icons/BookmarkOutline';
import { createButton } from './createButton';

export default ({ onClick, disabled, active }) =>
    createButton({
        onClick,
        disabled,
        active,
        children: active ? <BookmarkIcon /> : <BookmarkOutlineIcon />,
    });
