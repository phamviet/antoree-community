import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Img from 'components/Img';
import s from './Avatar.css';

const Avatar = ({ filename, remote, url, size }) => {
    const src = remote || url;
    const style = { width: size, height: size };
    const img = src
        ? <img
              className={`rounded-circle ${s.avatarImg}`}
              src={src}
              alt="User Avatar"
              style={style}
          />
        : <div
              style={style}
              className={`rounded-circle ${s.avatarPlaceholder}`}
          />;

    const imgIx = filename
        ? <Img className="rounded-circle" path={filename} {...style} />
        : null;

    return imgIx || img;
};

Avatar.propTypes = {
    filename: PropTypes.string,
    remote: PropTypes.string,
    url: PropTypes.string,
    size: PropTypes.number,
};

Avatar.defaultProps = {
    filename: undefined,
    url: undefined,
    remote: undefined,
    size: 32,
};

export const AvatarComponent = Avatar;

export default withStyles(s)(Avatar);
