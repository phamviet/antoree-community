/* eslint-env jest */
/* eslint-disable padded-blocks, no-unused-expressions */

import React from 'react';
import renderer from 'react-test-renderer';
import { TopicPicker } from './Topic';

describe('Topic', () => {
    it('renders children correctly', () => {
        const wrapper = renderer.create(<TopicPicker value={[]} />).toJSON();

        expect(wrapper).toMatchSnapshot();
    });
});
