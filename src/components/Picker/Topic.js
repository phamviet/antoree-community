/* eslint-disable react/require-default-props */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import isEqual from 'lodash/isEqual';
import Autosuggest from 'react-autosuggest';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Chip from '../Chip';

// eslint-disable-next-line css-modules/no-unused-class
import theme from '../Autosuggest/theme.css';
import p from './Picker.css';

/* Suggestion helpers */
const getSuggestionValue = topic => topic.displayName;
const renderSuggestion = topic =>
    <div>
        {topic.displayName}
    </div>;

export class TopicPicker extends Component {
    static propTypes = {
        value: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string,
                displayName: PropTypes.string.isRequired,
            }),
        ),
        onChange: PropTypes.func,
        onRequestSuggestions: PropTypes.func,
        inputProps: PropTypes.shape({
            className: PropTypes.string,
        }),
    };

    static defaultProps = {
        value: [],
        onChange: () => {},
    };

    state = {
        suggestions: [],
        searchText: '',
    };

    emitChange = values => {
        this.props.onChange(values);
    };

    handleOnKeyDown = event => {
        switch (keycode(event)) {
            case 'enter': {
                event.preventDefault();
                const searchText = this.state.searchText;
                if (searchText !== '') {
                    this.addItem({ displayName: searchText });
                }
                break;
            }
            default:
                break;
        }
    };

    addItem = item => {
        if (
            !this.props.value.some(
                i =>
                    (item.id && i.id === item.id) ||
                    i.displayName === item.displayName,
            )
        ) {
            const { id, displayName } = item;
            const values = [...this.props.value, { id, displayName }];
            this.setState({ searchText: '' });
            this.emitChange(values);
        } else {
            // already existed, reset
            this.setState({ searchText: '' });
        }
    };

    handleOnRequestDelete = item => {
        const values = this.props.value.filter(i => !isEqual(i, item));
        this.emitChange(values);
    };

    // Autosuggest handlers
    handleOnInputChange = (event, { newValue }) => {
        this.setState({
            searchText: newValue,
        });
    };

    handleOnSuggestionsFetchRequested = async ({ value }) => {
        const suggestions = this.props.onRequestSuggestions
            ? await this.props.onRequestSuggestions(value, this.props.value)
            : [];

        this.setState({
            suggestions,
        });
    };

    handleOnSuggestionSelected = (event, { suggestion }) => {
        this.addItem(suggestion);
    };

    handleOnSuggestionsClearRequested = () => {
        this.setState({
            suggestions: [],
            searchText: '',
        });
    };

    render() {
        const inputProps = {
            value: this.state.searchText,
            onChange: this.handleOnInputChange,
            onKeyDown: this.handleOnKeyDown,
            autoComplete: 'off',
            ...this.props.inputProps,
        };

        return (
            <div>
                <div>
                    <Autosuggest
                        theme={theme}
                        suggestions={this.state.suggestions}
                        onSuggestionsFetchRequested={
                            this.handleOnSuggestionsFetchRequested
                        }
                        onSuggestionsClearRequested={
                            this.handleOnSuggestionsClearRequested
                        }
                        onSuggestionSelected={this.handleOnSuggestionSelected}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        inputProps={inputProps}
                    />
                </div>
                <div className={p.chipWrapper}>
                    {(this.props.value || []).map(item =>
                        <Chip
                            onRequestDelete={() =>
                                this.handleOnRequestDelete(item)}
                            key={item.id || item.displayName}
                        >
                            {item.displayName || ''}
                        </Chip>,
                    )}
                </div>
            </div>
        );
    }
}

const Topic = withStyles(p, theme)(TopicPicker);

const onFieldChange = (fields, tags) => {
    fields.removeAll();
    tags.map(fields.push);
};

export const TopicField = ({ fields, ...props }) =>
    <Topic
        {...props}
        value={fields.getAll()}
        onChange={tags => onFieldChange(fields, tags)}
    />;

TopicField.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    fields: PropTypes.object.isRequired,
};

export default Topic;
