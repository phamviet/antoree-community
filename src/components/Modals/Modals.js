import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const Modals = ({ modals }) =>
    <div>
        {(modals || [])
            .map(({ component, isOpen }) =>
                React.createElement(component, { key: component.key, isOpen }),
            )}
    </div>;

Modals.propTypes = {
    modals: PropTypes.arrayOf(
        PropTypes.shape({
            isOpen: PropTypes.bool.isRequired,
            component: PropTypes.func.isRequired,
        }),
    ).isRequired,
};

export default connect(({ runtime }) => ({ modals: runtime.modals || [] }), {})(
    Modals,
);
