/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CloseIcon from '../Icons/XRemove';

const propTypes = {
    wrapTag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    toggle: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.node,
    closeAriaLabel: PropTypes.string,
};

const defaultProps = {
    wrapTag: 'div',
    closeAriaLabel: 'Close',
};

const ModalHeader = props => {
    let closeButton;
    const {
        className,
        children,
        toggle,
        wrapTag: WrapTag,
        closeAriaLabel,
        ...attributes
    } = props;

    if (toggle) {
        closeButton = (
            <button
                type="button"
                onClick={toggle}
                className="close"
                aria-label={closeAriaLabel}
            >
                <CloseIcon aria-hidden="true" />
            </button>
        );
    }

    return (
        <WrapTag
            {...attributes}
            className={classNames(className, 'modal-header')}
        >
            {children}
            {closeButton}
        </WrapTag>
    );
};

ModalHeader.propTypes = propTypes;
ModalHeader.defaultProps = defaultProps;

export default ModalHeader;
