import Modals from './Modals';
import LearningRequest from './LearningRequest';
import ModalHeader from './ModalHeader';

export { ModalHeader, LearningRequest };

export default Modals;
