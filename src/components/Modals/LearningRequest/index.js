/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Validator from 'validatorjs';
import { compose } from 'redux';
import DoneIcon from 'react-icons/lib/md/done';
import { Field, reduxForm } from 'redux-form';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
    Button,
    Media,
    Modal,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    Input,
} from 'reactstrap';
import { connect } from 'react-redux';
import { closeModal } from 'actions/modal';
import { injectIntl, defineMessages } from 'react-intl';
import FormField from 'components/helpers/FormField';
import sharedMessages from 'components/messages';
import ModalHeader from '../ModalHeader';
import action from './action';
import s from './LearningRequest.css';

const messages = defineMessages({
    successMessage: {
        id: 'LearningRequest.successMessage',
        defaultMessage:
            'Thank you for registering. We will response to you soon!',
    },
});

const registerOptions = [
    {
        value: 0,
        name: 'Người đi làm',
    },
    {
        value: 1,
        name: 'Phụ huynh đăng ký cho con',
    },
    {
        value: 2,
        name: 'Học sinh',
    },
];

const key = 'LearningRequest';
const validate = (data, { intl }) => {
    const rules = {
        name: 'required|min:3|max:64',
        email: 'required|email|max:128',
        phone: 'required|numeric',
    };

    const validator = new Validator(data, rules, {
        required: intl.formatMessage(sharedMessages.requiredMessage),
        email: intl.formatMessage(sharedMessages.invalidEmailMessage),
    });
    validator.setAttributeNames({
        name: intl.formatMessage(sharedMessages.fullName),
        phone: intl.formatMessage(sharedMessages.phone),
    });

    validator.passes();
    return validator.errors.all();
};

const Done = ({ message }) =>
    <Media className="align-items-center">
        <Media tag="h2" className="text-primary" left>
            <DoneIcon />
        </Media>
        <Media className="ml-3" body>
            <strong>
                {message}
            </strong>
        </Media>
    </Media>;

const LearningRequest = ({
    kid,
    isOpen,
    onToggle,
    handleSubmit,
    reset,
    pristine,
    submitting,
    submitSucceeded,
    intl,
}) =>
    <Modal onExit={reset} className={classNames(s.root)} isOpen={isOpen}>
        <Form onSubmit={handleSubmit}>
            <ModalHeader toggle={() => onToggle(key)}>
                <div />
            </ModalHeader>
            <ModalBody>
                <div className={classNames(s.contentWrapper, 'mb-5')}>
                    <h3 className={classNames(s.title, 'mb-4')}>
                        Đăng ký kiểm tra trình độ miễn phí {kid && 'cho bé'}
                    </h3>
                    <p className="text-slate">
                        Điền thông tin liên hệ để được kiểm tra trình độ và tư
                        vấn lộ trình học miễn phí.
                    </p>
                </div>
                {submitSucceeded
                    ? <Done
                          message={intl.formatMessage(messages.successMessage)}
                      />
                    : <div>
                          <FormField
                              component={Input}
                              type="text"
                              size="lg"
                              placeholder={intl.formatMessage(
                                  sharedMessages.fullName,
                              )}
                              name="name"
                          />
                          <FormField
                              component={Input}
                              type="email"
                              size="lg"
                              placeholder="Email"
                              name="email"
                          />
                          <FormField
                              component={Input}
                              placeholder={intl.formatMessage(
                                  sharedMessages.phone,
                              )}
                              type="text"
                              size="lg"
                              name="phone"
                          />
                          <FormField
                              component={Input}
                              className="custom-select"
                              type="select"
                              size="lg"
                              name="register"
                          >
                              {registerOptions.map(({ name, value }) =>
                                  <option key={value} value={value}>
                                      {name}
                                  </option>,
                              )}
                          </FormField>
                          <FormGroup check>
                              <Label className="text-slate" check>
                                  <Field
                                      component="input"
                                      name="subscribed"
                                      type="checkbox"
                                      className="form-check-input"
                                      value="true"
                                  />{' '}
                                  Tôi đồng ý nhận email thông tin về các khóa
                                  học của Antoree.
                              </Label>
                          </FormGroup>
                      </div>}
            </ModalBody>
            <ModalFooter>
                {submitSucceeded
                    ? <Button
                          role="button"
                          type="button"
                          size="lg"
                          onClick={() => onToggle(key)}
                      >
                          {intl.formatMessage(sharedMessages.close)}
                      </Button>
                    : <Button
                          role="button"
                          type="submit"
                          size="lg"
                          disabled={pristine || submitting}
                          color="primary"
                      >
                          {intl.formatMessage(sharedMessages.register)}
                      </Button>}{' '}
                <Field component={Input} type="hidden" name="postId" />
            </ModalFooter>
        </Form>
    </Modal>;

LearningRequest.propTypes = {
    isOpen: PropTypes.bool,
    kid: PropTypes.bool,
    // eslint-disable-next-line react/no-unused-prop-types
    onSubmit: PropTypes.func.isRequired,
    onToggle: PropTypes.func.isRequired,
};

LearningRequest.defaultProps = {
    isOpen: false,
    kid: false,
    onSubmit: action,
};

const Component = compose(
    connect(
        (state, props) => ({
            initialValues: {
                subscribed: true,
                register: props.kid ? 1 : 0,
                ...props.initialValues,
            },
        }),
        {
            onToggle: closeModal,
            onSubmit: action,
        },
    ),
    injectIntl,
    reduxForm({
        form: key,
        validate,
        enableReinitialize: true,
        onSubmitSuccess: (result, dispatch, { onToggle }) =>
            setTimeout(() => onToggle(key), 5000),
        onSubmitFail: (errors, dispatch, submitError) =>
            console.error(submitError),
    }),
    withStyles(s),
)(LearningRequest);

Component.key = key;

export default Component;
