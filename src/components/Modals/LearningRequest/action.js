import ReactGA from 'react-ga';
import { SubmissionError } from 'redux-form';
import requestLearning from 'shared/mutations/requestLearning.graphql';

export default input => (dispatch, getState, { client }) =>
    client
        .mutate({
            mutation: requestLearning,
            variables: { input },
        })
        .then(({ data }) => {
            if (data.errors) {
                throw new SubmissionError({
                    _error: new Error(data.errors[0].message),
                });
            }

            const { meta } = data.result.data;
            ReactGA.event({
                category: 'User',
                action: 'LR_SUBMITTED',
                label: meta.utm_campaign,
            });

            return data.result;
        })
        .catch(error => {
            throw new SubmissionError({ _error: error });
        });
