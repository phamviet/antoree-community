/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { compose } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
    Form,
    Collapse,
    Card,
    CardBlock,
    Button,
    Media,
    Input,
} from 'reactstrap';
import FormField from '../helpers/FormField';
import Avatar, { AvatarComponent } from '../Avatar';
import s from './CommentBox.css';

class CommentBox extends React.Component {
    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        user: PropTypes.shape({
            displayName: PropTypes.string,
            ...AvatarComponent.propTypes,
        }),
    };

    static defaultProps = {
        pristine: false,
        submitting: false,
        user: undefined,
    };

    state = { collapse: false };

    toggle = () => {
        this.setState({ collapse: !this.state.collapse });
    };

    render() {
        const { pristine, submitting, user, handleSubmit } = this.props;
        const { collapse } = this.state;
        return (
            <Card className={s.root} role="presentation">
                <CardBlock onClick={this.toggle}>
                    <Media className="align-items-center">
                        {user &&
                            <Media left>
                                <Avatar
                                    {...user.photo}
                                    size={50}
                                    className="media-object"
                                />
                            </Media>}
                        <Media body>
                            <Media className={s.placeholder}>
                                {collapse && user
                                    ? <span className={s.placeholderActive}>
                                          {user.displayName}
                                      </span>
                                    : 'Write a response'}
                            </Media>
                        </Media>
                    </Media>
                </CardBlock>
                <Collapse isOpen={this.state.collapse}>
                    <CardBlock>
                        <Form onSubmit={handleSubmit} method="POST">
                            <FormField
                                className={s.content}
                                name="content"
                                component={Input}
                                readOnly={!user}
                                autoFocus
                                placeholder={
                                    user
                                        ? 'What is your idea ?'
                                        : 'Please login!'
                                }
                                type="textarea"
                            />
                            <Field
                                name="authorId"
                                component="input"
                                type="hidden"
                            />
                            <Field
                                name="postId"
                                component="input"
                                type="hidden"
                            />
                            <Button
                                type="submit"
                                color="primary"
                                className={`mr-3`}
                                disabled={!user || pristine || submitting}
                            >
                                Comment
                            </Button>
                            <Button type="reset" onClick={this.toggle}>
                                Cancel
                            </Button>
                        </Form>
                    </CardBlock>
                </Collapse>
            </Card>
        );
    }
}

export default compose(reduxForm({ form: 'comment' }), withStyles(s))(
    CommentBox,
);
