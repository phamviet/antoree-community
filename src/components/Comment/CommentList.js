import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import ReactList from 'react-list';
import CommentItem, { CommentItemComponent } from './CommentItem';

class CommentList extends Component {
    static propTypes = {
        comments: PropTypes.arrayOf(
            PropTypes.shape(CommentItemComponent.propTypes),
        ),
    };

    static defaultProps = {
        comments: [],
    };

    renderItem = (index, key) => {
        const comment = this.props.comments[index];
        return <CommentItem key={key} {...comment} />;
    };

    render() {
        return (
            <ReactList
                ref={ref => (this.ref = ref)}
                itemRenderer={this.renderItem}
                length={this.props.comments.length}
            />
        );
    }
}

export default withStyles()(CommentList);
