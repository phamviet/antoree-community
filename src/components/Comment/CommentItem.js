import React from 'react';
import PropTypes from 'prop-types';
import { FormattedRelative } from 'react-intl';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Author, { AuthorComponent } from '../Author/Author';
import s from './CommentItem.css';

const CommentItem = ({ content, author, createdAt }) =>
    <div className={s.commentItem}>
        <div>
            <Author
                {...author}
                bold={false}
                subtitle={<FormattedRelative value={createdAt} />}
            />
        </div>
        <p className={s.content}>
            {content}
        </p>
    </div>;

CommentItem.propTypes = {
    author: PropTypes.shape(AuthorComponent.propTypes).isRequired,
    content: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
};

export const CommentItemComponent = CommentItem;
export default withStyles(s)(CommentItem);
