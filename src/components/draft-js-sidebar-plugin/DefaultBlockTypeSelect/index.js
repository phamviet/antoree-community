/* eslint-disable react/prop-types */
import React from 'react';

import { AddEmbedButton, AddEmojiButton } from 'draft-js-buttons-plugin';

import BlockTypeSelect from '../components/BlockTypeSelect';
import AddImageButton from '../components/AddImageButton';

const DefaultBlockTypeSelect = ({
    getEditorState,
    setEditorState,
    theme,
    store,
    openModal,
    closeModal,
    ...other
}) =>
    <BlockTypeSelect
        {...other}
        getEditorState={getEditorState}
        setEditorState={setEditorState}
        theme={theme}
        store={store}
        openModal={openModal}
        closeModal={closeModal}
        structure={[AddImageButton, AddEmbedButton, AddEmojiButton]}
    />;

export default DefaultBlockTypeSelect;
