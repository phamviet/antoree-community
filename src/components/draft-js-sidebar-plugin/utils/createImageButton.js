/* eslint-disable react/prop-types,react/no-children-prop */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import insertDataBlock from './insertDataBlock';

export default ({ children }) =>
    class imageButton extends Component {
        onClick = e => {
            e.preventDefault();
            // eslint-disable-next-line react/no-find-dom-node
            ReactDOM.findDOMNode(this.fileInput).click();
        };

        inputChange = e => {
            const file = e.target.files[0];
            this.props.uploadCallback({ file }, (error, f) => {
                if (!error) {
                    this.props.setEditorState(
                        insertDataBlock(
                            this.props.getEditorState(),
                            {
                                src: f.url,
                                id: f.id,
                                type: 'placeholder',
                                alignment: 'center',
                            },
                            'image',
                        ),
                    );
                }
                // reset input
                this.fileInput.value = '';
            });
        };

        preventBubblingUp = event => {
            event.preventDefault();
        };

        render() {
            const { theme } = this.props;
            return (
                // eslint-disable-next-line jsx-a11y/no-static-element-interactions
                <div
                    className={theme.buttonWrapper}
                    onMouseDown={this.preventBubblingUp}
                >
                    <button
                        className={theme.button}
                        onClick={this.onClick}
                        type="button"
                        children={children}
                    />

                    <div className={theme.addImage}>
                        <input
                            type="file"
                            accept="image/*"
                            ref={fileInput => {
                                this.fileInput = fileInput;
                            }}
                            onChange={this.inputChange}
                            style={{ display: 'none' }}
                        />
                    </div>
                </div>
            );
        }
    };
