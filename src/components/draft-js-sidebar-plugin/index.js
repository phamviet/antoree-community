/* eslint-disable css-modules/no-unused-class */
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import decorateComponentWithProps from 'decorate-component-with-props';
import createStore from 'last-draft-js-sidebar-plugin/lib/utils/createStore';
import getModalByType from 'last-draft-js-sidebar-plugin/lib/components/getModalByType';
import Sidebar from './components/Sidebar';
import DefaultBlockTypeSelect from './DefaultBlockTypeSelect';
import buttonStyles from './buttonStyles.css';
import blockTypeSelectStyles from './blockTypeSelectStyles.css';
import sidebarStyles from './sidebarStyles.css';
import modalStyles from './modalStyles.css';
import emojiPickerStyles from './emojiPickerStyles.css';

export default (config = {}) => {
    const defaultTheme = {
        buttonStyles,
        blockTypeSelectStyles,
        sidebarStyles,
        modalStyles,
        emojiPickerStyles,
    };

    const store = createStore({
        isVisible: false,
    });

    const {
        theme = defaultTheme,
        structure = [DefaultBlockTypeSelect],
        ...other
    } = config;
    const sidebarProps = {
        store,
        structure,
        getModalByType,
        theme,
        ...other,
    };

    return {
        initialize: ({ setEditorState, getEditorState, getEditorRef }) => {
            store.updateItem('getEditorState', getEditorState);
            store.updateItem('setEditorState', setEditorState);
            store.updateItem('getEditorRef', getEditorRef);
        },
        // Re-Render the sidebar on every change
        onChange: editorState => {
            store.updateItem('editorState', editorState);
            return editorState;
        },
        Sidebar: decorateComponentWithProps(
            withStyles(
                sidebarStyles,
                blockTypeSelectStyles,
                buttonStyles,
                modalStyles,
                emojiPickerStyles,
            )(Sidebar),
            sidebarProps,
        ),
    };
};
