/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Button, ButtonGroup } from 'reactstrap';

const SelectButton = ({
    options,
    isActive,
    onChange,
    btnProps = {},
    ...props
}) =>
    <ButtonGroup {...props}>
        {options.map(({ value, label }) => {
            const active = isActive(value);
            return (
                <Button
                    onClick={() => onChange(value)}
                    color={active ? 'primary' : 'secondary'}
                    outline={!active}
                    key={value}
                    role="button"
                    {...btnProps}
                >
                    {label}
                </Button>
            );
        })}
    </ButtonGroup>;

SelectButton.propTypes = {
    options: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
                .isRequired,
        }),
    ).isRequired,
    isActive: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default SelectButton;
