import React, { PropTypes, Component } from 'react';
import Slider from 'react-slick';

class RelatedPost extends Component {
    static propTypes = {
        // todo
        // eslint-disable-next-line react/forbid-prop-types
        posts: PropTypes.array.isRequired,
    };
    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
        };
        return (
            <Slider {...settings}>
                <div>
                    <h3>6</h3>
                </div>
            </Slider>
        );
    }
}

export default RelatedPost;
