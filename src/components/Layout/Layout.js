import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import LoadingBar from 'react-redux-loading-bar';
import { Container } from 'reactstrap';
import ReduxToastr from 'react-redux-toastr';
import Modals from 'components/Modals';

// external-global styles must be imported in your JS.
import s from './Layout.css';
import Header from '../Header';
import Footer from '../Footer';

class Layout extends React.Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
        fullWidth: PropTypes.bool,
        headerProps: PropTypes.shape({
            navigation: PropTypes.func,
            fixed: PropTypes.string,
        }),
        blank: PropTypes.bool,
    };

    static defaultProps = {
        fullWidth: false,
        headerProps: {
            fixed: undefined,
        },
        blank: false,
    };

    render() {
        const { children, fullWidth, blank, headerProps } = this.props;
        return (
            <div>
                <LoadingBar className={s.loadingBar} maxProgress={80} />
                <ReduxToastr
                    timeOut={3000}
                    preventDuplicates
                    position="bottom-right"
                    transitionIn="fadeIn"
                    transitionOut="fadeOut"
                />
                <div className={headerProps.fixed ? s.pageFixed : ''}>
                    {!blank ? <Header {...headerProps} /> : null}

                    <section>
                        {fullWidth
                            ? children
                            : <Container>
                                  {children}
                              </Container>}
                    </section>
                </div>
                {!blank ? <Footer /> : null}
                <Modals />
            </div>
        );
    }
}

export default withStyles(s)(Layout);
