/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'redux-form';
import { FormGroup, FormFeedback, Label } from 'reactstrap';

class FormField extends React.Component {
    static propTypes = {
        component: PropTypes.oneOfType([PropTypes.func, PropTypes.string])
            .isRequired,
        label: PropTypes.string,
        array: PropTypes.bool,
    };

    static defaultProps = {
        label: null,
        array: false,
    };

    field = ({ componentToRender, ...props }) => {
        const { input, meta, ...other } = props;
        const hasError = meta.touched && meta.error;
        return (
            <FormGroup {...(hasError ? { color: 'danger' } : {})}>
                {props.label &&
                    <Label>
                        {props.label}
                    </Label>}
                {React.createElement(componentToRender, {
                    ...input,
                    ...other,
                    ...(hasError ? { state: 'danger' } : {}),
                })}
                {hasError &&
                    <FormFeedback>
                        {meta.error}
                    </FormFeedback>}
            </FormGroup>
        );
    };

    render() {
        const { component, array, ...props } = this.props;

        return React.createElement(array ? FieldArray : Field, {
            componentToRender: component,
            component: this.field,
            ...props,
        });
    }
}

export default FormField;
