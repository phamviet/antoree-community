/* eslint-disable react/require-default-props,react/forbid-prop-types */
/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import chunkIt from 'lodash/chunk';
import { Row } from 'reactstrap';
import Card, { CardComponent } from 'components/Post/Card';
import SectionTitle from 'components/Post/SectionTitle';

const CardList = ({ posts, title, titleProps, cols, moreLink, left, render }) =>
    <div className="mt-3 mt-sm-5">
        <SectionTitle
            title={title}
            link={moreLink && posts.length > 0 ? moreLink : null}
            {...titleProps}
        />
        {chunkIt(posts, cols).map((chunk, i) =>
            /* eslint-disable react/no-array-index-key */
            <Row key={i}>
                {chunk.map(post =>
                    <div
                        className={
                            left
                                ? `col-12 col-lg-${12 / cols}`
                                : cols === 3
                                  ? 'col-12 col-md-6 col-lg-4'
                                  : `col-12 col-md-${12 / cols}`
                        }
                        key={post.id}
                    >
                        {render(post)}
                    </div>,
                )}
            </Row>,
        )}
    </div>;

CardList.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.shape(CardComponent.propTypes))
        .isRequired,
    cols: PropTypes.oneOf([2, 3]),
    title: PropTypes.string,
    titleProps: PropTypes.object,
    moreLink: PropTypes.string,
    left: PropTypes.bool,
    render: PropTypes.func,
};

CardList.defaultProps = {
    cols: 2,
    titleProps: {},
    cardProps: {},
    moreLink: undefined,
    left: false,
    render: post => <Card {...post} />,
};

export default CardList;
