import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Nav,
    NavItem,
    Button,
    NavDropdown,
    DropdownMenu,
    DropdownToggle,
    DropdownItem,
} from 'reactstrap';
import { defineMessages, FormattedMessage } from 'react-intl';
import Link from 'components/Link';
import { AvatarComponent } from 'components/Avatar';

const messages = defineMessages({
    logout: {
        id: 'navigation.logout',
        defaultMessage: 'Log out',
        description: 'Log out link in header',
    },
    compose: {
        id: 'navigation.compose',
        defaultMessage: 'Write a story',
        description: 'Write a story button in header',
    },
    profile: {
        id: 'navigation.profile',
        defaultMessage: 'Profile',
    },
});

class AuthenticatedNavigation extends Component {
    static propTypes = {
        user: PropTypes.shape({
            id: PropTypes.string,
            displayName: PropTypes.string,
            isEditor: PropTypes.bool,
            isAdministrator: PropTypes.bool,
            photo: PropTypes.shape(AvatarComponent.propTypes),
        }).isRequired,
    };

    state = {
        isOpen: false,
    };

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    };

    render() {
        const {
            displayName,
            id,
            isEditor,
            isAdministrator,
            photo,
        } = this.props.user;
        return (
            <Nav className="align-items-center">
                <NavItem className="mr-1 mr-md-2">
                    {isEditor &&
                        <Button
                            tag={Link}
                            className="hidden-xs-down"
                            color="primary"
                            to="/compose"
                        >
                            <FormattedMessage {...messages.compose} />
                        </Button>}
                </NavItem>
                <NavDropdown isOpen={this.state.isOpen} toggle={this.toggle}>
                    <DropdownToggle nav caret className="py-0">
                        <AvatarComponent {...photo} size={26} />
                        <span className="ml-2 hidden-sm-down">
                            {displayName}
                        </span>
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem tag={Link} to={`/profile/${id}`}>
                            <FormattedMessage {...messages.profile} />
                        </DropdownItem>
                        {isEditor &&
                            <div>
                                <DropdownItem divider />
                                <DropdownItem header>Admin</DropdownItem>
                                <DropdownItem tag={Link} to="/manage/posts/">
                                    Manage Articles
                                </DropdownItem>
                                <DropdownItem tag={Link} to="/manage/topics/">
                                    Manage Topics
                                </DropdownItem>
                                {isAdministrator &&
                                    <DropdownItem
                                        tag={Link}
                                        to="/manage/users/"
                                    >
                                        Manage Users
                                    </DropdownItem>}
                            </div>}
                        <DropdownItem divider />
                        <DropdownItem tag="a" href="/logout">
                            <FormattedMessage {...messages.logout} />
                        </DropdownItem>
                    </DropdownMenu>
                </NavDropdown>
            </Nav>
        );
    }
}

export default connect(({ user }) => ({ user }))(AuthenticatedNavigation);
