import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Container, Nav, NavItem, NavLink } from 'reactstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Link from 'components/Link';
import s from './HeaderMenu.css';

const notActive = () => false;

const HeaderMenu = ({ items, isActive }) =>
    <div className={classNames('nav-inverse', s.root)}>
        <Container>
            <Nav className="nav-left">
                {(items || []).map(entity =>
                    <NavItem key={entity.id}>
                        <NavLink
                            tag={Link}
                            title={entity.displayName}
                            to={entity.href}
                            className={classNames({ active: isActive(entity) })}
                        >
                            {entity.displayName}
                        </NavLink>
                    </NavItem>,
                )}
            </Nav>
        </Container>
    </div>;

HeaderMenu.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            displayName: PropTypes.string.isRequired,
            href: PropTypes.string.isRequired,
        }),
    ).isRequired,
    isActive: PropTypes.func,
};

HeaderMenu.defaultProps = {
    isActive: notActive,
};

export default withStyles(s)(HeaderMenu);
