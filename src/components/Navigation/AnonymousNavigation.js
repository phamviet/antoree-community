import React from 'react';
import { compose } from 'redux';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { injectIntl, defineMessages, FormattedMessage } from 'react-intl';
import Link from 'components/Link';

const messages = defineMessages({
    login: {
        id: 'navigation.login',
        defaultMessage: 'Sign in',
        description: 'Sign in link in header',
    },
});

// eslint-disable-next-line react/prop-types
const AnonymousNavigation = ({ intl }) =>
    <Nav className="nav-right">
        <NavItem>
            <NavLink
                tag={Link}
                title={intl.formatMessage(messages.login)}
                to="/login"
            >
                <FormattedMessage {...messages.login} />
            </NavLink>
        </NavItem>
    </Nav>;

export default compose(injectIntl)(AnonymousNavigation);
