import React from 'react';
import PropTypes from 'prop-types';

const ListenIcon = props =>
    <svg
        x="0px"
        y="0px"
        viewBox="0 0 24 24"
        xmlns="preserve"
        width={props.size}
        height={props.size}
    >
        <g className="nc-icon-wrapper" fill="#444444">
            <path
                fill="none"
                stroke="#fff"
                strokeWidth="2"
                strokeMiterlimit="10"
                d="M2,14.3V9c0-4.4,3.6-8,8-8h4 c4.4,0,8,3.6,8,8v5.3"
                strokeLinejoin="miter"
                strokeLinecap="butt"
            />
            <line
                fill="none"
                stroke="#fff"
                strokeWidth="2"
                strokeMiterlimit="10"
                x1="5"
                y1="12.2"
                x2="5"
                y2="22.8"
                strokeLinejoin="miter"
                strokeLinecap="butt"
            />
            <line
                fill="none"
                stroke="#fff"
                strokeWidth="2"
                strokeMiterlimit="10"
                x1="19"
                y1="12.2"
                x2="19"
                y2="22.8"
                strokeLinejoin="miter"
                strokeLinecap="butt"
            />
            <path
                fill="none"
                stroke="#fff"
                strokeWidth="2"
                strokeLinecap="square"
                strokeMiterlimit="10"
                d="M8,12.2 C7.5,12.1,7,12,6.5,12c-3,0-5.5,2.5-5.5,5.5c0,3,2.5,5.5,5.5,5.5c0.5,0,1-0.1,1.5-0.2V12.2z"
                strokeLinejoin="miter"
            />
            <path
                fill="none"
                stroke="#fff"
                strokeWidth="2"
                strokeLinecap="square"
                strokeMiterlimit="10"
                d="M16,22.8 c0.5,0.1,1,0.2,1.5,0.2c3,0,5.5-2.5,5.5-5.5c0-3-2.5-5.5-5.5-5.5c-0.5,0-1,0.1-1.5,0.2V22.8z"
                strokeLinejoin="miter"
            />
        </g>
    </svg>;

ListenIcon.propTypes = {
    size: PropTypes.number.isRequired,
};

ListenIcon.defaultProps = {
    size: 19,
};

export default ListenIcon;
