import React from 'react';
import Icon from 'react-icon-base';

const TailRight = props =>
    <Icon viewBox="0 0 24 24" {...props}>
        <g>
            <path d="M22.707,11.293L15,3.586L13.586,5l6,6H2c-0.553,0-1,0.448-1,1s0.447,1,1,1h17.586l-6,6L15,20.414 l7.707-7.707C23.098,12.316,23.098,11.684,22.707,11.293z" />
        </g>
    </Icon>;

export default TailRight;
