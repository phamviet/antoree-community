import React from 'react';
import PropTypes from 'prop-types';

const GenderIcon = props =>
    <svg
        x="0px"
        y="0px"
        viewBox="0 0 24 24"
        xmlns="preserve"
        width={props.size}
        height={props.size}
    >
        <g className="nc-icon-wrapper" fill="#444444">
            <line
                fill="none"
                stroke="#444444"
                strokeWidth="2"
                strokeMiterlimit="10"
                x1="15.657"
                y1="8.343"
                x2="22"
                y2="2"
                strokeLinejoin="miter"
                strokeLinecap="butt"
            />
            <polyline
                fill="none"
                stroke="#444444"
                strokeWidth="2"
                strokeLinecap="square"
                strokeMiterlimit="10"
                points=" 16,2 22,2 22,8 "
                strokeLinejoin="miter"
            />
            <circle
                fill="none"
                stroke="#444444"
                strokeWidth="2"
                strokeLinecap="square"
                strokeMiterlimit="10"
                cx="10"
                cy="14"
                r="8"
                strokeLinejoin="miter"
            />
        </g>
    </svg>;

GenderIcon.propTypes = {
    size: PropTypes.number.isRequired,
};

GenderIcon.defaultProps = {
    size: 20,
};

export default GenderIcon;
