/* eslint-disable react/prop-types */
import React from 'react';
import Icon from 'react-icon-base';

const XRemove = ({ width = 16, height = 16, ...attributes }) =>
    <Icon viewBox="0 0 16 16" width={width} height={height} {...attributes}>
        <g>
            <line
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeMiterlimit="10"
                x1="13.5"
                y1="2.5"
                x2="2.5"
                y2="13.5"
                data-cap="butt"
            />{' '}
            <line
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeMiterlimit="10"
                x1="2.5"
                y1="2.5"
                x2="13.5"
                y2="13.5"
                data-cap="butt"
            />
        </g>
    </Icon>;

export default XRemove;
