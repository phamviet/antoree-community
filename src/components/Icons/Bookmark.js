/* eslint-disable react/prop-types */
import React from 'react';
import Icon from 'react-icon-base';

const Bookmark = ({ width = 25, height = 25, ...other }) =>
    <Icon viewBox="0 0 25 26" width={width} height={height} {...other}>
        <g>
            <path
                d="M19 7c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v14.66h.012c.01.103.045.204.12.285a.5.5 0 0 0 .706.03L12.5 17.85l5.662 4.126a.508.508 0 0 0 .708-.03.5.5 0 0 0 .118-.285H19V7z"
                fillRule="evenodd"
            />
        </g>
    </Icon>;

export default Bookmark;
