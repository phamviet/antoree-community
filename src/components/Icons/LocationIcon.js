import React from 'react';
import PropTypes from 'prop-types';

const LocationIcon = props =>
    <svg
        version="1.1"
        x="0px"
        y="0px"
        viewBox="0 0 64 64"
        xmlns="preserve"
        width={props.size}
        height={props.size}
    >
        <g className="nc-icon-wrapper" fill="#000000">
            <path
                fill="none"
                stroke="#000000"
                strokeWidth="2"
                strokeLinecap="square"
                strokeMiterlimit="10"
                d="M54,24.095 C54,39,32,61,32,61S10,39,10,24.095C10,10.113,21.369,2,32,2S54,10.113,54,24.095z"
                strokeLinejoin="miter"
            />
            <circle
                fill="none"
                stroke="#000000"
                strokeWidth="2"
                strokeLinecap="square"
                strokeMiterlimit="10"
                cx="32"
                cy="24"
                r="8"
                strokeLinejoin="miter"
            />
        </g>
    </svg>;

LocationIcon.propTypes = {
    size: PropTypes.number.isRequired,
};

LocationIcon.defaultProps = {
    size: 20,
};

export default LocationIcon;
