/* eslint-disable react/prop-types */
import React from 'react';
import Icon from 'react-icon-base';

const HeartOutline = ({ width = 18, height = 18, ...other }) =>
    <Icon viewBox="0 0 16 16" width={width} height={height} {...other}>
        <g>
            <path
                fill="none"
                stroke="currentColor"
                strokeWidth="1"
                strokeLinecap="round"
                strokeMiterlimit="10"
                d="M14.328,2.672 c-1.562-1.562-4.095-1.562-5.657,0C8.391,2.952,8.18,3.27,8,3.601c-0.18-0.331-0.391-0.65-0.672-0.93 c-1.562-1.562-4.095-1.562-5.657,0c-1.562,1.562-1.562,4.095,0,5.657L8,14.5l6.328-6.172C15.891,6.766,15.891,4.234,14.328,2.672z"
            />
        </g>
    </Icon>;

export default HeartOutline;
