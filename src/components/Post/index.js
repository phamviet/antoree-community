import Form, { PostFormComponent, FORM_NAME } from './Form';
import savePostMutate from './savePostMutate.graphql';
import PostMutable from './PostMutable.graphql';

export {
    Form as PostForm,
    PostFormComponent,
    FORM_NAME,
    PostMutable,
    savePostMutate,
};
