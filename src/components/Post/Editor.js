import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import classNames from 'classnames';
import Editor from 'draft-js-plugins-editor';
import { RichUtils } from 'draft-js';
import editorStyles from 'draft-js/dist/Draft.css';

import { withEditorState as createBlockStyleWithEditorState } from 'lib/draft-js/utils/blockStyleFn';

/* Side Toolbar */
import sidebarStyles from 'last-draft-js-sidebar-plugin/lib/plugin.css';
import createSidebarPlugin from 'components/draft-js-sidebar-plugin';

/* Toolbar */
import createInlineToolbarPlugin, {
    Separator,
} from 'last-draft-js-toolbar-plugin';
import inlineToolbarStyles from 'last-draft-js-toolbar-plugin/lib/plugin.css';
import {
    ItalicButton,
    BoldButton,
    UnderlineButton,
    CodeButton,
    HeadlineTwoButton,
    HeadlineThreeButton,
    UnorderedListButton,
    OrderedListButton,
    BlockquoteButton,
} from 'draft-js-buttons';
import { AddLinkButton } from 'draft-js-buttons-plugin';
import TextCenterButton from 'lib/draft-js/buttons/TextCenterButton';
import TextRightButton from 'lib/draft-js/buttons/TextRightButton';

/* Embed plugin */
import createEmbedPlugin from 'draft-js-embed-plugin';
import EmbedComponent from 'components/Editor/embed-plugin/Component';
import embedStyles from 'draft-js-embed-plugin/lib/plugin.css';

/* Link plugin */
import createLinkPlugin from 'draft-js-link-plugin';
import LinkComponent from 'components/Editor/link-plugin/Link';

/* Linkify */
import createLinkifyPlugin from 'draft-js-linkify-plugin';

/* Emoji plugin */
import createEmojiPlugin from 'draft-js-emoji-plugin';
import emojiStyles from 'draft-js-emoji-plugin/lib/plugin.css';

/* Auto list */
import createAutoListPlugin from 'draft-js-autolist-plugin';

/* Image with Alignment, dnd, focus, resize plugin */
import createImagePlugin from 'components/Editor/image-plugin';
import createAlignmentPlugin from 'draft-js-alignment-plugin';
import createFocusPlugin from 'draft-js-focus-plugin';
import createResizeablePlugin from 'draft-js-resizeable-plugin';
import createDndPlugin from 'draft-js-drag-n-drop-plugin';

import alignmentStyles from 'draft-js-alignment-plugin/lib/plugin.css';
import focusStyles from 'draft-js-focus-plugin/lib/plugin.css';
import theme from 'components/Post/Theme.css';
import s from './Editor.css';

const focusPlugin = createFocusPlugin();
const resizeablePlugin = createResizeablePlugin();
const dndPlugin = createDndPlugin();
const alignmentPlugin = createAlignmentPlugin();
const { AlignmentTool } = alignmentPlugin;

const sidebarPlugin = createSidebarPlugin();
const { Sidebar } = sidebarPlugin;

const toolbarPlugin = createInlineToolbarPlugin({
    structure: [
        HeadlineTwoButton,
        HeadlineThreeButton,
        Separator,
        BoldButton,
        ItalicButton,
        UnderlineButton,
        CodeButton,
        Separator,
        TextCenterButton,
        TextRightButton,
        AddLinkButton,
        Separator,
        UnorderedListButton,
        OrderedListButton,
        BlockquoteButton,
    ],
});
const { Toolbar: InlineToolbar } = toolbarPlugin;
const imagePlugin = createImagePlugin();
const embedPlugin = createEmbedPlugin({ EmbedComponent });
const linkPlugin = createLinkPlugin({ component: LinkComponent });
const linkifyPlugin = createLinkifyPlugin();
const emojiPlugin = createEmojiPlugin();
const { EmojiSuggestions } = emojiPlugin;
const autoListPlugin = createAutoListPlugin();

/* init the plugins */
const plugins = [
    dndPlugin,
    focusPlugin,
    alignmentPlugin,
    resizeablePlugin,
    imagePlugin,
    linkifyPlugin,
    embedPlugin,
    emojiPlugin,
    linkPlugin,
    toolbarPlugin,
    sidebarPlugin,
    autoListPlugin,
];

class PostEditor extends Component {
    static propTypes = {
        onUploadCallback: PropTypes.func.isRequired,
    };

    handleKeyCommand = command => {
        // eslint-disable-next-line react/prop-types
        const { editorState } = this.props;
        const newState = RichUtils.handleKeyCommand(editorState, command);
        if (newState) {
            // eslint-disable-next-line react/prop-types
            this.props.onChange(newState);
            return true;
        }

        return false;
    };

    render() {
        const { onUploadCallback, ...other } = this.props;
        return (
            // eslint-disable-next-line css-modules/no-undef-class
            <div className={classNames(s.editor, theme.root)}>
                <Editor
                    {...other}
                    plugins={plugins}
                    blockStyleFn={createBlockStyleWithEditorState(
                        other.editorState,
                    )}
                    handleKeyCommand={this.handleKeyCommand}
                />
                <AlignmentTool />
                <InlineToolbar />
                <Sidebar uploadCallback={onUploadCallback} />
                <EmojiSuggestions />
            </div>
        );
    }
}

export default withStyles(
    editorStyles,
    emojiStyles,
    alignmentStyles,
    focusStyles,
    embedStyles,
    sidebarStyles,
    inlineToolbarStyles,
    theme,
    s,
)(PostEditor);
