import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Nav, NavItem, NavLink } from 'reactstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import messages from 'components/messages';
import s from './SectionTitle.css';
import Link from '../Link';

const SectionTitle = ({ link, title }) => {
    if (link) {
        return (
            <div className={s.root}>
                <Nav
                    className={classNames(
                        'flex-column flex-sm-row justify-content-between',
                    )}
                >
                    <NavItem>
                        <NavLink tag="h4" className={s.title}>
                            <Link title={title} to={link}>
                                {title}
                            </Link>
                        </NavLink>
                    </NavItem>
                    <NavItem className="">
                        <NavLink
                            tag={Link}
                            title={`See more in ${title}`}
                            to={link}
                            className={classNames('text-slate', s.more)}
                        >
                            <FormattedMessage {...messages.seeMore} />
                        </NavLink>
                    </NavItem>
                </Nav>
            </div>
        );
    }

    return (
        <div className={s.root}>
            <h4 className={s.title}>
                {title}
            </h4>
        </div>
    );
};

SectionTitle.propTypes = {
    title: PropTypes.string.isRequired,
    link: PropTypes.string,
};

SectionTitle.defaultProps = {
    link: undefined,
};

export default withStyles(s)(SectionTitle);
