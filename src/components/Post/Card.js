import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Card, CardText, CardBlock, CardTitle } from 'reactstrap';
import s from './Card.css';
import { CardThumbComponent } from './CardThumb';
import CardMeta, { CardMetaComponent } from './CardMeta';
import Link from '../Link';

const ThumbWrapper = ({ wrap, children }) =>
    wrap
        ? <div style={{ flex: '0 0 auto' }} className="w-full w-md-170 h-full">
              {children}
          </div>
        : children;

ThumbWrapper.propTypes = {
    wrap: PropTypes.bool,
    children: PropTypes.node.isRequired,
};

ThumbWrapper.defaultProps = {
    wrap: false,
};

const Thumb = ({ wrap, displayName, href, cardUrl }) =>
    // eslint-disable-next-line jsx-a11y/anchor-has-content
    <Link
        to={href}
        role="button"
        title={displayName}
        className={classNames(
            'd-flex d-block w-full h-200 backgroundCover',
            wrap ? 'h-md-full' : '',
        )}
        style={{
            backgroundImage: cardUrl ? `url("${cardUrl}")` : 'none',
        }}
    />;

Thumb.propTypes = {
    displayName: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    wrap: PropTypes.bool,
    cardUrl: PropTypes.string,
};

Thumb.defaultProps = {
    wrap: false,
    cardUrl: undefined,
};

export const CardComponent = ({
    displayName,
    href,
    description,
    photo,
    vertical,
    ...props
}) =>
    <Card
        className={classNames(
            {
                'flex-column flex-md-row': vertical,
            },
            'w-full',
            'h-auto',
            'h-md-260',
            s.root,
        )}
    >
        <ThumbWrapper wrap={vertical}>
            <Thumb
                wrap={vertical}
                href={href}
                displayName={displayName}
                {...photo}
            />
        </ThumbWrapper>

        <CardBlock className={classNames('h-auto')}>
            <div className="h-full d-flex align-items-end flex-column">
                <CardTitle
                    className={classNames(
                        'h5',
                        'align-self-stretch',
                        s.cardTitle,
                    )}
                >
                    <Link to={href} title={displayName}>
                        {displayName}
                    </Link>
                </CardTitle>
                {description &&
                    <CardText className={classNames(s.cardDescription)}>
                        {description}
                    </CardText>}
                <div className="mt-auto align-self-stretch">
                    <CardMeta className={classNames(s.cardMeta)} {...props} />
                </div>
            </div>
        </CardBlock>
    </Card>;

CardComponent.propTypes = {
    id: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    description: PropTypes.string,
    ...CardMetaComponent.propTypes,
    photo: PropTypes.shape(CardThumbComponent.propTypes),
    vertical: PropTypes.bool,
};

CardComponent.defaultProps = {
    description: undefined,
    photo: [],
    vertical: true,
};

export default withStyles(s)(CardComponent);
