import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Photo from 'components/Photo';
import s from './CardThumb.css';

export const CardThumbComponent = props =>
    <div className={s.cardThumbnail}>
        <Photo className={s.thumbnailImage} {...props} />
    </div>;

CardThumbComponent.propTypes = Photo.propTypes;

export default withStyles(s)(CardThumbComponent);
