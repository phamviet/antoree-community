/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Nav } from 'reactstrap';
import { FormattedRelative } from 'react-intl';
import { LikeButton, BookmarkButton, TextButton } from 'components/Reactions';
import { toggleLike, toggleBookmark } from 'actions/post';
import s from './CardMeta.css';
import Author, { AuthorComponent } from '../Author';

export const CardMetaComponent = ({
    author,
    publishedAt,
    likeCount,
    isLiked,
    isBookmarked,
    disabled,
    onLikeClick,
    onBookmarkClick,
    className,
}) =>
    <div className={classNames(className, s.root)}>
        <Author
            {...author}
            size={40}
            subtitle={<FormattedRelative value={publishedAt} />}
        />
        <Nav className="align-items-center">
            {onLikeClick &&
                <LikeButton
                    onClick={onLikeClick}
                    active={isLiked}
                    disabled={disabled}
                />}
            <TextButton>
                {likeCount}
            </TextButton>
            {onBookmarkClick &&
                <BookmarkButton
                    onClick={onBookmarkClick}
                    active={isBookmarked}
                    disabled={disabled}
                />}
        </Nav>
    </div>;

CardMetaComponent.propTypes = {
    id: PropTypes.string.isRequired,
    publishedAt: PropTypes.string.isRequired,
    author: PropTypes.shape(AuthorComponent.propTypes).isRequired,
    likeCount: PropTypes.number,
    isLiked: PropTypes.bool,
    isBookmarked: PropTypes.bool,
    disabled: PropTypes.bool,
    onLikeClick: PropTypes.func,
    onBookmarkClick: PropTypes.func,
    className: PropTypes.string,
};

CardMetaComponent.defaultProps = {
    likeCount: 0,
    isLiked: false,
    isBookmarked: false,
    disabled: false,
    onLikeClick: undefined,
    onBookmarkClick: undefined,
    className: undefined,
};

export default compose(
    withStyles(s),
    connect(null, (dispatch, { id }) => ({
        onLikeClick: () => dispatch(toggleLike(id)),
        onBookmarkClick: () => dispatch(toggleBookmark(id)),
    })),
)(CardMetaComponent);
