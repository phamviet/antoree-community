/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { compose } from 'redux';
import Validator from 'validatorjs';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Form, Input } from 'reactstrap';
import { TopicField as TopicPickerField } from 'components/Picker';
import Uploader from 'components/Uploader';
import Img from 'components/Img';
import { FEED } from 'shared/constants/imageSizes';
import FormField from '../helpers/FormField';
import EditorField from '../Editor/EditorField';
import s from './Form.css';

const Thumbnail = ({ value, onChange, onUpload, filename, ...other }) =>
    <div className={s.photo}>
        {filename &&
            <Img className="mx-auto d-block" {...FEED} path={filename} />}
        <Uploader
            onChange={file => {
                onUpload(file);
                onChange(file.id);
            }}
            {...other}
        />
    </div>;

Thumbnail.propTypes = {
    value: PropTypes.string.isRequired,
    filename: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onUpload: PropTypes.func.isRequired,
};

Thumbnail.defaultProps = {
    filename: undefined,
};

class PostForm extends Component {
    static propTypes = {
        initialValues: PropTypes.shape({
            displayName: PropTypes.string,
            raw: PropTypes.string,
            status: PropTypes.string,
            authorId: PropTypes.string,
            photoId: PropTypes.string,
            photo: PropTypes.shape({
                filename: PropTypes.string,
            }),
            tags: PropTypes.arrayOf(
                PropTypes.shape({
                    id: PropTypes.string,
                    name: PropTypes.string,
                }),
            ),
        }),
        handleUploadCallback: PropTypes.func.isRequired,
        onRequestSuggestions: PropTypes.func.isRequired,
    };

    static defaultProps = {
        initialValues: undefined,
    };

    state = {
        thumbnail:
            this.props.initialValues && this.props.initialValues.photo
                ? this.props.initialValues.photo.filename
                : null,
    };

    handleThumbnailUpload = file => {
        this.setState({
            thumbnail: file.filename,
        });
    };

    render() {
        const { handleUploadCallback, onRequestSuggestions } = this.props;
        return (
            <div className={s.root}>
                <Form>
                    <FormField
                        name="displayName"
                        component={Input}
                        placeholder="Title"
                        size="lg"
                        type="textarea"
                        className={s.formControlTitle}
                        autoFocus
                    />
                    <FormField
                        name="raw"
                        component={EditorField}
                        onUploadCallback={handleUploadCallback}
                        editorClassName={s.editor}
                        placeholder="Write here"
                    />
                    <FormField
                        label="Picture"
                        name="photoId"
                        component={Thumbnail}
                        filename={this.state.thumbnail}
                        onUploadCallback={handleUploadCallback}
                        onUpload={this.handleThumbnailUpload}
                    />
                    <FormField
                        name="tags"
                        array
                        component={TopicPickerField}
                        onRequestSuggestions={onRequestSuggestions}
                        inputProps={{
                            className: 'form-control',
                            placeholder: 'Topics',
                        }}
                    />
                    <Field name="status" component="input" type="hidden" />
                    <Field name="id" component="input" type="hidden" />
                </Form>
            </div>
        );
    }
}

const validate = data => {
    const rules = {
        displayName: 'required|max:128',
    };

    const validator = new Validator(data, rules);
    validator.passes();

    return validator.errors.all();
};

export const PostFormComponent = PostForm;
export const FORM_NAME = 'post';

export default compose(
    reduxForm({
        form: FORM_NAME,
        validate,
        touchOnBlur: false,
        enableReinitialize: false,
    }),
    withStyles(s),
)(PostForm);
