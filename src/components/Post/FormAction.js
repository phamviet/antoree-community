/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import {
    submit,
    isInvalid,
    isSubmitting,
    isPristine,
    change,
    getFormValues,
} from 'redux-form';
import {
    STATUS_PUBLISHED,
    STATUS_PENDING,
    STATUS_DRAFT,
} from 'shared/constants/post';
import Link from 'components/Link';
import { FORM_NAME } from './Form';

const FormAction = ({
    values,
    handleSubmit,
    changeFieldValue,
    invalid,
    submitting,
    pristine,
    isEditor,
}) =>
    <div>
        <Button color="link" tag={Link} to="/manage/posts/">
            Cancel
        </Button>
        {isEditor &&
            values &&
            values.status !== STATUS_PUBLISHED &&
            <Button
                color="primary"
                className="mr-1 mr-md-3 hidden-xs-down"
                disabled={invalid || pristine || submitting}
                role="button"
                onClick={() => {
                    changeFieldValue(
                        FORM_NAME,
                        'status',
                        STATUS_PUBLISHED,
                        true,
                    );
                    setTimeout(() => {
                        handleSubmit(FORM_NAME);
                    }, 0);
                }}
            >
                Publish
            </Button>}
        <Button
            color="primary"
            outline
            disabled={invalid || pristine || submitting}
            role="button"
            onClick={() => {
                if (values.status === STATUS_DRAFT) {
                    changeFieldValue(FORM_NAME, 'status', STATUS_PENDING, true);
                }
                setTimeout(() => {
                    handleSubmit(FORM_NAME);
                }, 0);
            }}
        >
            Save
        </Button>
    </div>;

FormAction.propTypes = {
    values: PropTypes.shape({
        status: PropTypes.string,
    }),
    handleSubmit: PropTypes.func.isRequired,
    changeFieldValue: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    invalid: PropTypes.bool.isRequired,
    isEditor: PropTypes.bool,
};

// export const FormActionComponent = FormAction;

export default connect(
    state => ({
        values: getFormValues(FORM_NAME)(state),
        submitting: isSubmitting(FORM_NAME)(state),
        pristine: isPristine(FORM_NAME)(state),
        invalid: isInvalid(FORM_NAME)(state),
        isEditor: state.user && state.user.isEditor,
    }),
    {
        handleSubmit: submit,
        changeFieldValue: change,
    },
)(FormAction);
