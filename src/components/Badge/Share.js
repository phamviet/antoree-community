import React from 'react';
import PropTypes from 'prop-types';
import FaShare from 'react-icons/lib/fa/share-alt';

const Share = props =>
    <div>
        <FaShare /> {props.shareCount}
    </div>;

Share.propTypes = {
    shareCount: PropTypes.number.isRequired,
};

export default Share;
