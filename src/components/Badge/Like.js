import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import FaHeart from 'react-icons/lib/fa/heart';
import FaHeartO from 'react-icons/lib/fa/heart-o';
import s from './Like.css';

export const LikeComponent = ({ isLiked, likeCount, onLike }) => {
    const style = {
        color: isLiked ? 'red' : '#000',
    };

    const btnProps = {
        className: s.likeButton,
    };

    if (onLike) {
        btnProps.onClick = onLike;
    }

    return (
        <button {...btnProps}>
            {React.createElement(isLiked ? FaHeart : FaHeartO, {
                style,
            })}{' '}
            {likeCount}
        </button>
    );
};

LikeComponent.propTypes = {
    isLiked: PropTypes.bool,
    likeCount: PropTypes.number,
    // eslint-disable-next-line react/require-default-props
    onLike: PropTypes.func,
};

LikeComponent.defaultProps = {
    isLiked: false,
    likeCount: 0,
};

export default withStyles(s)(LikeComponent);
