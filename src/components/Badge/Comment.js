import React from 'react';
import PropTypes from 'prop-types';
import FaComments from 'react-icons/lib/fa/comments';

const Comment = props =>
    <div>
        <FaComments /> {props.commentCount}
    </div>;

Comment.propTypes = {
    commentCount: PropTypes.number.isRequired,
};

export default Comment;
