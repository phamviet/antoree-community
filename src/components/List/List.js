/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

const List = ({ items, component, ...other }) => {
    if (!Array.isArray(items)) {
        return <span>...</span>;
    }
    return (
        <div className="my-3">
            {items.map(item =>
                <div key={item.id}>
                    {React.createElement(component, {
                        key: item.id,
                        item,
                        ...other,
                    })}
                    <hr />
                </div>,
            )}

            {items.length > 0 &&
                <Button onClick={fetch} color="link">
                    Load more
                </Button>}
        </div>
    );
};

List.propTypes = {
    items: PropTypes.array.isRequired,
    component: PropTypes.oneOfType([PropTypes.func, PropTypes.element])
        .isRequired,
};

export default List;
