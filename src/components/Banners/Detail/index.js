import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import classNames from 'classnames';
import { Button, Row, Col } from 'reactstrap';
import TailRightIcon from 'components/Icons/TailRight';
import s from './style.css';
import kidBg from './kid.png';
import adultBg from './adult.png';

const BannerDetail = ({ onAction, kid, className }) =>
    <div
        style={{
            backgroundImage: `url(${kid ? kidBg : adultBg})`,
        }}
        className={classNames(s.root, className)}
    >
        <Row className="justify-content-end">
            <Col md={6}>
                <div className={s.contentWrapper}>
                    <h3 className={classNames(s.title, 'mb-4')}>
                        Đăng ký kiểm tra trình độ miễn phí {kid && 'cho bé'}
                    </h3>
                    <p className="text-slate mb-5">
                        Điền thông tin liên hệ để được kiểm tra trình độ và tư
                        vấn lộ trình học miễn phí.
                    </p>
                    <Button
                        onClick={onAction}
                        color="primary"
                        size="lg"
                        role="button"
                    >
                        Đăng ký ngay để nhận tư vấn {' '}
                        <TailRightIcon width={24} height={16} />
                    </Button>
                </div>
            </Col>
        </Row>
    </div>;

BannerDetail.propTypes = {
    onAction: PropTypes.func.isRequired,
    kid: PropTypes.bool,
    className: PropTypes.string,
};

BannerDetail.defaultProps = {
    kid: false,
    className: undefined,
};

export default withStyles(s)(BannerDetail);
