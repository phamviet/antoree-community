/* eslint-disable react/prop-types,react/no-children-prop,jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react';
import classNames from 'classnames';
import { getSelectedBlocksMetadata, setBlockData } from 'draftjs-utils';

export default ({ name, value, children }) =>
    class BlockMetaButton extends Component {
        toggleStyle = event => {
            event.preventDefault();
            const editorState = this.props.getEditorState();
            let val = getSelectedBlocksMetadata(editorState).get(name);
            if (val === value) {
                val = undefined;
            } else {
                val = value;
            }

            this.props.setEditorState(
                setBlockData(this.props.getEditorState(), { [name]: val }),
            );
        };

        preventBubblingUp = event => {
            event.preventDefault();
        };

        blockTypeIsActive = () => {
            const editorState = this.props.getEditorState();
            const val = getSelectedBlocksMetadata(editorState).get(name);
            return val && val === value;
        };

        render() {
            const { theme } = this.props;
            return (
                <div
                    className={theme.buttonWrapper}
                    onMouseDown={this.preventBubblingUp}
                >
                    <button
                        className={classNames(theme.button, {
                            [theme.active]: this.blockTypeIsActive(),
                        })}
                        onClick={this.toggleStyle}
                        type="button"
                        children={children}
                    />
                </div>
            );
        }
    };
