import classNames from 'classnames';

const BASE_BLOCK_CLASS = 'block-content';

const blockStyleFn = block => {
    const blockClass = `block-${block.getType()}`;
    switch (block.getType()) {
        case 'blockquote':
            return classNames(BASE_BLOCK_CLASS, 'blockquote', blockClass);
        case 'header-one':
        case 'header-two':
        case 'header-three':
        case 'header-four':
        case 'header-five':
        case 'header-six':
            return classNames(BASE_BLOCK_CLASS, 'block-header', blockClass);
        case 'unstyled': {
            const data = block.getData();
            const textAlign = data.get('text-align');
            return classNames(BASE_BLOCK_CLASS, blockClass, {
                [`text-${textAlign}`]: textAlign,
            });
        }
        case 'atomic':
            return classNames(BASE_BLOCK_CLASS, blockClass);
        default:
            return blockClass;
    }
};

export function withEditorState(editorState) {
    const contentState = editorState.getCurrentContent();

    return block => {
        const styleFn = blockStyleFn(block);
        switch (block.getType()) {
            case 'atomic': {
                const entity = contentState.getEntity(block.getEntityAt(0));
                const type = entity.getType();
                return classNames(styleFn, `atomic-${type}`);
            }

            default:
                return styleFn;
        }
    };
}

export default blockStyleFn;
