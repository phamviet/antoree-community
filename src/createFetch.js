/**
 *
 */

/* @flow */

type Fetch = (url: string, options: ?any) => Promise<any>;

type Options = {
    baseUrl: string,
    cookie?: string,
};

/**
 * Creates a wrapper function around the HTML5 Fetch API that provides
 * default arguments to fetch(...) and is intended to reduce the amount
 * of boilerplate code in the application.
 * https://developer.mozilla.org/docs/Web/API/Fetch_API/Using_Fetch
 */
function createFetch(fetch: Fetch, { baseUrl, cookie }: Options) {
    // NOTE: Tweak the default options to suite your application needs
    const defaults = {
        method: 'POST', // handy with GraphQL backends
        mode: baseUrl ? 'cors' : 'same-origin',
        credentials: baseUrl ? 'include' : 'same-origin',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            ...(cookie ? { Cookie: cookie } : null),
        },
    };

    return (url: string, options: any) =>
        url.startsWith('/graphql')
            ? fetch(`${baseUrl}${url}`, {
                  ...defaults,
                  ...options,
                  headers: {
                      ...defaults.headers,
                      ...(options && options.headers),
                  },
              })
            : fetch(url, options);
}

export const createFetchUploader = (fetch: Fetch) => async (
    file,
    params = {},
) => {
    const data = new FormData();
    data.append('file', file);
    Object.keys(params).forEach(name => data.set(name, params[name]));

    const res = await fetch('/api/file', {
        method: 'POST',
        credentials: 'same-origin',
        body: data,
    });

    if (res.ok) {
        const f = await res.json();
        return f;
    }

    // eslint-disable-next-line no-console
    console.log(res);

    throw new Error('There was something wrong. Please try again.');
};

export default createFetch;
