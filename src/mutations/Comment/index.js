/* eslint-disable import/prefer-default-export */
import gql from 'graphql-tag';

export const saveCommentMutate = gql`
    mutation saveComment($input: CommentInput!) {
        saveComment(input: $input) {
            id
            content
            postId
            parentId
        }
    }
`;
