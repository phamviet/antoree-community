import ApolloClient, { createNetworkInterface } from 'apollo-client';
import environment from '../relay';

const client = new ApolloClient({
    networkInterface: createNetworkInterface({
        uri: '/graphql',
        opts: {
            // Additional fetch options like `credentials` or `headers`
            credentials: 'include',
        },
    }),
    queryDeduplication: true,
    reduxRootSelector: state => state.apollo,
});

client.relay = environment;

export default function createApolloClient() {
    return client;
}
