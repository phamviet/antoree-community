/**
 *
 */

import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import deepForceUpdate from 'react-deep-force-update';
import queryString from 'query-string';
import ReactGA from 'react-ga';
import ReactPixel from 'react-facebook-pixel';
import Validator from 'validatorjs';
import { createPath } from 'history/PathUtils';
import { addLocaleData } from 'react-intl';
// This is so bad: requiring all locale if they are not needed?
/* @intl-code-template import ${lang} from 'react-intl/locale-data/${lang}'; */
import en from 'react-intl/locale-data/en';
import vi from 'react-intl/locale-data/vi';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import configureStore from './store/configureStore';
import App from './components/App';
import createFetch from './createFetch';
import history from './history';
import createApolloClient from './core/createApolloClient';
import { updateMeta } from './DOMUtils';
import { resetNavbarActions } from './actions/runtime';
import { getIntl } from './actions/intl';
import createQuery from './shared/queries';
import { SITE_TITLE } from './shared/constants';
import router from './router';

// eslint-disable-next-line
import bootstrap from 'scss/build/bootstrap.min.css'; // it's here for deploying

/* @intl-code-template addLocaleData(${lang}); */
addLocaleData(en);
addLocaleData(vi);

const apolloClient = createApolloClient();

// Universal HTTP client
const fetch = createFetch(self.fetch, {
    baseUrl: window.App.apiUrl,
});

const store = configureStore(window.App.state, {
    history,
    apolloClient,
    fetch,
});

// Global (context) variables that can be easily accessed from any React component
// https://facebook.github.io/react/docs/context.html
const context = {
    // Enables critical path CSS rendering
    // https://github.com/kriasoft/isomorphic-style-loader
    insertCss: (...styles) => {
        // eslint-disable-next-line no-underscore-dangle
        const removeCss = styles.map(x => x._insertCss());
        return () => {
            removeCss.forEach(f => f());
        };
    },
    // For react-apollo
    client: apolloClient,

    // Universal HTTP client
    fetch,
    // Initialize a new Redux store
    // http://redux.js.org/docs/basics/UsageWithReact.html
    store,
    queries: createQuery({ store, client: apolloClient }),
    storeSubscription: null,
    // intl instance as it can be get with injectIntl
    intl: store.dispatch(getIntl()),
};

Validator.useLang(context.intl.locale.split('-')[0]);

// Switch off the native scroll restoration behavior and handle it manually
// https://developers.google.com/web/updates/2015/09/history-api-scroll-restoration
const scrollPositionsHistory = {};
if (window.history && 'scrollRestoration' in window.history) {
    window.history.scrollRestoration = 'manual';
}

function logPageView(location) {
    // ignore management page from ga
    if (location.pathname.indexOf('/manage') !== 0) {
        const path = createPath(location);
        ReactGA.set({ page: path });
        ReactGA.pageview(path);
    }
}

let onRenderComplete = function initialRenderComplete() {
    const elem = document.getElementById('css');
    if (elem) elem.parentNode.removeChild(elem);
    onRenderComplete = function renderComplete(route, location) {
        document.title = route.title
            ? `${route.title} - ${SITE_TITLE}`
            : SITE_TITLE;

        updateMeta('description', route.description);
        // Update necessary tags in <head> at runtime here, ie:
        // updateMeta('keywords', route.keywords);
        // updateCustomMeta('og:url', route.canonicalUrl);
        // updateCustomMeta('og:image', route.imageUrl);
        // updateLink('canonical', route.canonicalUrl);
        // etc.

        let scrollX = 0;
        let scrollY = 0;
        const pos = scrollPositionsHistory[location.key];
        if (pos) {
            scrollX = pos.scrollX;
            scrollY = pos.scrollY;
        } else {
            const targetHash = location.hash.substr(1);
            if (targetHash) {
                const target = document.getElementById(targetHash);
                if (target) {
                    scrollY =
                        window.pageYOffset + target.getBoundingClientRect().top;
                }
            }
        }

        // Restore the scroll position if it was saved into the state
        // or scroll to the given #hash anchor
        // or scroll to top of the page
        window.scrollTo(scrollX, scrollY);

        // Google Analytics tracking. Don't send 'pageview' event after
        // the initial rendering, as it was already sent
        logPageView(location);
    };
};

const container = document.getElementById('app');
let appInstance;
let currentLocation = history.location;

// Re-render the app when window.location changes
async function onLocationChange(location, action) {
    // Remember the latest scroll position for the previous location
    scrollPositionsHistory[currentLocation.key] = {
        scrollX: window.pageXOffset,
        scrollY: window.pageYOffset,
    };
    // Delete stored scroll position for next page if any
    if (action === 'PUSH') {
        delete scrollPositionsHistory[location.key];
    }

    currentLocation = location;
    context.intl = store.dispatch(getIntl());

    try {
        // Start loading bar
        context.store.dispatch(showLoading());
        context.store.dispatch(resetNavbarActions());

        const startTime = Date.now();
        // Traverses the list of routes in the order they are defined until
        // it finds the first route that matches provided URL path string
        // and whose action method returns anything other than `undefined`.
        const route = await router.resolve({
            ...context,
            path: location.pathname,
            query: queryString.parse(location.search),
            locale: store.getState().intl.locale,
        });

        if (action) {
            // Don't track initial rendering
            ReactGA.timing({
                category: 'Ajax Page Load',
                variable: 'load',
                value: Date.now() - startTime, // in milliseconds
                label: location.pathname,
            });
        }

        // Stop loading bar
        context.store.dispatch(hideLoading());

        // Prevent multiple page renders during the routing process
        if (currentLocation.key !== location.key) {
            return;
        }

        if (route.redirect) {
            history.replace(route.redirect);
            return;
        }

        appInstance = ReactDOM.render(
            <App context={context}>
                {route.component}
            </App>,
            container,
            () => onRenderComplete(route, location),
        );
    } catch (error) {
        if (__DEV__) {
            throw error;
        }

        console.error(error);

        // Do a full page reload if error occurs during client-side navigation
        if (action && currentLocation.key === location.key) {
            window.location.reload();
        }
    }
}

let isHistoryObserved = false;
export default function main() {
    // Handle client-side navigation by using HTML5 History API
    // For more information visit https://github.com/mjackson/history#readme
    currentLocation = history.location;
    if (!isHistoryObserved) {
        isHistoryObserved = true;
        history.listen(onLocationChange);
    }

    const { analytics, user } = window.App;
    if (analytics) {
        if (analytics.googleTrackingId) {
            ReactGA.initialize(analytics.googleTrackingId, {
                debug: __DEV__,
            });

            if (user) {
                ReactGA.set({ userId: user.id });
            }

            logPageView(currentLocation);
        }

        if (analytics.fbPixelId) {
            // Facebook Pixel will track pageview on every page change
            // # https://developers.facebook.com/ads/blog/post/2017/05/29/tagging-a-single-page-application-facebook-pixel/
            ReactPixel.init(analytics.fbPixelId);
            ReactPixel.pageView();
        }
    }

    onLocationChange(currentLocation);
}

// globally accesible entry point
window.RSK_ENTRY = main;

// Enable Hot Module Replacement (HMR)
if (module.hot) {
    module.hot.accept('./router', () => {
        if (appInstance) {
            // Force-update the whole tree, including components that refuse to update
            deepForceUpdate(appInstance);
        }

        onLocationChange(currentLocation);
    });
}
