## Antoree Community

Project was started with [React Starter Kit](https://www.reactstarterkit.com)

* [Getting Started](https://github.com/kriasoft/react-starter-kit/blob/master/docs/getting-started.md)

### Image CDN

Environments need to set:

* **AWS_ACCESS_KEY_ID**
* **AWS_SECRET_ACCESS_KEY**
* **AWS_S3_BUCKET**: `community.antoree.com`
* **IMAGE_DOMAIN**: `https://antoree-community.imgix.net`

### Links

* https://jpuri.github.io/react-draft-wysiwyg
* https://reactstrap.github.io
* http://redux-form.com/7.0.3/
