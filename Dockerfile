FROM node:6.11-alpine

# Set a working directory
WORKDIR /usr/src/app

COPY ./build/package.json .
#COPY ./build/yarn.lock .

# Install Node.js dependencies
RUN yarn install --production --no-progress --no-lockfile

# Copy application files
COPY ./build .

EXPOSE 3000

CMD [ "node", "server.js" ]
