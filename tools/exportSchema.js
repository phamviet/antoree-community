import path from 'path';
import fetch from 'node-fetch';
import { writeFile } from './lib/fs';
import runServer from './runServer';

async function exportSchema() {
    const server = await runServer();

    const url = `http://${server.host}/graphql/schema/`;
    const fileName = 'schema.graphql';
    const dist = path.join('src', fileName);
    const timeStart = new Date();
    const response = await fetch(url);
    const timeEnd = new Date();
    const text = await response.text();
    await writeFile(dist, text);
    const time = timeEnd.getTime() - timeStart.getTime();
    console.info(`=> ${response.status} ${response.statusText} (${time} ms)`);

    server.kill('SIGTERM');
}

export default exportSchema;
