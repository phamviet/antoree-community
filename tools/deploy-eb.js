/**
 *
 */

import Promise from 'bluebird';
import EB from 'aws-sdk/clients/elasticbeanstalk';
import S3 from 'aws-sdk/clients/s3';

const region = process.env.AWS_REGION || 'ap-southeast-1';
const S3Bucket = process.env.EB_BUCKET;
const ApplicationName = process.env.EB_APP_NAME;
const EnvironmentName = process.argv[3];
const branch = process.env.CI_COMMIT_REF_SLUG;
const tag = process.env.CI_COMMIT_SHA.substr(0, 8);

const VersionLabel = `${EnvironmentName}-${branch}-${tag}`;
const DockerrrunJson = `${EnvironmentName}/${branch}/${tag}-Dockerrrun.aws.json`;

const eb = new EB({ region });
const s3 = new S3({ region, params: { Bucket: S3Bucket } });

const dockerRunTemplate = {
    AWSEBDockerrunVersion: 1,
    Authentication: {
        Bucket: S3Bucket,
        Key: 'dockercfg',
    },
    Image: {
        Name: `${process.env.CI_REGISTRY_IMAGE}:${process.env.CI_COMMIT_SHA}`,
        Update: 'true',
    },
    Ports: [
        {
            ContainerPort: 3000,
        },
    ],
};

const s3Upload = params =>
    new Promise((resolve, reject) => {
        s3.upload(params, (err, data) => {
            if (err) {
                return reject(err);
            }
            return resolve(data);
        });
    });

const createApplicationVersion = params =>
    new Promise((resolve, reject) => {
        eb.createApplicationVersion(params, (err, data) => {
            if (err) {
                return reject(err);
            }
            return resolve(data);
        });
    });

const updateEnvironment = params =>
    new Promise((resolve, reject) => {
        eb.updateEnvironment(params, (err, data) => {
            if (err) {
                return reject(err);
            }
            return resolve(data);
        });
    });

/**
 * Deploy the contents of the `/build` folder to a remote server via Git.
 */
async function deployEb() {
    const dockerRunJson = JSON.stringify(dockerRunTemplate, null, 2);

    s3Upload({
        Key: DockerrrunJson,
        Body: dockerRunJson,
    })
        .then(() => {
            console.info('Successfully uploaded Dockerrrun.aws.json.');
            const SourceBundle = {
                S3Bucket,
                S3Key: DockerrrunJson,
            };

            return createApplicationVersion({
                ApplicationName,
                VersionLabel,
                SourceBundle,
            });
        })
        .then(() => {
            console.info(
                `Successfully created new app version ${VersionLabel}.`,
            );
            return updateEnvironment({
                VersionLabel,
                EnvironmentName,
            });
        })
        .then(() => {
            console.info(
                `Successfully updated environment ${EnvironmentName}.`,
            );
        })
        .catch(error => {
            console.error(`Error: ${error.message}`);
        });
}

export default deployEb;
